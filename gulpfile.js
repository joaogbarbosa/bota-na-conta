var gulp = require('gulp');
var path = require('path');
var fs = require('fs');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');

var modules_root = './application/modules/';

function getFolders(dir) {
    return fs.readdirSync(dir)
      .filter(function(file) {
        return fs.statSync(path.join(dir, file)).isDirectory();
      });
}

var modules = getFolders(modules_root);

var folders = ['./assets'];
modules.map(function(folder){
    folders.push(modules_root + folder + '/assets');
});


gulp.task('sass', function(){

    folders.map(function(element){
        gulp.src(element + '/scss/*.scss')
            .pipe(sass({
                errLogToConsole: false,
                onError: function(err) {
                    return notify().write(err);
                }
            }))
            .pipe(sourcemaps.init())
            .pipe(autoprefixer({
                browsers: ['last 10 versions', 'Firefox > 20', 'Chrome > 20', 'IE > 8'],
                cascade: true
            }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(element + '/css'))
    });
});

gulp.task('watch', function () {
    folders.map(function(element){
        gulp.watch(element + '/scss/*.scss', ['sass']);
    });

});

gulp.task('default', ['sass', 'watch']);