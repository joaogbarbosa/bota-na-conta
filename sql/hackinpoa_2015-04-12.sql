# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: hackinpoa.c2kchoytmehy.eu-west-1.rds.amazonaws.com (MySQL 5.6.22-log)
# Database: hackinpoa
# Generation Time: 2015-04-12 13:42:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table activities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activities`;

CREATE TABLE `activities` (
  `activity_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `activity` longtext NOT NULL,
  `module` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `deleted` tinyint(12) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;

INSERT INTO `activities` (`activity_id`, `user_id`, `activity`, `module`, `created_on`, `deleted`)
VALUES
	(1,1,'logged in from: ::1','users','2015-04-11 12:28:12',0),
	(2,1,'logged in from: ::1','users','2015-04-11 17:31:30',0),
	(3,1,'Migrate Type: restaurantes_ to Version: 2 from: ::1','migrations','2015-04-11 17:40:11',0),
	(4,1,'Migrate module: restaurantes Version: 2 from: ::1','migrations','2015-04-11 17:40:11',0),
	(5,1,'Criado registro: 1 | IP: ::1 | Data: {\"name\":\"Kings K\\u00e3o\",\"code\":\"1\",\"users\":{\"1\":\"1\"},\"ativo\":\"1\",\"save\":\"Salvar\"}','restaurantes','2015-04-11 12:51:41',0),
	(6,1,'Criado registro: 2 | IP: ::1 | Data: {\"name\":\"Kings K\\u00e3o\",\"code\":\"1\",\"users\":{\"1\":\"1\"},\"ativo\":\"1\",\"save\":\"Salvar\"}','restaurantes','2015-04-11 12:51:43',0),
	(7,1,'Criado registro: 3 | IP: ::1 | Data: {\"name\":\"Kings K\\u00e3o\",\"code\":\"1\",\"users\":{\"1\":\"1\"},\"ativo\":\"1\",\"save\":\"Salvar\"}','restaurantes','2015-04-11 12:51:45',0),
	(8,1,'Deletado registro: 2 | IP: ::1 | Data: {\"id\":\"2\",\"created_on\":\"2015-04-11 12:51:42\",\"modified_on\":\"0000-00-00 00:00:00\",\"name\":\"Kings K\\u00e3o\",\"code\":\"1\",\"ativo\":\"1\"}','restaurantes','2015-04-11 12:52:11',0),
	(9,1,'Deletado registro: 3 | IP: ::1 | Data: {\"id\":\"3\",\"created_on\":\"2015-04-11 12:51:44\",\"modified_on\":\"0000-00-00 00:00:00\",\"name\":\"Kings K\\u00e3o\",\"code\":\"1\",\"ativo\":\"1\"}','restaurantes','2015-04-11 12:52:12',0),
	(10,1,'Migrate Type: restaurantes_ Uninstalled Version: 0 from: ::1','migrations','2015-04-11 17:56:13',0),
	(11,1,'Migrate module: restaurantes Version: 0 from: ::1','migrations','2015-04-11 17:56:13',0),
	(12,1,'Migrate Type: restaurantes_ to Version: 2 from: ::1','migrations','2015-04-11 17:56:48',0),
	(13,1,'Migrate module: restaurantes Version: 2 from: ::1','migrations','2015-04-11 17:56:49',0),
	(14,1,'Migrate Type: restaurant_ to Version: 2 from: ::1','migrations','2015-04-11 18:07:05',0),
	(15,1,'Migrate module: restaurant Version: 2 from: ::1','migrations','2015-04-11 18:07:05',0),
	(16,1,'Migrate Type: restaurants_ to Version: 2 from: ::1','migrations','2015-04-11 18:12:18',0),
	(17,1,'Migrate module: restaurants Version: 2 from: ::1','migrations','2015-04-11 18:12:19',0),
	(18,1,'Log settings modified from: ::1','logs','2015-04-11 18:15:43',0),
	(19,1,'Criado registro: 1 | IP: ::1 | Data: {\"name\":\"Cavanhas\",\"code\":\"12345\",\"users\":{\"1\":\"1\"},\"active\":\"1\",\"save\":\"Salvar\"}','restaurants','2015-04-11 14:15:07',0),
	(20,1,'Criado registro: 2 | IP: ::1 | Data: {\"name\":\"Kings K\\u00e3o\",\"code\":\"321\",\"users\":{\"1\":\"1\"},\"active\":\"1\",\"save\":\"Salvar\"}','restaurants','2015-04-11 14:18:46',0),
	(21,1,'Migrate Type: products_categories_ to Version: 2 from: ::1','migrations','2015-04-11 19:20:54',0),
	(22,1,'Migrate module: products_categories Version: 2 from: ::1','migrations','2015-04-11 19:20:55',0),
	(23,1,'Migrate Type: products_ to Version: 2 from: ::1','migrations','2015-04-11 19:22:07',0),
	(24,1,'Migrate module: products Version: 2 from: ::1','migrations','2015-04-11 19:22:08',0),
	(25,1,'Criado registro: 1 | IP: ::1 | Data: {\"name\":\"Bebidas\",\"active\":\"1\",\"save\":\"Salvar\"}','products_categories','2015-04-11 19:26:32',0),
	(26,1,'Criado registro: 1 | IP: ::1 | Data: {\"id_product_category\":\"1\",\"name\":\"Coca-Cola Lata\",\"value\":\"3.50\",\"ativo\":\"1\",\"save\":\"Salvar\"}','products','2015-04-11 19:29:45',0),
	(27,1,'Criado registro: 2 | IP: ::1 | Data: {\"id_product_category\":\"1\",\"name\":\"Coca-Cola 600ml\",\"value\":\"4.50\",\"active\":\"1\",\"save\":\"Salvar\"}','products','2015-04-11 19:37:14',0),
	(28,1,'Atualizado registro: 1 | IP: ::1 | Data: {\"active\":\"1\",\"id\":\"1\"}','products','2015-04-11 19:38:04',0),
	(29,1,'Criado registro: 3 | IP: ::1 | Data: {\"id_product_category\":\"1\",\"name\":\"Agua Mineral\",\"value\":\"2.50\",\"active\":\"1\",\"save\":\"Salvar\"}','products','2015-04-11 19:38:46',0),
	(30,1,'App settings saved from: ::1','core','2015-04-11 19:46:50',0),
	(31,1,'Migrate Type: establishments_ to Version: 2 from: ::1','migrations','2015-04-11 20:31:32',0),
	(32,1,'Migrate module: establishments Version: 2 from: ::1','migrations','2015-04-11 20:31:32',0),
	(33,1,'Migrate Type: products_categories_ to Version: 2 from: ::1','migrations','2015-04-11 20:32:11',0),
	(34,1,'Migrate module: products_categories Version: 2 from: ::1','migrations','2015-04-11 20:32:11',0),
	(35,1,'Migrate Type: products_ to Version: 2 from: ::1','migrations','2015-04-11 20:32:47',0),
	(36,1,'Migrate module: products Version: 2 from: ::1','migrations','2015-04-11 20:32:47',0),
	(37,1,'logged in from: ::1','users','2015-04-11 15:35:24',0),
	(38,1,'Criado registro: 1 | IP: ::1 | Data: {\"id_establishment\":\"1\",\"name\":\"Bebidas\",\"active\":\"1\",\"save\":\"Salvar\"}','products_categories','2015-04-11 15:46:49',0),
	(39,1,'Criado registro: 2 | IP: ::1 | Data: {\"id_establishment\":\"1\",\"name\":\"Entradas\",\"active\":\"1\",\"save\":\"Salvar\"}','products_categories','2015-04-11 15:48:49',0),
	(40,1,'Criado registro: 3 | IP: ::1 | Data: {\"id_establishment\":\"1\",\"name\":\"Entradas\",\"active\":\"1\",\"save\":\"Salvar\"}','products_categories','2015-04-11 15:49:27',0),
	(41,1,'Atualizado registro: 3 | IP: ::1 | Data: {\"id_establishment\":\"1\",\"name\":\"Principais\",\"active\":\"1\",\"save\":\"Salvar\",\"id\":\"3\"}','products_categories','2015-04-11 15:50:09',0),
	(42,1,'Atualizado registro: 3 | IP: ::1 | Data: {\"id_establishment\":\"1\",\"name\":\"Principais\",\"active\":\"1\",\"save\":\"Salvar\",\"id\":\"3\"}','products_categories','2015-04-11 15:50:09',0),
	(43,1,'Criado registro: 1 | IP: ::1 | Data: {\"id_product_category\":\"1\",\"name\":\"Agua com g\\u00e1s\",\"value\":\"3.00\",\"active\":\"1\",\"save\":\"Salvar\"}','products','2015-04-11 15:52:37',0),
	(44,1,'Migrate Type: identifiers_ to Version: 2 from: ::1','migrations','2015-04-11 20:56:36',0),
	(45,1,'Migrate module: identifiers Version: 2 from: ::1','migrations','2015-04-11 20:56:36',0),
	(46,1,'Criado registro: 1 | IP: ::1 | Data: {\"id_establishment\":\"2\",\"name\":\"Mesa 1\",\"code\":\"km1\",\"active\":\"1\",\"save\":\"Salvar\"}','identifiers','2015-04-11 21:05:57',0),
	(47,1,'Criado registro: 2 | IP: ::1 | Data: {\"id_establishment\":\"2\",\"name\":\"Mesa 2\",\"code\":\"km2\",\"active\":\"1\",\"save\":\"Salvar\"}','identifiers','2015-04-11 21:06:35',0),
	(48,1,'Atualizado registro: 5 | IP: ::1 | Data: {\"id_product_category\":\"2\",\"name\":\"Iscas de fil\\u00e9\",\"value\":\"18.00\",\"active\":\"1\",\"save\":\"Salvar\",\"id\":\"5\"}','products','2015-04-11 16:17:57',0),
	(49,1,'Migrate Type: orders_ to Version: 2 from: ::1','migrations','2015-04-11 21:43:36',0),
	(50,1,'Migrate module: orders Version: 2 from: ::1','migrations','2015-04-11 21:43:36',0),
	(51,1,'Migrate Type: orders_ Uninstalled Version: 0 from: ::1','migrations','2015-04-11 21:49:08',0),
	(52,1,'Migrate module: orders Version: 0 from: ::1','migrations','2015-04-11 21:49:08',0),
	(53,1,'Migrate Type: orders_ to Version: 2 from: ::1','migrations','2015-04-11 21:49:52',0),
	(54,1,'Migrate module: orders Version: 2 from: ::1','migrations','2015-04-11 21:49:52',0),
	(55,1,'Criado registro: 1 | IP: ::1 | Data: {\"id_identifier\":\"2\",\"device_code\":\"123\",\"open\":\"1\",\"save\":\"Salvar\"}','orders','2015-04-11 21:53:22',0),
	(56,1,'Migrate Type: order_items_ to Version: 2 from: ::1','migrations','2015-04-11 22:22:19',0),
	(57,1,'Migrate module: order_items Version: 2 from: ::1','migrations','2015-04-11 22:22:19',0),
	(58,1,'Criado registro: 1 | IP: ::1 | Data: {\"id_order\":\"1\",\"id_product\":\"1\",\"status\":\"new\",\"value\":\"3.00\",\"save\":\"Salvar\"}','order_items','2015-04-11 22:28:56',0),
	(59,1,'Migrate Type: calls_ to Version: 2 from: ::1','migrations','2015-04-12 00:36:56',0),
	(60,1,'Migrate module: calls Version: 2 from: ::1','migrations','2015-04-12 00:36:56',0),
	(61,1,'Migrate Type: products_ to Version: 3 from: ::1','migrations','2015-04-12 00:57:26',0),
	(62,1,'Migrate module: products Version: 3 from: ::1','migrations','2015-04-12 00:57:27',0),
	(63,1,'Atualizado registro: 4 | IP: ::1 | Data: {\"id_product_category\":\"2\",\"name\":\"Batata frita\",\"value\":\"10.00\",\"description\":\"Por\\u00e7\\u00e3o para 1 pessoa, acompanha peda\\u00e7os de calabresa e queijo.\",\"active\":\"1\",\"save\":\"Salvar\",\"id\":\"4\"}','products','2015-04-12 01:01:29',0),
	(64,1,'created a new Gerente: Gerente','users','2015-04-12 01:41:27',0),
	(65,1,'created a new Garçon: Garçon','users','2015-04-12 01:42:35',0),
	(66,1,'created a new Cozinha: Cozinha','users','2015-04-12 01:44:27',0),
	(67,1,'modified user: Administrador','users','2015-04-12 01:48:50',0),
	(68,2,'logged in from: ::1','users','2015-04-12 01:56:29',0),
	(69,1,'Atualizado registro: 2 | IP: ::1 | Data: {\"name\":\"Kings\",\"users\":{\"1\":\"2\"},\"active\":\"1\",\"save\":\"Salvar\",\"id\":\"2\"}','establishments','2015-04-12 03:04:40',0),
	(70,2,'logged in from: ::1','users','2015-04-12 03:05:14',0),
	(71,2,'Atualizado registro: 2 | IP: ::1 | Data: {\"name\":\"Kings\",\"active\":\"1\",\"save\":\"Salvar\",\"id\":\"2\"}','establishments','2015-04-12 03:30:17',0),
	(72,2,'Atualizado registro: 2 | IP: ::1 | Data: {\"name\":\"Kings K\\u00e3o\",\"active\":\"1\",\"save\":\"Salvar\",\"id\":\"2\"}','establishments','2015-04-12 03:30:31',0),
	(73,1,'Criado registro: 3 | IP: ::1 | Data: {\"name\":\"Cavanhas\",\"code\":\"cv1n\",\"active\":\"1\",\"save\":\"Salvar\"}','establishments','2015-04-12 03:33:33',0),
	(74,1,'logged in from: ::1','users','2015-04-12 05:03:01',0),
	(75,1,'logged in from: 172.16.5.6','users','2015-04-12 02:47:14',0),
	(76,1,'modified user: Administrador','users','2015-04-12 02:47:57',0),
	(77,1,'modified user: Administrador','users','2015-04-12 02:47:59',0),
	(78,1,'Atualizado registro: 1 | IP: ::1 | Data: {\"active\":\"1\",\"ci_csrf_token\":\"\",\"id\":\"1\"}','establishments','2015-04-12 07:50:32',0),
	(79,1,'Atualizado registro: 1 | IP: ::1 | Data: {\"active\":\"0\",\"ci_csrf_token\":\"\",\"id\":\"1\"}','establishments','2015-04-12 07:50:32',0),
	(80,2,'logged in from: ::1','users','2015-04-12 08:48:48',0),
	(81,1,'logged in from: ::1','users','2015-04-12 09:17:04',0),
	(82,1,'logged in from: ::1','users','2015-04-12 11:07:14',0),
	(83,1,'logged in from: 172.16.5.6','users','2015-04-12 07:08:55',0),
	(84,1,'Atualizado registro: 88 | IP: 172.16.5.6 | Data: {\"id_order\":\"112\",\"id_product\":\"13\",\"status\":\"pending\",\"value\":\"6.00\",\"save\":\"Salvar\",\"id\":\"88\"}','order_items','2015-04-12 07:10:14',0),
	(85,1,'Atualizado registro: 181 | IP: 172.16.5.6 | Data: {\"id_order\":\"125\",\"id_product\":\"13\",\"status\":\"pending\",\"value\":\"6.00\",\"save\":\"Salvar\",\"id\":\"181\"}','order_items','2015-04-12 07:13:06',0),
	(86,1,'Atualizado registro: 186 | IP: 172.16.5.6 | Data: {\"id_order\":\"125\",\"id_product\":\"14\",\"status\":\"delivered\",\"value\":\"8.00\",\"save\":\"Salvar\",\"id\":\"186\"}','order_items','2015-04-12 07:13:19',0),
	(87,1,'Atualizado registro: 314 | IP: 172.16.5.6 | Data: {\"id_order\":\"141\",\"id_product\":\"14\",\"status\":\"pending\",\"value\":\"8.00\",\"save\":\"Salvar\",\"id\":\"314\"}','order_items','2015-04-12 07:15:52',0),
	(88,1,'Atualizado registro: 309 | IP: 172.16.5.6 | Data: {\"id_order\":\"141\",\"id_product\":\"13\",\"status\":\"new\",\"value\":\"7.00\",\"save\":\"Salvar\",\"id\":\"309\"}','order_items','2015-04-12 07:16:17',0),
	(89,1,'Atualizado registro: 309 | IP: 172.16.5.6 | Data: {\"id_order\":\"141\",\"id_product\":\"13\",\"status\":\"delivered\",\"value\":\"7.00\",\"save\":\"Salvar\",\"id\":\"309\"}','order_items','2015-04-12 07:17:46',0),
	(90,1,'Atualizado registro: 323 | IP: 172.16.5.6 | Data: {\"id_order\":\"142\",\"id_product\":\"15\",\"status\":\"pending\",\"value\":\"8.00\",\"save\":\"Salvar\",\"id\":\"323\"}','order_items','2015-04-12 07:20:04',0),
	(91,1,'Atualizado registro: 321 | IP: 172.16.5.6 | Data: {\"id_order\":\"142\",\"id_product\":\"14\",\"status\":\"delivered\",\"value\":\"8.00\",\"save\":\"Salvar\",\"id\":\"321\"}','order_items','2015-04-12 07:20:39',0),
	(92,1,'logged in from: ::1','users','2015-04-12 13:30:56',0);

/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table calls
# ------------------------------------------------------------

DROP TABLE IF EXISTS `calls`;

CREATE TABLE `calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_order` int(11) NOT NULL,
  `attended` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `calls` WRITE;
/*!40000 ALTER TABLE `calls` DISABLE KEYS */;

INSERT INTO `calls` (`id`, `created_on`, `modified_on`, `id_order`, `attended`)
VALUES
	(48,'2015-04-12 04:43:16','0000-00-00 00:00:00',93,0),
	(49,'2015-04-12 05:00:14','0000-00-00 00:00:00',115,0),
	(50,'2015-04-12 05:00:17','0000-00-00 00:00:00',115,0),
	(51,'2015-04-12 05:00:20','0000-00-00 00:00:00',115,0),
	(52,'2015-04-12 05:00:23','0000-00-00 00:00:00',115,0),
	(53,'2015-04-12 06:18:12','0000-00-00 00:00:00',130,0),
	(54,'2015-04-12 06:18:15','0000-00-00 00:00:00',130,0),
	(55,'2015-04-12 06:18:18','0000-00-00 00:00:00',130,0),
	(56,'2015-04-12 06:18:21','0000-00-00 00:00:00',130,0),
	(57,'2015-04-12 06:18:23','0000-00-00 00:00:00',130,0),
	(58,'2015-04-12 06:18:26','0000-00-00 00:00:00',130,0),
	(59,'2015-04-12 06:18:29','0000-00-00 00:00:00',130,0),
	(60,'2015-04-12 06:18:32','0000-00-00 00:00:00',130,0),
	(61,'2015-04-12 06:18:35','0000-00-00 00:00:00',130,0),
	(62,'2015-04-12 06:18:38','0000-00-00 00:00:00',130,0),
	(63,'2015-04-12 06:18:41','0000-00-00 00:00:00',130,0),
	(64,'2015-04-12 06:18:46','0000-00-00 00:00:00',130,0),
	(65,'2015-04-12 06:18:49','0000-00-00 00:00:00',130,0),
	(66,'2015-04-12 06:18:53','0000-00-00 00:00:00',130,0),
	(67,'2015-04-12 06:18:56','0000-00-00 00:00:00',130,0),
	(68,'2015-04-12 06:18:59','0000-00-00 00:00:00',130,0),
	(69,'2015-04-12 06:19:02','0000-00-00 00:00:00',130,0),
	(70,'2015-04-12 06:22:04','0000-00-00 00:00:00',131,0),
	(71,'2015-04-12 06:22:08','0000-00-00 00:00:00',131,0),
	(72,'2015-04-12 06:22:12','0000-00-00 00:00:00',131,0),
	(73,'2015-04-12 06:22:16','0000-00-00 00:00:00',131,0),
	(74,'2015-04-12 06:22:20','0000-00-00 00:00:00',131,0),
	(75,'2015-04-12 06:22:24','0000-00-00 00:00:00',131,0),
	(76,'2015-04-12 06:25:24','0000-00-00 00:00:00',131,0),
	(77,'2015-04-12 06:58:23','0000-00-00 00:00:00',140,0),
	(78,'2015-04-12 06:58:26','0000-00-00 00:00:00',140,0),
	(79,'2015-04-12 06:58:29','0000-00-00 00:00:00',140,0),
	(80,'2015-04-12 06:58:32','0000-00-00 00:00:00',140,0),
	(81,'2015-04-12 06:58:35','0000-00-00 00:00:00',140,0),
	(82,'2015-04-12 06:58:38','0000-00-00 00:00:00',140,0),
	(83,'2015-04-12 06:58:41','0000-00-00 00:00:00',140,0),
	(84,'2015-04-12 06:58:43','0000-00-00 00:00:00',140,0),
	(85,'2015-04-12 06:58:46','0000-00-00 00:00:00',140,0),
	(86,'2015-04-12 06:58:49','0000-00-00 00:00:00',140,0),
	(87,'2015-04-12 06:58:53','0000-00-00 00:00:00',140,0),
	(88,'2015-04-12 06:58:56','0000-00-00 00:00:00',140,0);

/*!40000 ALTER TABLE `calls` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table content_languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `content_languages`;

CREATE TABLE `content_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `key` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `content_languages` WRITE;
/*!40000 ALTER TABLE `content_languages` DISABLE KEYS */;

INSERT INTO `content_languages` (`id`, `created_on`, `modified_on`, `key`, `name`)
VALUES
	(1,'0000-00-00 00:00:00','0000-00-00 00:00:00','pt','Português');

/*!40000 ALTER TABLE `content_languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table email_queue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_queue`;

CREATE TABLE `email_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_email` varchar(128) NOT NULL,
  `reply_to` varchar(128) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `alt_message` text,
  `max_attempts` int(11) NOT NULL DEFAULT '3',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  `date_published` datetime DEFAULT NULL,
  `last_attempt` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table establishments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `establishments`;

CREATE TABLE `establishments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(100) NOT NULL,
  `code` varchar(10) NOT NULL,
  `active` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `establishments` WRITE;
/*!40000 ALTER TABLE `establishments` DISABLE KEYS */;

INSERT INTO `establishments` (`id`, `created_on`, `modified_on`, `name`, `code`, `active`)
VALUES
	(1,'0000-00-00 00:00:00','2015-04-12 07:50:32','Rapach','1',1),
	(2,'0000-00-00 00:00:00','2015-04-12 03:30:31','Kings Kão','2',1),
	(3,'2015-04-12 03:33:33','0000-00-00 00:00:00','Cavanhas','cv1n',1);

/*!40000 ALTER TABLE `establishments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table establishments_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `establishments_users`;

CREATE TABLE `establishments_users` (
  `establishments_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `establishments_users` WRITE;
/*!40000 ALTER TABLE `establishments_users` DISABLE KEYS */;

INSERT INTO `establishments_users` (`establishments_id`, `users_id`)
VALUES
	(2,2);

/*!40000 ALTER TABLE `establishments_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table identifiers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `identifiers`;

CREATE TABLE `identifiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_establishment` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `active` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `identifiers` WRITE;
/*!40000 ALTER TABLE `identifiers` DISABLE KEYS */;

INSERT INTO `identifiers` (`id`, `created_on`, `modified_on`, `id_establishment`, `name`, `code`, `active`)
VALUES
	(1,'2015-04-11 21:05:57','0000-00-00 00:00:00',2,'Mesa 1','km2',1),
	(2,'2015-04-11 21:06:35','0000-00-00 00:00:00',1,'Mesa 2','rm2',1);

/*!40000 ALTER TABLE `identifiers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) NOT NULL,
  `login` varchar(50) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table order_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order_items`;

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_order` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;

INSERT INTO `order_items` (`id`, `created_on`, `modified_on`, `id_order`, `id_product`, `status`, `value`)
VALUES
	(396,'2015-04-12 09:14:17','0000-00-00 00:00:00',166,1,'new',3.00),
	(397,'2015-04-12 09:14:18','0000-00-00 00:00:00',166,1,'new',3.00),
	(398,'2015-04-12 09:14:19','0000-00-00 00:00:00',166,7,'new',12.50),
	(399,'2015-04-12 09:14:38','0000-00-00 00:00:00',166,8,'new',15.50),
	(400,'2015-04-12 09:14:39','0000-00-00 00:00:00',166,8,'new',15.50),
	(401,'2015-04-12 09:17:01','0000-00-00 00:00:00',169,14,'new',8.00),
	(402,'2015-04-12 09:17:02','0000-00-00 00:00:00',169,14,'new',8.00),
	(403,'2015-04-12 09:17:02','0000-00-00 00:00:00',169,15,'new',8.00),
	(404,'2015-04-12 09:18:08','0000-00-00 00:00:00',170,2,'new',4.00),
	(405,'2015-04-12 09:18:09','0000-00-00 00:00:00',170,2,'new',4.00),
	(406,'2015-04-12 09:18:09','0000-00-00 00:00:00',170,2,'new',4.00),
	(407,'2015-04-12 09:18:10','0000-00-00 00:00:00',170,3,'new',7.00),
	(408,'2015-04-12 09:18:11','0000-00-00 00:00:00',170,3,'new',7.00),
	(409,'2015-04-12 09:18:12','0000-00-00 00:00:00',170,3,'new',7.00),
	(410,'2015-04-12 09:18:12','0000-00-00 00:00:00',170,3,'new',7.00),
	(411,'2015-04-12 09:18:13','0000-00-00 00:00:00',170,3,'new',7.00),
	(412,'2015-04-12 09:18:14','0000-00-00 00:00:00',170,12,'new',8.00),
	(413,'2015-04-12 09:18:15','0000-00-00 00:00:00',170,12,'new',8.00),
	(414,'2015-04-12 10:15:23','0000-00-00 00:00:00',171,2,'new',4.00),
	(415,'2015-04-12 10:15:24','0000-00-00 00:00:00',171,2,'new',4.00),
	(416,'2015-04-12 10:15:24','0000-00-00 00:00:00',171,3,'new',7.00),
	(417,'2015-04-12 10:15:25','0000-00-00 00:00:00',171,12,'new',8.00),
	(418,'2015-04-12 10:15:26','0000-00-00 00:00:00',171,12,'new',8.00);

/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_identifier` int(11) NOT NULL,
  `device_code` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `created_on`, `modified_on`, `id_identifier`, `device_code`, `status`)
VALUES
	(166,'2015-04-12 09:13:45','0000-00-00 00:00:00',2,'353918058614112','open'),
	(167,'2015-04-12 09:14:53','0000-00-00 00:00:00',2,'353918058614112','open'),
	(168,'2015-04-12 09:16:32','0000-00-00 00:00:00',2,'353918058614112','open'),
	(169,'2015-04-12 09:16:49','0000-00-00 00:00:00',1,'353918058614112','open'),
	(170,'2015-04-12 09:17:30','0000-00-00 00:00:00',2,'353918058614112','open'),
	(171,'2015-04-12 10:15:05','0000-00-00 00:00:00',2,'353918058614112','open'),
	(172,'2015-04-12 10:42:20','0000-00-00 00:00:00',2,'353918058614112','open');

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`permission_id`, `name`, `description`, `status`)
VALUES
	(1,'Site.Content.View','Permite aos usuários visualizar o contexto \"Conteúdo\"','active'),
	(2,'Site.Reports.View','Allow users to view the Reports Context','active'),
	(3,'Site.Settings.View','Allow users to view the Settings Context','active'),
	(4,'Site.Developer.View','Allow users to view the Developer Context','active'),
	(5,'WTche.Roles.Manage','Allow users to manage the user Roles','active'),
	(6,'WTche.Users.Manage','Allow users to manage the site Users','active'),
	(7,'WTche.Users.View','Allow users access to the User Settings','active'),
	(8,'WTche.Users.Add','Allow users to add new Users','active'),
	(9,'WTche.Database.Manage','Allow users to manage the Database settings','active'),
	(10,'WTche.Emailer.Manage','Allow users to manage the Emailer settings','active'),
	(11,'WTche.Logs.View','Allow users access to the Log details','active'),
	(12,'WTche.Logs.Manage','Allow users to manage the Log files','active'),
	(13,'WTche.Emailer.View','Allow users access to the Emailer settings','active'),
	(14,'Site.Signin.Offline','Allow users to login to the site when the site is offline','active'),
	(15,'WTche.Permissions.View','Allow access to view the Permissions menu unders Settings Context','active'),
	(16,'WTche.Permissions.Manage','Allow access to manage the Permissions in the system','active'),
	(17,'WTche.Roles.Delete','Allow users to delete user Roles','active'),
	(18,'WTche.Modules.Add','Allow creation of modules with the builder.','active'),
	(19,'WTche.Modules.Delete','Allow deletion of modules.','active'),
	(20,'Permissions.Administrador.Manage','To manage the access control permissions for the Administrator role.','active'),
	(21,'Permissions.Usuário.Manage','To manage the access control permissions for the Developer role.','inactive'),
	(22,'Activities.Own.View','To view the users own activity logs','active'),
	(23,'Activities.Own.Delete','To delete the users own activity logs','active'),
	(24,'Activities.User.View','To view the user activity logs','active'),
	(25,'Activities.User.Delete','To delete the user activity logs, except own','active'),
	(26,'Activities.Module.View','To view the module activity logs','active'),
	(27,'Activities.Module.Delete','To delete the module activity logs','active'),
	(28,'Activities.Date.View','To view the users own activity logs','active'),
	(29,'Activities.Date.Delete','To delete the dated activity logs','active'),
	(30,'WTche.Settings.View','To view the site settings page.','active'),
	(31,'WTche.Settings.Manage','To manage the site settings.','active'),
	(32,'WTche.Activities.View','To view the Activities menu.','active'),
	(33,'WTche.Database.View','To view the Database menu.','active'),
	(34,'WTche.Migrations.View','To view the Migrations menu.','active'),
	(35,'WTche.Builder.View','To view the Modulebuilder menu.','active'),
	(36,'WTche.Roles.View','To view the Roles menu.','active'),
	(37,'WTche.Sysinfo.View','To view the System Information page.','active'),
	(38,'WTche.Translate.Manage','To manage the Language Translation.','active'),
	(39,'WTche.Translate.View','To view the Language Translate menu.','active'),
	(40,'WTche.Profiler.View','To view the Console Profiler Bar.','active'),
	(41,'WTche.Roles.Add','To add New Roles','active'),
	(42,'WTche.Module.Builder','To create new Modules','active'),
	(43,'Analytics.Settings.View','','active'),
	(44,'Analytics.Settings.Manage','','active'),
	(45,'Content_languages.Settings.View','','active'),
	(46,'Content_languages.Settings.Create','','active'),
	(47,'Content_languages.Settings.Edit','','active'),
	(48,'Content_languages.Settings.Delete','','active'),
	(49,'Seo.Settings.View','','active'),
	(50,'Seo.Settings.Create','','active'),
	(51,'Seo.Settings.Edit','','active'),
	(52,'Seo.Settings.Delete','','active'),
	(53,'Seo.Settings.Manage','','active'),
	(78,'Establishments.Content.View','','active'),
	(79,'Establishments.Content.Create','','active'),
	(80,'Establishments.Content.Edit','','active'),
	(81,'Establishments.Content.Delete','','active'),
	(82,'Products_categories.Content.View','','active'),
	(83,'Products_categories.Content.Create','','active'),
	(84,'Products_categories.Content.Edit','','active'),
	(85,'Products_categories.Content.Delete','','active'),
	(86,'Products.Content.View','','active'),
	(87,'Products.Content.Create','','active'),
	(88,'Products.Content.Edit','','active'),
	(89,'Products.Content.Delete','','active'),
	(90,'Permissions.Gerente.Manage','To manage the access control permissions for the Gerente role.','active'),
	(91,'Permissions.Garçon.Manage','To manage the access control permissions for the Garçon role.','active'),
	(92,'Permissions.Cozinha.Manage','To manage the access control permissions for the Cozinha role.','active'),
	(93,'Identifiers.Content.View','','active'),
	(94,'Identifiers.Content.Create','','active'),
	(95,'Identifiers.Content.Edit','','active'),
	(96,'Identifiers.Content.Delete','','active'),
	(101,'Orders.Content.View','','active'),
	(102,'Orders.Content.Create','','active'),
	(103,'Orders.Content.Edit','','active'),
	(104,'Orders.Content.Delete','','active'),
	(105,'Order_items.Content.View','','active'),
	(106,'Order_items.Content.Create','','active'),
	(107,'Order_items.Content.Edit','','active'),
	(108,'Order_items.Content.Delete','','active'),
	(109,'Calls.Content.View','','active'),
	(110,'Calls.Content.Create','','active'),
	(111,'Calls.Content.Edit','','active'),
	(112,'Calls.Content.Delete','','active'),
	(113,'Establishments.Manager','Pode gerenciar os Estabelecimentos','active');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_product_category` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `active` smallint(6) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `created_on`, `modified_on`, `id_product_category`, `name`, `value`, `active`, `description`)
VALUES
	(1,'2015-04-11 15:52:37','0000-00-00 00:00:00',1,'Agua com gás',3.00,1,''),
	(2,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Coca-cola',4.00,1,''),
	(3,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Cerveja Polar',7.00,1,''),
	(4,'0000-00-00 00:00:00','2015-04-12 01:01:29',2,'Batata frita',10.00,1,'Porção para 1 pessoa, acompanha pedaços de calabresa e queijo.'),
	(5,'0000-00-00 00:00:00','2015-04-11 16:17:56',2,'Iscas de filé',18.00,1,''),
	(6,'0000-00-00 00:00:00','0000-00-00 00:00:00',2,'Polenta',15.00,1,''),
	(7,'0000-00-00 00:00:00','0000-00-00 00:00:00',3,'Xis salada',12.50,1,''),
	(8,'0000-00-00 00:00:00','0000-00-00 00:00:00',3,'Xis picanha',15.50,1,''),
	(9,'0000-00-00 00:00:00','0000-00-00 00:00:00',3,'Xis coração',14.50,1,''),
	(10,'0000-00-00 00:00:00','0000-00-00 00:00:00',3,'Cachorro quente simples',6.90,1,''),
	(11,'0000-00-00 00:00:00','0000-00-00 00:00:00',3,'Cachorro quente completo',8.90,1,''),
	(12,'0000-00-00 00:00:00','0000-00-00 00:00:00',1,'Cerveja Bohemia',8.00,1,''),
	(13,'0000-00-00 00:00:00','0000-00-00 00:00:00',4,'Ceva',6.00,1,''),
	(14,'0000-00-00 00:00:00','0000-00-00 00:00:00',5,'Di gado',8.00,1,''),
	(15,'0000-00-00 00:00:00','0000-00-00 00:00:00',5,'Di frangu',8.00,1,'');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_categories`;

CREATE TABLE `products_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_establishment` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products_categories` WRITE;
/*!40000 ALTER TABLE `products_categories` DISABLE KEYS */;

INSERT INTO `products_categories` (`id`, `created_on`, `modified_on`, `id_establishment`, `name`, `active`)
VALUES
	(1,'2015-04-11 15:46:49','0000-00-00 00:00:00',1,'Bebidas',1),
	(2,'2015-04-11 15:48:49','0000-00-00 00:00:00',1,'Entradas',1),
	(3,'2015-04-11 15:49:26','2015-04-11 15:50:09',1,'Principais',1),
	(4,'0000-00-00 00:00:00','0000-00-00 00:00:00',2,'Entradas 2 ',1),
	(5,'0000-00-00 00:00:00','0000-00-00 00:00:00',2,'Hamburguer',1);

/*!40000 ALTER TABLE `products_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_permissions`;

CREATE TABLE `role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `role_permissions` WRITE;
/*!40000 ALTER TABLE `role_permissions` DISABLE KEYS */;

INSERT INTO `role_permissions` (`role_id`, `permission_id`)
VALUES
	(1,1),
	(1,2),
	(1,3),
	(1,4),
	(1,5),
	(1,6),
	(1,7),
	(1,8),
	(1,9),
	(1,10),
	(1,11),
	(1,12),
	(1,13),
	(1,14),
	(1,15),
	(1,16),
	(1,17),
	(1,18),
	(1,19),
	(1,20),
	(1,21),
	(1,22),
	(1,23),
	(1,24),
	(1,25),
	(1,26),
	(1,27),
	(1,28),
	(1,29),
	(1,30),
	(1,31),
	(1,32),
	(1,33),
	(1,34),
	(1,35),
	(1,36),
	(1,37),
	(1,38),
	(1,39),
	(1,40),
	(1,41),
	(1,42),
	(1,43),
	(1,44),
	(1,45),
	(1,46),
	(1,47),
	(1,48),
	(1,49),
	(1,50),
	(1,51),
	(1,52),
	(1,53),
	(1,78),
	(1,79),
	(1,80),
	(1,81),
	(1,82),
	(1,83),
	(1,84),
	(1,85),
	(1,86),
	(1,87),
	(1,88),
	(1,89),
	(1,90),
	(1,91),
	(1,92),
	(1,93),
	(1,94),
	(1,95),
	(1,96),
	(1,101),
	(1,102),
	(1,103),
	(1,104),
	(1,105),
	(1,106),
	(1,107),
	(1,108),
	(1,109),
	(1,110),
	(1,111),
	(1,112),
	(1,113),
	(3,1),
	(3,78),
	(3,80),
	(3,82),
	(3,83),
	(3,84),
	(3,85),
	(3,86),
	(3,87),
	(3,88),
	(3,89),
	(3,93),
	(3,94),
	(3,95),
	(3,96),
	(3,101),
	(3,102),
	(3,103),
	(3,104),
	(3,105),
	(3,106),
	(3,107),
	(3,108);

/*!40000 ALTER TABLE `role_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '1',
  `login_destination` varchar(255) NOT NULL DEFAULT '/',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `default_context` varchar(255) NOT NULL DEFAULT 'content',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`role_id`, `role_name`, `description`, `default`, `can_delete`, `login_destination`, `deleted`, `default_context`)
VALUES
	(1,'Administrador','Has full control over every aspect of the site.',0,0,'/manager',0,'content'),
	(2,'Usuário','This is the default user with access to login.',1,1,'/manager',0,'content'),
	(3,'Gerente','Pode Alterar todos os Produtos e Valores',0,0,'',0,'reports'),
	(4,'Garçon','Recebe os Pedidos e Pode Alterar Status dos Itens',0,0,'',0,'reports'),
	(5,'Cozinha','Pode alterar o status dos itens (Preparando, Preparado)',0,0,'',0,'reports');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table schema_version
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schema_version`;

CREATE TABLE `schema_version` (
  `type` varchar(20) NOT NULL,
  `version` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `schema_version` WRITE;
/*!40000 ALTER TABLE `schema_version` DISABLE KEYS */;

INSERT INTO `schema_version` (`type`, `version`)
VALUES
	('analytics_',1),
	('calls_',2),
	('content_languages_',3),
	('core',1),
	('establishments_',2),
	('identifiers_',2),
	('orders_',2),
	('order_items_',2),
	('products_',3),
	('products_categories_',2),
	('seo_',2);

/*!40000 ALTER TABLE `schema_version` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table seo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `seo`;

CREATE TABLE `seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uri` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(100) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `name` varchar(30) NOT NULL,
  `module` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`name`, `module`, `value`)
VALUES
	('analytics.code','analytics',''),
	('auth.allow_name_change','core','1'),
	('auth.allow_register','core','0'),
	('auth.allow_remember','core','1'),
	('auth.do_login_redirect','core','1'),
	('auth.login_type','core','username'),
	('auth.name_change_frequency','core','1'),
	('auth.name_change_limit','core','1'),
	('auth.password_force_mixed_case','core','0'),
	('auth.password_force_numbers','core','0'),
	('auth.password_force_symbols','core','0'),
	('auth.password_min_length','core','8'),
	('auth.password_show_labels','core','0'),
	('auth.remember_length','core','1209600'),
	('auth.user_activation_method','core','0'),
	('auth.use_extended_profile','core','0'),
	('auth.use_usernames','core','1'),
	('mailpath','email','/usr/sbin/sendmail'),
	('mailtype','email','html'),
	('password_iterations','users','8'),
	('protocol','email','smtp'),
	('sender_email','email',''),
	('seo.default_description','seo',''),
	('seo.default_keywords','seo',''),
	('seo.default_title','seo',''),
	('site.languages','core','a:1:{i:0;s:2:\"pt\";}'),
	('site.list_limit','core','25'),
	('site.show_front_profiler','core','0'),
	('site.show_profiler','core','1'),
	('site.status','core','1'),
	('site.system_email','core','luismiguelprs@gmail.com'),
	('site.title','core','Bota na Conta'),
	('smtp_host','email','smtp.wtprime.com.br'),
	('smtp_pass','email','testelocal09'),
	('smtp_port','email','587'),
	('smtp_timeout','email','30'),
	('smtp_user','email','enviosmtp@wtprime.com.br');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_cookies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_cookies`;

CREATE TABLE `user_cookies` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(128) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_meta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_meta`;

CREATE TABLE `user_meta` (
  `meta_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) NOT NULL DEFAULT '',
  `meta_value` text NOT NULL,
  PRIMARY KEY (`meta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `email` varchar(120) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password_hash` char(60) NOT NULL,
  `reset_hash` varchar(40) DEFAULT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_ip` varchar(40) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `reset_by` int(10) DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_message` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) NOT NULL DEFAULT '',
  `display_name_changed` date DEFAULT NULL,
  `timezone` char(4) NOT NULL DEFAULT 'UM3',
  `language` varchar(20) NOT NULL DEFAULT 'pt',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activate_hash` varchar(40) NOT NULL DEFAULT '',
  `password_iterations` int(4) NOT NULL,
  `force_password_reset` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `role_id`, `email`, `username`, `password_hash`, `reset_hash`, `last_login`, `last_ip`, `created_on`, `deleted`, `reset_by`, `banned`, `ban_message`, `display_name`, `display_name_changed`, `timezone`, `language`, `active`, `activate_hash`, `password_iterations`, `force_password_reset`)
VALUES
	(1,1,'joaogbarbosa@gmail.com','admin','$2a$08$PgkLwYcTu3FMiu4OV2.So.rfxy9Vh3vgCdMOivmQNNyGO3GUwpv1C',NULL,'2015-04-12 13:30:55','::1','2015-04-11 12:27:38',0,NULL,0,NULL,'Administrador',NULL,'UM3','pt',1,'',0,0),
	(2,3,'luismiguelprs+gerente@gmail.com','gerente','$2a$08$80BzOer1WUH0xrTVBKF92ee.Egw3/cdT5w2Rh/w6h9lrNKj.8PIbC',NULL,'2015-04-12 08:48:47','::1','2015-04-12 01:41:26',0,NULL,0,NULL,'Gerente',NULL,'UM3','pt',1,'',8,0),
	(3,4,'luismiguelprs+garcon@gmail.com','garcon','$2a$08$4pZwecQBSqSnx179PmQ0he.l.0JcA52wHDDLpQKlLrnTITyWSbgHG',NULL,'0000-00-00 00:00:00','0','2015-04-12 01:42:35',0,NULL,0,NULL,'Garçon',NULL,'UM3','pt',1,'',8,0),
	(4,5,'luismiguelprs+cozinheiro@gmail.com','cozinha','$2a$08$1Ub6gx8qBKVdmsXWphXtzezeXjFfw.EJzi1jOd5lR95fgzsLL5s.a',NULL,'0000-00-00 00:00:00','0','2015-04-12 01:44:27',0,NULL,0,NULL,'Cozinha',NULL,'UM3','pt',1,'',8,0);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
