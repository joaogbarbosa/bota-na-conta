(function($){
    $.fn.wtSelect = function(settings){
        var config = {
            'inputClass' : 'wtCapaSelect',
            'textoClass': 'wtTextoSelect',
            'setaClass': 'wtSetaSelect',
            'ajaxLoaded': null
        };

        if (settings){$.extend(config, settings);}

        return this.each(function(){

            var inputClass = config.inputClass;
            var textoClass = config.textoClass;
            var setaClass  = config.setaClass;
            var ajaxLoaded  = config.ajaxLoaded;

            //cache do select
            var select = $(this).find('select');

            //Pega o placeholder de cada select
            var placeholder = select.find(':selected').html();

            //Se for vazio ou sem o placeholder escreve vazio
            if (placeholder == '' || placeholder == undefined) {
                placeholder = ' ';
            };

            $(this).addClass(''+inputClass+'');
            
            $(this).css('position', 'relative');
            
            //adiciona uma classe onde escreve o nome option selecionado se for passada por parametro
            $(this).append('<span class="'+textoClass+'">'+placeholder+'</span>');

            //cria a setinha 
            $(this).append('<a class="'+setaClass+'"></a>');
            
            //Aplica regras para esconder o select mas deixar sempre por cima+-
            select.css({
               width: '100%',
               height: '100%',
               opacity: 0,
               position: 'absolute',
               top: 0,
               left: 0,
               zIndex: 10
           });
            
            //pega o que está escrito no option e escreve na span
            select.bind('change', function(){
                var selecionado = $(this).find(':selected').html();
                $(this).parent().find('.'+textoClass+'').html(selecionado);
            });

            if (ajaxLoaded != null ) {
                 $(this).bind('change', function(){
                    $(ajaxLoaded).find('select option').eq(0).prop('selected', true);
                    var sel = $(ajaxLoaded).find(':selected').html();
                    $(ajaxLoaded).find('span').html(sel);
                });
            }
        });
};
})(jQuery);
