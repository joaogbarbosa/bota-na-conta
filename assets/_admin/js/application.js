
$(function() {

	/*
		Check All Feature
	*/
	$(document).on('click','.check-all', function(){
		$("table .column-check input[type=checkbox]").prop('checked', $(this).is(':checked')).uniform.update();
	});


	//===== Add fadeIn animation to dropdown =====//

	$('.dropdown, .btn-group').on('show.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).fadeIn(100);
	});


	//===== Add fadeOut animation to dropdown =====//

	$('.dropdown, .btn-group').on('hide.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).fadeOut(100);
	});


	//===== Prevent dropdown from closing on click =====//

	$('.popup').click(function (e) {
		e.stopPropagation();
	});

	//===== Form elements styling =====//
	_ui();

	//===== jGrowl notifications defaults =====//

	$.jGrowl.defaults.closer = false;
	$.jGrowl.defaults.easing = 'easeInOutCirc';


	//===== Collapsible navigation =====//
	
	$('.sidebar-wide li:not(.disabled) .expand, .sidebar-narrow .navigation > li ul .expand').collapsible({
		defaultOpen: 'second-level,third-level',
		cssOpen: 'level-opened',
		cssClose: 'level-closed',
		speed: 150
	});

	//===== Hiding sidebar =====//

	$('.sidebar-toggle').click(function () {
		$('.page-container').toggleClass('sidebar-hidden');
	});


	//===== Disabling main navigation links =====//

	$('.navigation li.disabled a, .navbar-nav > .disabled > a').click(function(e){
		e.preventDefault();
	});

	/* # Data Table
	================================================== */

	//Inicializado na View para poder utilizar parametros do PHP
	
	//===== Pré-Validate Forms =====//
	$('form.pre-validate').submit(function(event){

		event.preventDefault();

		var form = $(this);

		post_data = form.serializeObject();
		post_data['just_validate'] = true;
		post_data['save'] = true;

		$.ajax({
			type: "POST",
			data: post_data,
			url: form.attr('action'),
			success: function(data){
				form.unbind('submit').find('input[type="submit"]').click();
			},
			error: function(xhr){
				console.log(xhr.responseJSON);
				$('label[for]').parents('.form-group').removeClass('has-error');
				$.each(xhr.responseJSON, function(index, value) {
					$('label[for="' + index + '"]').parents('.form-group').addClass('has-error');
				});
			},
			dataType: "json"
		});


	});

});

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function _ui() {

	//===== Tooltip =====//

	$('.tip').tooltip();

	//===== Form elements styling =====//

	$(".styled, .multiselect-container input").uniform({ radioClass: 'choice', selectAutoWidth: false });

	//===== Tooltip =====//

	$('.tip').tooltip();

	//===== Popover =====//

	$("[data-toggle=popover]").popover().click(function(e) {
		e.preventDefault()
	});

	//===== Bootstrap switches =====// 

	$.fn.bootstrapSwitch.defaults.onColor = 'success';
	$.fn.bootstrapSwitch.defaults.offColor = 'danger';
	$('.switch').bootstrapSwitch();

	//===== Selects =====//

	$('select.select2').select2({
		width: "100%"
	});

	//===== File Upload =====//
	$('input[type=file]').bootstrapFileInput();

	
	//===== Lightbox =====//
	$(".lightbox").fancybox({
		'margin': 50
	});

}

function _ui_message(message, type) {

	$('#ui_messages').append(
		$('<div>')
			.hide()
			.addClass('callout callout-' + type)
			.text(message)
			.prepend(
				$('<button>')
					.attr('type','button')
					.addClass('close')
					.attr('data-dismiss','alert')
					.text('×')
			).slideDown(400)
	);

}

function _field_update_status(field, message, type) {

	$.jGrowl(message, { theme: 'growl-' + type });

	if (field) {
		field.parent().removeClass (function (index, css) {
		    return (css.match (/(^|\s)has-\S+/g) || []).join(' ');
		}).addClass('has-' + type);
		setTimeout(function(){
			if (type != 'error')
				field.parent().removeClass('has-' + type);
		},3500);
	}
	
}

function ci_csrf_token()
{
	// Match the config of the same name in application/config/config.php
	var csrf_cookie_name = "ci_csrf_token";

	return readCookie(csrf_cookie_name);

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
}