<?php

    //Assets::clear_cache();

    Assets::add_css( array(

        'bootstrap.css',

    ));

    Assets::add_js( array( 

        'bootstrap.js',

    ), 'external', true);
?>

<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo isset($page_title) ? $page_title .' : ' : ''; ?> <?php if (class_exists('Settings_lib')) e(settings_item('site.title')); else echo 'WTchê'; ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="robots" content="noindex" />
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    
    <?php echo Assets::css(); ?>

    <?php echo Assets::js(); ?>

    <script type="text/javascript">

        var site_url = "<?php echo site_url(); ?>";
        var base_url = "<?php echo base_url(); ?>";

        //Adiciona o hash de csrf_protection em todas as requisições ajax
        $.ajaxSetup({
            beforeSend: function(jqXHR, settings) {
                settings.data = settings.data + "&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>";
            }
        });

    </script>

</head>

<body>


 
