<?php echo theme_view('partials/_header'); ?>
<body class="sidebar-wide">

	<?php echo theme_view('partials/_navbar'); ?>

	<!-- Page container -->
	<div class="page-container">

		<!-- Sidebar -->
		<div class="sidebar collapse">
			<div class="sidebar-content">

				<br>

				<!-- Main navigation -->
				<ul class="navigation">

					<li><a href="<?php echo site_url('manager') ?>"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>
					
					<?php echo Contexts::render_menu('text', 'normal'); ?>
					
				</ul>
				<!-- /main navigation -->

			</div>
		</div>
		<!-- /sidebar -->


		<!-- Page content -->
	 	<div class="page-content">

	 		<?php Template::block('sub_nav', ''); ?>

			<?php echo Template::message(); ?>

			<div id="ui_messages">
				<?php
				if (validation_errors()) :
				?>
					<div class="callout callout-error fade in">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<h5><?php echo lang('module_form_validation_error_title') ?></h5>
						<?php echo validation_errors('','<br>'); ?>
					</div>
				<?php endif; ?>
			</div>

			<?php echo isset($content) ? $content : Template::content(); ?>

			<!-- Footer -->
		    <div class="footer clearfix">
		        <div class="pull-right">Bota na Conta</div>
		    </div>
		    <!-- /footer -->

<?php echo theme_view('partials/_footer'); ?>
