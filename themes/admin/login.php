<?php echo theme_view('partials/_header'); ?>
<body class="full-width page-condensed">

	<?php echo theme_view('partials/_navbar'); ?>

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
	 	<div>

			<div class="login-wrapper">

				<?php echo isset($content) ? $content : Template::content(); ?>

			</div>

	<?php echo theme_view('partials/_footer'); ?>
