<?php

	Assets::clear_cache();

	Assets::add_css( array(
		'bootstrap.css',
		'londinium-theme.css',
		'styles.css',
		'icons.css',
		'validationEngine.jquery.css',
		'bootstrap-switch.css',
		'wtche.css',
	));

	Assets::add_js( array( 

		'jquery.js',
		'jquery-ui.js',

		'bootstrap.js',
		
		'plugins/charts/jquery.sparkline.js',

		'plugins/forms/jquery.uniform.js',
		'plugins/forms/select2.js',
		'plugins/forms/jquery.autosize.js',
		'plugins/forms/jquery.inputlimiter.js',
		'plugins/forms/listbox.js',
		'plugins/forms/multiselect.js',
		'plugins/forms/tags.min.js',
		'plugins/forms/bootstrap-switch.js',

		'plugins/forms/wysihtml5/wysihtml5.js',
		'plugins/forms/wysihtml5/toolbar.js',

		'plugins/interface/daterangepicker.js',
		'plugins/interface/fancybox.js',
		'plugins/interface/moment.js',
		'plugins/interface/jgrowl.js',
		'plugins/interface/jquery.dataTables.js',
		'plugins/interface/jquery.tableTools.js',
		'plugins/interface/fullcalendar.min.js',
		'plugins/interface/timepicker.min.js',
		'plugins/interface/collapsible.min.js',
		'plugins/interface/mousewheel.js',

		'jquery.mask.js',
		'jquery.validationEngine.js',
		'jquery.validationEngine-pt_BR.js',

		'fileupload/load-image.js',
		'fileupload/load-image-ios.js',
		'fileupload/load-image-orientation.js',
		'fileupload/load-image-meta.js',
		'fileupload/load-image-exif.js',
		'fileupload/load-image-exif-map.js',

		'fileupload/jquery.fileupload.js',
		'fileupload/jquery.iframe-transport.js',
		'fileupload/jquery.fileupload-process.js',
		'fileupload/jquery.fileupload-image.js',
		//'fileupload/jquery.fileupload-audio.js',
		//'fileupload/jquery.fileupload-video.js',
		'fileupload/jquery.fileupload-validate.js',

		'bootstrap.file-input.js',

		'bootbox.js',
		
		'application.js',

	), 'external', true);
?>

<!doctype html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php echo isset($toolbar_title) ? $toolbar_title .' : ' : ''; ?> <?php e($this->settings_lib->item('site.title')) ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="robots" content="noindex" />
	
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	
	<?php echo Assets::css(); ?>

	<script type="text/javascript">
  		var site_url = "<?php echo site_url(); ?>";
  		var base_url = "<?php echo base_url(); ?>";
  	</script>
	<?php echo Assets::js(); //NÃO MOVER PARA O FOOTER ?>
	<script type="text/javascript">

		//Limita o número de notificações
		$.jGrowl.defaults.pool = 10;

  		//Adiciona o hash de csrf_protection em todas as requisições ajax
  		$.ajaxSetup({
		   	beforeSend: function(jqXHR, settings) {
		   		if (typeof(settings.data) == 'string') {
		   			settings.data = settings.data + "&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>";
		   		} else if (typeof(settings.data) == 'object') {
		   			settings.data.<?php echo $this->security->get_csrf_token_name(); ?> = '<?php echo $this->security->get_csrf_hash(); ?>';
		   		}
		   	}
		});
	</script>

</head>


 
