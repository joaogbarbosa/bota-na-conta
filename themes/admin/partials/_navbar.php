<!-- Navbar -->
	<div class="navbar navbar-inverse" role="navigation">
		<div class="navbar-header">
			<a href="<?php echo site_url(SITE_AREA) ?>"><img src="<?php echo base_url('assets/_admin/images/bota_na_conta.png') ?>" style="margin-top: 15px; height: 20px;"></a>
			<?php if ($this->auth->is_logged_in()): ?>
				<a class="sidebar-toggle"><i class="icon-paragraph-justify2"></i></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
					<span class="sr-only">Navegação</span>
					<i class="icon-grid3"></i>
				</button>
			<?php endif ?>
			<?php if ($this->auth->is_logged_in()): ?>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar">
					<span class="sr-only">Menu</span>
					<i class="icon-paragraph-justify2"></i>
				</button>
			<?php endif; ?>
		</div>

		<ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
			<?php if ($this->auth->is_logged_in()): ?>
				<li class="user dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<?php echo gravatar_link($current_user->email, 32, null, $current_user->display_name) ?>
						<span><?php echo (isset($current_user->display_name) && !empty($current_user->display_name)) ? $current_user->display_name : ($this->settings_lib->item('auth.use_usernames') ? $current_user->username : $current_user->email); ?></span>
						<i class="caret"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right icons-right">
						<li><a href="<?php echo site_url(SITE_AREA .'/settings/users/edit') ?>"><i class="icon-user"></i> <?php echo lang('bf_user_settings')?></a></li>
						<li><a href="<?php echo site_url('logout'); ?>"><i class="icon-exit"></i> <?php echo lang('bf_action_logout')?></a></li>
					</ul>
				</li>
			<?php endif ?>
		</ul>
	</div>
	<!-- /navbar -->