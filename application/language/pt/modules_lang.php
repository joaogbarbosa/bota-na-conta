<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


//--------------------------------------------------------------------
// Modules 
//--------------------------------------------------------------------
$lang["modules_act_create_record"]			= "Criado registro";
$lang["modules_act_edit_record"]			= "Atualizado registro";
$lang["modules_act_delete_record"]			= "Deletado registro";
$lang["module_manage"]						= "Gerenciar %s";
$lang["module_edit"]						= "Editar";
$lang["module_true"]						= "Sim";
$lang["module_false"]						= "Não";
$lang["module_create"]						= "Criar";
$lang["module_list"]						= "Listar";
$lang["module_new"]							= "Novo";
$lang["module_edit_text"]					= "Edite conforme sua necessidade";
$lang["module_no_records"]					= "Nenhum registro encontrado";
$lang["module_create_new"]					= "Criar";
$lang["module_create_success"]				= "Criado com sucesso!";
$lang["module_create_failure"]				= "Houve um problema ao criar o registro: ";
$lang["module_create_new_button"]			= "Criar";
$lang["module_invalid_id"]					= "ID Inválido!";
$lang["module_edit_success"]				= "Editado com sucesso!";
$lang["module_edit_failure"]				= "Houve um problema ao editar o registro: ";
$lang["module_delete_success"]				= "Registro(s) deletado(s) com sucesso!";
$lang["module_delete_failure"]				= "Houve um problema ao deletar o registro: ";
$lang["module_delete_error"]				= "Você deve selecionar ao menos um registro para deletar!";
$lang["module_actions"]						= "Ações";
$lang["module_cancel"]						= "Cancelar";
$lang["module_delete_record"]				= "Deletar";
$lang["module_delete_confirm"]				= "Tem certeza que deseja excluir este(s) registro(s)?";
$lang["module_edit_heading"]				= "Editar";
$lang["module_action_edit"]					= "Salvar";
$lang["module_action_create"]				= "Criar";
$lang["module_column_created"]				= "Criado";
$lang["module_column_deleted"]				= "Deletado";
$lang["module_column_modified"]				= "Modificado";

$lang["module_form_validation_error_title"]	= "Por favor corrija os seguintes erros:";

$lang["module_action_delete_batch"]			= "Excluir selecionados";
$lang["module_action_delete_batch_title"]	= "Atenção";

$lang["module_action_save"]					= "Salvar";

$lang["datatables_search"]					= "Filtrar";
$lang["datatables_search_placeholder"]		= "Digite para buscar...";
$lang["datatables_lengthMenu"]				= "Mostrar";
$lang["datatables_emptyTable"]				= "Não existem %s cadastrados";
$lang["datatables_zeroRecords"]				= "Nenhum registro encontrado";
$lang["datatables_infoEmpty"]				= "Nenhum registro encontrado";
$lang["datatables_infoFiltered"]			= " (filtrados de um total de _MAX_ registros)";
$lang["datatables_processing"]				= "Processando...";
$lang["datatables_info"]					= "Mostrando de _START_ até _END_ de um total de _TOTAL_ registros";
