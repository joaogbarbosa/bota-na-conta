<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <?php echo Modules::run('seo'); ?>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="canonical" href="<?php echo current_url(); ?>" />

        <meta property="og:locale" content="pt_BR" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="" />
        <meta property="og:description" content="" />
        <meta property="og:url" content="<?php echo current_url(); ?>" />
        <meta property="og:site_name" content="Bota na Conta" />

        <!--[if IE]><link rel="shortcut icon" href="favicon.ico"><![endif]-->
        <link rel="icon" href="favicon.png">

        <?php
            echo Assets::css();
        ?>
    
    </head>
    <body>