<?php echo theme_view('_header'); ?>

<?php Template::block('header','install/_header'); ?>

    <?php if (@$installed): ?>
        <div class="alert alert-warning" role="alert">
            <?php echo $installed ?>
        </div>
    <?php elseif (!@$can_install): ?>
        <div class="well">

            <table class="table table-hover" style="width: 100%;">
                <tbody>
                    <!-- PHP Version -->
                    <tr>
                        <td>Versão do PHP <?php echo $php_min_version ?>+</td>
                        <td style="width: 10em"><?php echo $php_acceptable ? '<span class="label label-success">' : '<span class="label label-danger">'; ?><?php echo $php_version ?></span></td>
                    </tr>
                    <tr>
                        <td>cURL</td>
                        <td><?php echo $curl_enabled ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>'; ?></td>
                    </tr>
                    <?php
                        if (!$curl_enabled || !$php_acceptable) {
                            $can_install = false;
                        }
                    ?>

                    <!-- Folders -->
                    <tr><td colspan="2" class="text-muted" style="text-align: center">Pastas (Permissão de escrita)</td></tr>

                    <?php foreach ($folders as $folder => $perm) :?>
                        <?php 
                            if (!$perm) {
                                $can_install = false;
                            }
                        ?>
                    <tr>
                        <td><?php echo $folder ?></td>
                        <td><?php echo $perm ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>' ?></td>
                    </tr>
                    <?php endforeach; ?>

                    <!-- Files -->
                    <tr><td colspan="2" class="text-muted" style="text-align: center">Arquivos (Permissão de escrita)</td></tr>

                    <?php foreach ($files as $file => $perm) :?>
                        <?php 
                            if (!$perm) {
                                $can_install = false;
                            }
                        ?>
                    <tr>
                        <td><?php echo str_replace('{{ENV}}', ENVIRONMENT, $file) ?></td>
                        <td><?php echo $perm ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>' ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>

        <?php if (@$can_install === false): ?>
            <div class="alert alert-danger" role="alert">
                Por favor verifique os problemas acima e recarregue a página!
            </div>
        <?php else: ?>
            <p style="text-align: right">
                <a href="<?php echo site_url('install/import') ?>" class="btn btn-primary btn-large">Continuar</a>
            </p>
        <?php endif ?>

    <?php endif ?>

    

<?php Template::block('header','install/_footer'); ?>

<?php echo theme_view('_footer'); ?>