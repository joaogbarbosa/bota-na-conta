<?php echo theme_view('_header'); ?>

<?php Template::block('header','install/_header'); ?>

    <?php echo form_open('install/do_install', 'class="form-horizontal"'); ?>

        <?php if (@$import_error): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $import_error ?>
            </div>
        <?php endif ?>

        <h4>Dados da conta Administrador</h4>
        <div class="well">

          <div class="form-group">
            <label class="col-sm-2 control-label">Nome</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="admin_name" value="Administrador" placeholder="Nome">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" name="admin_email" value="programadores@wtprime.com.br" placeholder="Email">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Usuário</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="admin_user" value="admin" placeholder="Usuário">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Senha</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="admin_password" placeholder="wt@...">
            </div>
          </div>

        </div>


        <p>
            <input type="submit" class="btn btn-primary btn-large pull-right" name="importar" value="Instalar">
        </p>

    <?php echo form_close() ?>

<?php Template::block('header','install/_footer'); ?>

<?php echo theme_view('_footer'); ?>