<?php echo theme_view('_header'); ?>

<?php Template::block('header','install/_header'); ?>

    <?php if (@$installed): ?>
        <div class="alert alert-warning" role="alert">
            <?php echo $installed ?>
        </div>
        <a href="<?php echo site_url('install') ?>" class="btn btn-primary btn-large">Voltar</a>
    <?php else: ?>

      <?php echo form_open('install/do_config_db', 'class="form-horizontal"'); ?>

          <?php if (@$error): ?>
              <div class="alert alert-danger" role="alert">
                  <?php echo $error ?>
              </div>
          <?php endif ?>

          <h4>Configurações do Mochinho de Dados</h4>
          <div class="well">

            <div class="form-group">
              <label class="col-sm-2 control-label">Driver</label>
              <div class="col-sm-10">
                <select class="form-control" name="dbdriver">
                  <option value="mysqli">MySQLi</option>
                  <option value="mysql">MySQL (Deprecated)</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Servidor</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="hostname" value="localhost">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Porta</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="port" value="3306">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Usuário</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="username" value="root">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Senha</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="password" value="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Banco</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="database" value="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Prefixo</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="dbprefix" value="wt_">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">DB Debug</label>
              <div class="col-sm-10">
                <select class="form-control" name="db_debug">
                  <option value="yes" <?php echo ($environment == 'development')?'selected="selected"':'' ?>>Sim</option>
                  <option value="no" <?php echo ($environment != 'development')?'selected="selected"':'' ?>>Não</option>
                </select>
              </div>
            </div>

          </div>


          <p>
              <input type="submit" class="btn btn-primary btn-large pull-right"  value="Continuar">
          </p>

      <?php echo form_close() ?>
      
    <?php endif; ?>

<?php Template::block('header','install/_footer'); ?>

<?php echo theme_view('_footer'); ?>