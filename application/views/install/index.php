<?php echo theme_view('_header'); ?>

<?php Template::block('header','install/_header'); ?>

    <div class="jumbotron">
        <h1>Daí Tchê!</h1>
        <p class="lead">Selecione o bolicho em que deseja instalar o framework</p>
        <p>
            <a href="<?php echo site_url('install/environment/development') ?>" class="btn btn-success btn-large">Desenvolvimento</a>
            <a href="<?php echo site_url('install/environment/testing') ?>" class="btn btn-warning btn-large">Teste</a>
            <a href="<?php echo site_url('install/environment/production') ?>" class="btn btn-danger btn-large">Produção</a>
        </p>
    </div>
    

<?php Template::block('header','install/_footer'); ?>

<?php echo theme_view('_footer'); ?>