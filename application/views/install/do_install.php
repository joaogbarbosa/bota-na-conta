<?php echo theme_view('_header'); ?>

<?php Template::block('header','install/_header'); ?>

    <?php if (BF_DID_INSTALL): ?>
        <div class="jumbotron">
            <h1>Mas que taL!</h1>
            <p class="lead">Deu tudo certo! Pode tomar um chima agora ou...</p>
            <p><a class="btn btn-lg btn-success" href="<?php echo site_url('manager') ?>" role="button">Ir para o Manager</a></p>
        </div>
    <?php else: ?>
        <div class="jumbotron">
            <h1 class="text-danger">Barbaridade!</h1>
            <p class="lead">Deu algo errado! Larga o chima e tenta denovo...</p>
            <p><a class="btn btn-lg btn-danger" href="<?php echo site_url('install') ?>" role="button">Voltar</a></p>
        </div>
    <?php endif ?>

   

<?php Template::block('header','install/_footer'); ?>

<?php echo theme_view('_footer'); ?>