<?php echo theme_view('_header'); ?>

<?php Template::block('header','install/_header'); ?>

    <?php echo form_open('install/do_config_app', 'class="form-horizontal"'); ?>

        <?php if (@$error): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $error ?>
            </div>
        <?php endif ?>

        <h4>Configurações do Site</h4>
        <div class="well">

          <div class="form-group">
            <label class="col-sm-4 control-label">Base URL</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="config[base_url]" placeholder="http://">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Index Page</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="config[index_page]" value="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Habilitar compressão GZIP</label>
            <div class="col-sm-8">
              <select class="form-control" name="config[compress_output]">
                <option value="yes">Sim</option>
                <option value="no">Não</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Combinar Arquivos JS</label>
            <div class="col-sm-8">
              <select class="form-control" name="application[assets.js_combine]">
                <option value="yes" <?php echo ($environment != 'development')?'selected="selected"':'' ?>>Sim</option>
                <option value="no" <?php echo ($environment == 'development')?'selected="selected"':'' ?>>Não</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Comprimir Arquivos JS</label>
            <div class="col-sm-8">
              <select class="form-control" name="application[assets.js_minify]">
                <option value="yes" <?php echo ($environment != 'development')?'selected="selected"':'' ?>>Sim</option>
                <option value="no" <?php echo ($environment == 'development')?'selected="selected"':'' ?>>Não</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Combinar Arquivos CSS</label>
            <div class="col-sm-8">
              <select class="form-control" name="application[assets.css_combine]">
                <option value="yes" <?php echo ($environment != 'development')?'selected="selected"':'' ?>>Sim</option>
                <option value="no" <?php echo ($environment == 'development')?'selected="selected"':'' ?>>Não</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Comprimir Arquivos CSS</label>
            <div class="col-sm-8">
              <select class="form-control" name="application[assets.css_minify]">
                <option value="yes" <?php echo ($environment != 'development')?'selected="selected"':'' ?>>Sim</option>
                <option value="no" <?php echo ($environment == 'development')?'selected="selected"':'' ?>>Não</option>
              </select>
            </div>
          </div>

        </div>


        <p>
            <input type="submit" class="btn btn-primary btn-large pull-right"  value="Continuar">
        </p>

    <?php echo form_close() ?>

<?php Template::block('header','install/_footer'); ?>

<?php echo theme_view('_footer'); ?>