<?php echo theme_view('_header'); ?>

<?php Template::block('header','install/_header'); ?>

    <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>

        <?php if (@$import_error): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $import_error ?>
            </div>
        <?php endif ?>

        <h4>Importar SQL</h4>
        <p class="text-muted">Envie o arquivo somente caso queira copiar a estrutura e dados de uma instalação já existente do Framework</p>
        <div class="well">

          <div class="form-group">
            <label class="col-sm-2 control-label">Arquivo</label>
            <div class="col-sm-10">
                <input type="file" name="file" size="20">
                <p class="text-danger">Atenção: importar somente banco que utilizam a mesma versão do Framework!</p>
            </div>
          </div>

        </div>

        <p>
            <input type="submit" class="btn btn-warning btn-large pull-left" name="importar" value="Importar">
            <a href="<?php echo site_url('install/settings') ?>" class="btn btn-primary btn-large pull-right">Criar Banco e Rodar Migrations</a>
        </p>

    <?php echo form_close() ?>

<?php Template::block('header','install/_footer'); ?>

<?php echo theme_view('_footer'); ?>