<?php
/**
 * page_footer.php
 *
 * Author: pixelcave
 *
 * The footer of each page
 *
 */
?>
    <!-- Footer -->
    <footer class="site-footer site-section">
        <div class="container">
            <!-- Footer Links -->
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading">Sobre</h4>
                    <ul class="footer-nav list-inline">
                        <li><a href="about.php">A Empresa</a></li>
                        <li><a href="contact.php">Contato</a></li>
                        <li><a href="contact.php">Suporte</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading">Legal</h4>
                    <ul class="footer-nav list-inline">
                        <li><a href="javascript:void(0)">Licença</a></li>
                        <li><a href="javascript:void(0)">Política de Privacidade</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading">Siga-nos</h4>
                    <ul class="footer-nav footer-nav-social list-inline">
                        <li><a href="https://twitter.com/bota_na_conta"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://bitbucket.org/joaogbarbosa/bota-na-conta"><i class="fa fa-bitbucket"></i></a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h4 class="footer-heading"><span id="year-copy">2015</span> &copy; <a href="http://goo.gl/TDOSuC"><?php echo $template['name']; ?></a></h4>
                    <!-- <ul class="footer-nav list-inline">
                        <li>Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I">pixelcave</a></li>
                    </ul> -->
                </div>
            </div>
            <!-- END Footer Links -->
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>
