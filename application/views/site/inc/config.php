<?php
/**
 * config.php
 *
 * Author: pixelcave
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */

/* Template variables */
$template = array(
    'name'          => 'Bota na conta',
    'author'        => 'Bota na conta',
    'robots'        => 'noindex, nofollow',
    'title'         => 'Bota na conta - O jeito facil de consumir...',
    'description'   => '',
    // true             for a boxed layout
    // false            for a full width layout
    'boxed'         => false,
    'active_page'   => basename($_SERVER['PHP_SELF'])
);

/* Primary navigation array (the primary navigation will be created automatically based on this array) */
$primary_nav = array(
    array(
        'name'  => 'Gerenciador',
        'url'   => 'manager'
    ),
    array(
        'name'  => 'Baixar Aplicativo',
        'url'   => 'app/app.apk',
        'css'   => "destaque"
    ),
);