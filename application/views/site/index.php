<?php include 'inc/config.php'; ?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Home Carousel -->
<div id="home-carousel" class="carousel carousel-home slide" data-ride="carousel" data-interval="5000">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">

        <div class="item active">
            <section class="site-section site-section-light site-section-top themed-background-botanaconta-1">
                <div class="container">
                    <p class="text-center animation-fadeIn360">
                        <img src="<?php echo base_url("assets/site/") ?>/img/botanaconta/b1.png">
                    </p>
                </div>
            </section>
        </div>
        <div class="item ">
            <section class="site-section site-section-light site-section-top themed-background-botanaconta-1">
                <div class="container">
                    <p class="text-center animation-fadeIn360">
                        <img src="<?php echo base_url("assets/site/") ?>/img/botanaconta/b2.png">
                    </p>
                </div>
            </section>
        </div>
        <div class="item ">
            <section class="site-section site-section-light site-section-top themed-background-botanaconta-1">
                <div class="container">
                    <p class="text-center animation-fadeIn360">
                        <img src="<?php echo base_url("assets/site/") ?>/img/botanaconta/b3.png">
                    </p>
                </div>
            </section>
        </div>
        <div class="item ">
            <section class="site-section site-section-light site-section-top themed-background-botanaconta-1">
                <div class="container">
                    <p class="text-center animation-fadeIn360">
                        <img src="<?php echo base_url("assets/site/") ?>/img/botanaconta/b4.png">
                    </p>
                </div>
            </section>
        </div>
        <div class="item">
            <section class="site-section site-section-light site-section-top themed-background-botanaconta-1">
                <div class="container">
                    <p class="text-center animation-fadeIn360">
                        <img src="<?php echo base_url("assets/site/") ?>/img/botanaconta/b5.png">
                    </p>
                </div>
            </section>
        </div>
    </div>
    <!-- END Wrapper for slides -->

    <!-- Controls -->
    <a class="left carousel-control" href="#home-carousel" data-slide="prev">
        <span>
            <i class="fa fa-chevron-left"></i>
        </span>
    </a>
    <a class="right carousel-control" href="#home-carousel" data-slide="next">
        <span>
            <i class="fa fa-chevron-right"></i>
        </span>
    </a>
    <!-- END Controls -->
</div>
<!-- END Home Carousel -->

<!-- Quick Stats -->
<section class="site-content site-section themed-background">
    <div class="container">
        <!-- Stats Row -->
        <!-- CountTo (initialized in js/app.js), for more examples you can check out https://github.com/mhuggins/jquery-countTo -->
        <div class="row" id="counters">
            <div class="col-sm-4">
                <div class="counter site-block">
                    <span data-toggle="countTo" data-to="7000" data-after="+"></span>
                    <small>Consumidores</small>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="counter site-block">
                    <span data-toggle="countTo" data-to="500" data-after="+"></span>
                    <small>Estabelecimentos</small>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="counter site-block">
                    <span data-toggle="countTo" data-to="456" data-after=""></span>
                    <small>Pedidos feitos hoje</small>
                </div>
            </div>
        </div>
        <!-- END Stats Row -->
    </div>
</section>
<!-- END Quick Stats -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>