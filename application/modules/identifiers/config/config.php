<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(

	'description'	=> '',
	'name'			=> humanize(get_module_name(__FILE__)),
	'version'		=> '',
	'author'		=> 'WT Prime',

);