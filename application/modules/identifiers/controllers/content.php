<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class content extends Admin_Controller_WT
{

	public function __construct()
	{
		
		parent::__construct(__CLASS__, get_module_name(__FILE__));

	}

	public function get_list() {
		$this->load->library('Datatables');
		if (!$this->auth->has_permission('Establishments.Manager')) {
			$this->datatables->join('establishments_users','identifiers.id_establishment = establishments_users.establishments_id');
			$this->datatables->where('establishments_users.users_id', $this->auth->user()->id);
		}
		parent::get_list();
	}


}