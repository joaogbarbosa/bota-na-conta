<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Identifiers_model extends WT_Model {

	protected $table_name; //Pupulado no Construct
	protected $key			= "id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";

	protected $log_user 	= FALSE;

	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";

	/*
		Customize the operations of the model without recreating the insert, update,
		etc methods by adding the method names to act as callbacks here.
	 */
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	/*
		For performance reasons, you may require your model to NOT return the
		id of the last inserted row as it is a bit of a slow method. This is
		primarily helpful when running big loops over data.
	 */
	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array("id");

	/**
     * An array of validation rules. This needs to be the same format
     * as validation rules passed to the Form_validation library.
     *
     * @see http://ellislab.com/codeigniter/user-guide/libraries/form_validation.html#validationrulesasarray
     */
    protected $validation_rules = array();

    /**
     * @var Array Additional Validation rules only used on insert
     */
    protected $insert_validation_rules = array();

	protected $skip_validation 			= FALSE;

	protected $module_name;

	//--------------------------------------------------------------------

	public function __construct()
	{

		parent::__construct();

		$this->module_name = get_module_name(__FILE__);

		$this->table_name = $this->module_name;

		$this->lang->load($this->module_name,'',FALSE,TRUE,'',$this->module_name);

		$this->load->library('users/auth');

		if ($this->auth->is_logged_in() && !$this->auth->has_permission('Establishments.Manager')) {
			$where = 'establishments.id IN (SELECT establishments_id FROM establishments_users WHERE users_id = ' . $this->auth->user()->id . ')';
		}

		$this->wtche->form->addField('select')
			->setFieldName('id_establishment')
			->setRelation('id', 'establishments', 'name', isset($where)?$where:null)
			->setLabel('Establishment');

		$this->wtche->form->addField('char')
			->setFieldName('name')
			->setMaxLength(100)
			->setValidationRules('required');

		$this->wtche->form->addField('char')
			->setFieldName('code')
			->setMaxLength(100)
			->setValidationRules('required');
		
		$this->wtche->form->addField('switcher')
			->setFieldName('active')
			->setLabelOn('Ativo')
			->setLabelOff('Inativo')
			->setDefaultOn()
			->setInlineEdit()
			->setValidationRules('numeric');


	}

}
