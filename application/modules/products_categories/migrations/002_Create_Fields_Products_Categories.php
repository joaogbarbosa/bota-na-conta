<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Create_Fields_Products_Categories extends Migration
{

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name; //Pupulado no Construct

	/**
	 * The table's fields
	 *
	 * @var Array
	 */
	private $fields = array(
		'id_establishment' => array(
			'type' => 'INT',
			'constraint' => 11
		),
		'name' => array(
			'type' => 'VARCHAR',
			'constraint' => 100
		),
		'active' => array(
			'type' => 'SMALLINT'
		),
	);

	//--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->table_name = get_module_name(__FILE__);

	}

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$this->dbforge->add_column($this->table_name, $this->fields);
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->fields as $key => $field)
		{
			$this->dbforge->drop_column($this->table_name, $key);
		}

	}

	//--------------------------------------------------------------------

}