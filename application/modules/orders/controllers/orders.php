<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends Base_Controller
{

	public function open_order($identifier_code,$device_code)
	{

		$this->load->model('orders/orders_model');
		$this->load->model('identifiers/identifiers_model');

		$identifier = reset($this->identifiers_model->find_all_by('code', $identifier_code));


		return $this->orders_model->insert(array(
		    'id_identifier'     => $identifier->id,
		    'device_code'  => $device_code,
		    'status'     => 'open'
		));
	}

	public function close_order($device_code)
	{
		$this->load->model('orders/orders_model');

		$order = $this->orders_model->get_last_open_order($device_code);

		return $this->orders_model->update($order->id, array(
		    'status'     => 'paid'
		));
	}

	public function get_last($device_code,$identifier_code = null) {
		$this->load->model('orders/orders_model');
		return $this->orders_model->get_last_open_order($device_code,$identifier_code);
	}

}