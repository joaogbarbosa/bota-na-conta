<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Calls extends Base_Controller
{

	public function call($device_code)
	{
		
		$this->load->model('orders/orders_model');
		$this->load->model('calls/calls_model');

		$order = $this->orders_model->get_last_open_order($device_code);

		return $this->calls_model->insert(array(
		    'id_order' => $order->id,
		    'attended' => 0
		));
		
	}

}