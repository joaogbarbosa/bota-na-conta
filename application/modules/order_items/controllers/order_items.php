<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_Items extends Base_Controller
{

	public function add($id_product,$device_code)
	{

		$this->load->model('orders/orders_model');
		$this->load->model('products/products_model');
		$this->load->model('order_items/order_items_model');

		$order = $this->orders_model->get_last_open_order($device_code);
		$product = $this->products_model->find($id_product);

		return $this->order_items_model->insert(array(
		    'id_order'     => $order->id,
		    'id_product'  => $product->id,
		    'status' => 'new',
		    'value' => $product->value
		));

	}

}