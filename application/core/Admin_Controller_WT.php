<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Controller responsável pelos formulários do painel de administração do site (Manager)
 *
 * @author Luis MIguel Pires <luis@wtagencia.com.br>
 */
class Admin_Controller_WT extends Admin_Controller
{

    /**
     * Armazena o nome do módulo que está carregado atualmente
     * @var string
     */
    public $module_name;

    /**
     * Armazena o nome do contexto que está carregado atualmente
     * @var string
     */
    public $context_name;
    
    /**
     * Armazena a instância do CodeIgniter para possibilitar a utilização das libraries compartilhadas entre
     * Models, Views, Controllers e próprias Libraries.
     * 
     * @var CI Object //$this->wtche =& get_instance();
     */
    public $wtche;

    /**
     * Armazena a query string para propagação automatica na navegação
     * @var string
     */
    public $query_string;

    /**
     * Recebe dois parâmetros que devem ser passados pelo módulo que está extendendo-o
     * @param string $context_name
     * @param string $module_name
     */
    public function __construct($context_name, $module_name) {
        
        parent::__construct();

        $this->context_name = $context_name;
        $this->module_name = $module_name;
        
        Template::set('context_name', $this->context_name);
        Template::set('module_name', $this->module_name);

        $this->module_config = module_config($this->module_name);

        Template::set('module_title', $this->module_config['name']);

        $this->wtche =& get_instance();

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.View');
        $this->load->model($this->module_name . '/' . $this->module_name . '_model', null, true);
        //$this->lang->load($this->module_name);

        $this->query_string = $_SERVER['QUERY_STRING'];
        Template::set('query_string', $this->query_string);

        Template::set('key', $this->wtche->form->model->get_key());

        Template::set('can_delete', $this->auth->has_permission(ucfirst($this->module_name) . '.' . ucfirst($this->context_name) . '.Delete'));
        Template::set('can_edit', $this->auth->has_permission(ucfirst($this->module_name) . '.' . ucfirst($this->context_name) . '.Edit'));

        //SubNav Defaut
        Template::set_block('sub_nav', 'forms/_sub_nav');
       
    }

    /**
     * Tela de listagem do form
     * @return void
     */
    public function index(){

        // Deleting anything?
        if (isset($_POST['delete']))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    // Log the activity
                    log_activity($this->current_user->id, lang('modules_act_delete_record') .': '. $pid .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($this->{$this->module_name.'_model'}->find($pid)), $this->module_name);
                    
                    $result = $this->{$this->module_name.'_model'}->delete($pid);

                }

                if ($result)
                {
                    Template::set_message(count($checked) .' '. lang('module_delete_success'), 'success');
                }
                else
                {
                    Template::set_message(lang('module_delete_failure') . $this->{$this->module_name.'_model'}->error, 'error');
                }
            }
        }

        Template::set('toolbar_title', sprintf(lang('module_manage'), $this->module_config['name']));

        //Carrega os fields
        Template::set('fields', $this->wtche->form->getFields('list'));


        Template::set_view('forms/index');

        if ($this->input->get('template')) {
            Template::render($this->input->get('template'));
        } else {
            Template::render();
        }
    }

    /**
     * Retorna os dados do módulo formatados para o DataTables
     * @return void Retorno via AJAX
     */
    public function get_list(){

        $this->output->enable_profiler(FALSE);
        
        $this->load->library('Datatables');

        //Busca os Fields para Listagem
        $fields = $this->wtche->form->getFields('list');

        //Adicionamos um a um no Select
        foreach ($fields as $field) {
            if (!$field->isStatic() && $this->db->field_exists($field->fieldName(), $this->{$this->module_name.'_model'}->get_table())) {
                $this->datatables->select($this->db->dbprefix . $this->{$this->module_name.'_model'}->get_table() . '.' . $field->fieldName());
            } elseif ($field->isStatic() && $field->value()) {
                //Caso o campo não seja do Banco vamos adicionar o valor fixo na consulta para poder ser filtrado e tudo mais
                $this->datatables->select('"' . $this->db->dbprefix . $this->{$this->module_name.'_model'}->get_table() . '.' . $field->value() . '" as ' . $field->fieldName(), FALSE);
            }
        }

        $this->datatables->from($this->db->dbprefix . $this->{$this->module_name.'_model'}->get_table());

        //Caso tenha algum parent filtrado:
        $filter = unserialize(base64_decode($this->input->get('filter')));
        if ($filter) {
            foreach ($filter as $field => $value) {
               $this->datatables->where($field, $value);
            }
        }

        $datatable = json_decode($this->datatables->generate());

        //Percorre todos os registros para trazer o value de acordo com cada Field
        //Ex: Caso seja um select o value será um array (chave=>valor)
        foreach ($datatable->data as &$row) {
            foreach ($fields as $field) {

                //Caso o atributo não exista (Não é um campo do banco) iremos inclui-lo ao $row
                if (!isset($row->{$field->fieldName()})) {
                    $row = (object) array_merge( (array)$row, array( $field->fieldName() => 0 ) );
                }

                //Joga o value do banco pra dentro do Field
                $field->setValue($row->{$field->fieldName()}, $row->{$this->{$this->module_name.'_model'}->get_key()});

                //E então faz a mágica de acordo com cada Field =)
                $row->{$field->fieldName()} = $field->value();

            }
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($datatable));

    }

    /**
     * Retorna os dados de um registro específico de file (Pode ser usado para mais coisas?)
     * @return void Retorno via AJAX
     */
    public function get_file($field_name, $key){

        $this->output->enable_profiler(FALSE);
        
        $field = $this->wtche->form->getField($field_name);

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($this->db->from($field->get_table())->where($field->model->get_key(),$key)->get()->row()));

    }

    /**
     * Salva os dados enviados através de Ajax 
     * @param  int $id ID do registro que está sendo salvo
     * @return void     Retorna TRUE em caso de sucesso ou um array de erros em formato json em caso de falha
     */
    public function save_ajax($id) {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        if ($this->save('update', $id, true)) {
            // Log the activity
            log_activity($this->current_user->id, lang('modules_act_edit_record') .': '. $id .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($_POST), $this->module_name);

            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(true));

        } else {

            $this->output
                ->set_content_type('application/json')
                ->set_status_header('400')
                ->set_output(json_encode($this->wt_validation->error_array()));
        
        }

    }

    /**
     * Salva os dados enviados através de Ajax 
     * @param  int $id ID do registro que está sendo salvo
     * @return void     Retorna TRUE em caso de sucesso ou um array de erros em formato json em caso de falha
     */
    public function save_sortable($table) {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        foreach ($this->input->post('order') as $order) {
            
            $update_data = array();
            $update_where = array();
            foreach ($order as $key => $value) {
                if ($key != 'file_order') {
                    $update_where[$key] = $value;
                } else {
                    $update_data[$key] = $value;
                }
            }
            
            if ($update_where) {
                $this->db->update($table, $update_data, $update_where); 
            }
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(true));

    }

    /**
     * Carrega o Form para inserção de um novo registro
     * @return void
     */
    public function create()
    {
        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Create');


        if (isset($_POST['save']))
        {

            if ($insert_id = $this->save())
            {
                // Log the activity
                log_activity($this->current_user->id, lang('modules_act_create_record') .': '. $insert_id .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($_POST), $this->module_name);

                Template::set_message(lang('module_create_success'), 'success');
                redirect(SITE_AREA .'/' . $this->context_name . '/' . $this->module_name . (($this->query_string)?('?'.$this->query_string):''));
            }
            else
            {
                if (!isset($_POST['just_validate'])) {
                    Template::set_message(lang('module_create_failure') . $this->{$this->module_name.'_model'}->error, 'error');
                }
            }
        }

        

        if (!isset($_POST['just_validate'])) {
            Template::set('toolbar_title', lang('module_create') . ' ' . $this->module_config['name']);

            //Carrega os Fields
            $fields = $this->wtche->form->getFields('create');

            //Caso tenha um filter 
            $filter = unserialize(base64_decode($this->input->get('filter')));

            if ($filter) {
                foreach ($fields as $key => &$field) {
                    if (array_key_exists($field->fieldName(), $filter)) {
                        //Remove o field do form
                        $this->wtche->form->removeField($field->fieldName());
                        //adiciona um hidden no lugar
                        $this->wtche->form->addField('hidden')
                            ->setFieldName($field->fieldName())
                            ->setValue($filter[$field->fieldName()]);
                    }
                }
                //RE-Carrega os Fields
                $fields = $this->wtche->form->getFields('create');
            }

            Template::set('fields', $fields);


            Template::set_view('forms/form');

            if ($this->input->get('template')) {
                Template::render($this->input->get('template'));
            } else {
                Template::render();
            }
        }
        
    }

    /**
     * Carrega o Form para edição de um registro
     * @return void
     */
    public function edit($id)
    {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        if (empty($id))
        {
            Template::set_message(lang('module_invalid_id'), 'error');
            redirect(SITE_AREA .'/' . $this->context_name . '/' . $this->module_name);
        }

        if (isset($_POST['save']))
        {
            $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

            if ($this->save('update', $id))
            {
                // Log the activity
                log_activity($this->current_user->id, lang('modules_act_edit_record') .': '. $id .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($_POST), $this->module_name);

                Template::set_message(lang('module_edit_success'), 'success');
            }
            else
            {
                if (!isset($_POST['just_validate'])) {
                    Template::set_message(lang('module_edit_failure') . $this->{$this->module_name.'_model'}->error, 'error');
                }
            }
        }
        else if (isset($_POST['delete']))
        {
            $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Delete');

            // Log the activity
            log_activity($this->current_user->id, lang('modules_act_delete_record') .': '. $id .' | IP: '. $this->input->ip_address() . ' | Data: ' . json_encode($this->{$this->module_name.'_model'}->find($id)), $this->module_name);

            if ($this->{$this->module_name.'_model'}->delete($id))
            {

                Template::set_message(lang('module_delete_success'), 'success');

                redirect(SITE_AREA .'/' . $this->context_name . '/' . $this->module_name);
            }
            else
            {
                Template::set_message(lang('module_delete_failure') . $this->{$this->module_name.'_model'}->error, 'error');
            }
        }

        if (!isset($_POST['just_validate'])) {
            ${$this->module_name} = $this->{$this->module_name.'_model'}->find($id);

            if (!${$this->module_name}) {
                Template::set_message(lang('module_invalid_id'), 'error');
                redirect(SITE_AREA .'/' . $this->context_name . '/' . $this->module_name);
            }

            Template::set($this->module_name, ${$this->module_name});
            Template::set('toolbar_title', lang('module_edit') .' '.$this->module_config['name']);

            //Carrega os Fields
            $fields = $this->wtche->form->getFields('edit');

            //Caso tenha um filter 
            $filter = unserialize(base64_decode($this->input->get('filter')));

            if ($filter) {
                foreach ($fields as $key => &$field) {
                    if (array_key_exists($field->fieldName(), $filter)) {
                        //Remove o field do form
                        $this->wtche->form->removeField($field->fieldName());
                        //adiciona um hidden no lugar
                        $this->wtche->form->addField('hidden')
                            ->setFieldName($field->fieldName())
                            ->setValue($filter[$field->fieldName()]);
                    }
                }
                //RE-Carrega os Fields
                $fields = $this->wtche->form->getFields('edit');
            }

            Template::set('fields', $fields);

            //Percorre todos os campos para trazer o value de acordo com cada Field
            //Ex: Caso seja um select o value será um array (chave=>valor)
            foreach ($fields as &$field) {

                //Caso o atributo não exista (Não é um campo do banco) iremos inclui-lo ao ${$this->module_name}
                if (!isset(${$this->module_name}->{$field->fieldName()})) {
                    ${$this->module_name} = (object) array_merge( (array)${$this->module_name}, array( $field->fieldName() => 0 ) );
                }

                //Joga o value do banco pra dentro do Field
                $field->setValue(${$this->module_name}->{$field->fieldName()}, $id);

            }


            Template::set_view('forms/form');

            if ($this->input->get('template')) {
                Template::render($this->input->get('template'));
            } else {
                Template::render();
            }
        }
    }

    /**
     * Faz upload dos arquivos para o módulo
     * @return retorna um JSON contendo os dados do upload
     *
     *  O post deve conter um atributo 'field_name' contendo o nome do campo que contém o arquivo
     * 
     */ 
    public function do_upload() {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        $field = $this->wtche->form->getField($this->input->post('field_name'));

        $path = 'uploads/' . $field->get_table() . '/_temp/';

        //Cria o folder caso não exista
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $config['upload_path'] = $path;

        //Cria um filename unico
        $config['encrypt_name'] = TRUE;
        
        $config['max_size'] = $field->maxSize();
        $config['allowed_types'] = $field->allowedTypes();

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($this->input->post('field_name'))) {

            $error = array('error' => strip_tags($this->upload->display_errors()));

            $this->output
                ->set_content_type('application/json')
                ->set_status_header('400')
                ->set_output(json_encode($error));

        } else {

            $data = $this->upload->data();

            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));            

        }

    }

    /**
     * Salva algum arquivo de upload no banco
     * @param  Recebe via POST os parametros field_name, key (FK), file_name e orig_name
     * @return void     Retorna o objeto inserido em caso de sucesso ou uma mensagem de erro em formato json em caso de falha
     */
    public function save_upload() {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        $field = $this->wtche->form->getField($this->input->post('field_name'));

        $insert[$this->input->post('file_name')] = array(
           $field->fk()         => $this->input->post('key'),
           'file_name'          => $this->input->post('file_name'),
           'orig_name' => $this->input->post('orig_name'),
           'is_valid'           => 1,
        );

        if ($field->save($this->input->post('key'), $insert, true)) {

            $insert[$field->model->get_key()] = $this->db->insert_id();

            //Retorna o objeto com as informacoes
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($insert));
        } else {

            $this->output
                ->set_content_type('application/json')
                ->set_status_header('400')
                ->set_output(json_encode('Erro ao inserir registro do arquivo no banco de dados!'));

        }

    }

    /**
     * Deleta algum arquivo de upload
     * @param  Recebe via POST os parametros field_name e file_name a ser deletado
     * @return void     Retorna TRUE em caso de sucesso ou uma mensagem de erro em formato json em caso de falha
     */
    public function delete_upload() {

        $this->auth->restrict(ucfirst($this->module_name) . '.' . $this->context_name . '.Edit');

        $field = $this->wtche->form->getField($this->input->post('field_name'));

        $path = 'uploads/' . $field->get_table();

        $file_name = $this->input->post('file_name');

        if (@unlink($path . '/' . $file_name) || @unlink($path . '/_temp/' . $file_name)) {

            if ($this->db->delete($field->get_table(), array('file_name' => $file_name))) {
                
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(true));

            } else {

                $this->output
                    ->set_content_type('application/json')
                    ->set_status_header('400')
                    ->set_output(json_encode('Erro ao remover o registro do arquivo no banco de dados!'));

            }

        } else {

            $this->output
                    ->set_content_type('application/json')
                    ->set_status_header('400')
                    ->set_output(json_encode('Erro ao deletar o arquivo!'));
        }

    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Salva o registro no Banco
     * @param  string  $type "insert" ou "update"
     * @param  integer $id   ID do registro que está sendo atualizado (ignorar em caso de insert)
     * @return Mixed        Um INT em caso de sucesso no insert, TRUE em caso de sucesso no update ou FALSE
     */
    private function save($type='insert', $id=0, $is_ajax = false)
    {

        if ($type == 'update')
        {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want
        $data = array();

        //Fields
        $fields = $this->wtche->form->getFields(($type == 'insert')?'create':(($type == 'update')?'edit':''));

        foreach ($fields as $field) {

            //Remove o valor do Hidden dos select multiplos
            if (method_exists($field, 'items') && $field->relationMultiple()) {
                foreach ($this->input->post($field->fieldName()) as $key => $item) {
                    if (!$item) {
                        unset($_POST[$field->fieldName()][$key]);
                    }
                }
            }

            if ((array_key_exists($field->fieldName(), $this->input->post()) || (get_class($field) == 'File_Upload' && !$is_ajax)) && !$field->isStatic()) {

                if ($this->db->field_exists($field->fieldName(), $this->{$this->module_name.'_model'}->get_table())) {
                    $data[$field->fieldName()] = $this->input->post($field->fieldName(), !(get_class($field) == 'Text' && $field->isRich())); //Não deve habilitar o XSS para o Rich
                }

                //Set Validation
                $this->wt_validation->set_rules($field->fieldName(), $field->label(), $field->validationRules());
            }

            //Se for um array em branco de um select multiplo vamos remove-lo para validar certo
            if (method_exists($field, 'items') && $field->relationMultiple() && empty($_POST[$field->fieldName()])) {
                unset($_POST[$field->fieldName()]);
            }

            //Se for um Switcher e não tiver no post então deve considerar o valor OFF dele
            if ((!array_key_exists($field->fieldName(), $this->input->post()) && (get_class($field) == 'Switcher' && !$is_ajax)) && !$field->isStatic()) {
                $data[$field->fieldName()] = $field->valueOff();
            }
        }

        if ($this->wt_validation->run() === FALSE) {

            if (isset($_POST['just_validate'])) {
                $this->output
                    ->set_content_type('application/json')
                    ->set_status_header('400')
                    ->set_output(json_encode($this->wt_validation->error_array()));
            }
            return FALSE;

        } elseif (isset($_POST['just_validate'])) {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(true));
            return FALSE;
        } else {

            if ($type == 'insert') {
                $id = $this->{$this->module_name.'_model'}->insert($data);

                if (is_numeric($id))
                {
                    $return = $id;
                }
                else
                {
                    $return = FALSE;
                }
            } elseif ($type == 'update') {
                $return = $this->{$this->module_name.'_model'}->update($id, $data);
            }


            //Chama o Save em cada field para que cada um salve onde precisar
            foreach ($fields as $field) {
                if (array_key_exists($field->fieldName(), $this->input->post()) && !$field->isStatic() && method_exists($field, 'save')) {
                    if (!$field->save($id, $this->input->post($field->fieldName()))) {
                        return FALSE;
                    }
                }
            }

        }
        
        return $return;
    }


}

/* End of file Admin_Controller_WT.php */
/* Location: ./application/core/Admin_Controller_WT.php */