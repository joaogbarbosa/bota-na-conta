<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Front Controller
 *
 * This class provides a common place to handle any tasks that need to
 * be done for all public-facing controllers.
 *
 * @package    WTchê\Core\Controllers
 * @category   Controllers
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */
class Site_Controller extends Front_Controller
{

    //--------------------------------------------------------------------

    /**
     * Class constructor
     *
     */
    public function __construct($context_name, $module_name)
    {
        
        parent::__construct($context_name, $module_name);
        
        Events::trigger('before_site_controller');

        $this->lang->load('site');

        Assets::add_js(array(
            '../plugins/jquery-1.11.2.js',
            )
        );
        Assets::add_css(array(
            'reset.css',
            'default.css',
            '../plugins/animate.css',
            )
        );

        Events::trigger('after_site_controller');
    }//end __construct()

}

/* End of file Front_Controller.php */
/* Location: ./application/core/Front_Controller.php */