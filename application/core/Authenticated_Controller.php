<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Authenticated Controller
 *
 * Provides a base class for all controllers that must check user login
 * status.
 *
 * @package    WTchê\Core\Controllers
 * @category   Controllers
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */
class Authenticated_Controller extends Base_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Class constructor setup login restriction and load various libraries
	 *
	 */
	public function __construct()
	{
		// Load the Auth library before the parent constructor to ensure
		// the current user's settings are honored by the parent
		$this->load->library('users/auth');

		parent::__construct();

		// Make sure we're logged in.
		$this->auth->restrict();

		// Load additional libraries
		$this->load->helper('form');
		$this->load->library('WT_Validation');
		$this->wt_validation->set_error_delimiters('', '');
		$this->wt_validation->CI =& $this;	// Hack to make it work properly with HMVC
	}//end construct()

	//--------------------------------------------------------------------

}

/* End of file Authenticated_Controller.php */
/* Location: ./application/core/Authenticated_Controller.php */