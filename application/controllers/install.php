<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Install extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        //Troca o Assets Base Folder
        $this->config->set_item('assets.base_folder', $this->config->item('assets.base_folder') . '/_admin');

        // Load basics since we are not relying on
        // Base_Controller here...
        $this->load->library('template');
        $this->load->library('assets');
        $this->load->library('events');
        $this->load->helper('application');
        $this->lang->load('application');

        Template::set('environment', ENVIRONMENT);
        Template::set_theme('install');

    }

    //--------------------------------------------------------------------

    public function index()
    {

        //Cria Estrutura de Pastas que não podem estar no SVN
        @mkdir('application/logs'); //Logs do Sistema
        @mkdir('uploads'); //Upload de Arquivos

        Template::set_view('install/index');
        Template::render();
    }

    public function environment($env) {

        $this->load->helper('file');

        $index_current  = fopen("index.php",'r');
        $index_temp     = fopen("index.tmp.php",'w');

        if (!$index_current || !$index_temp) {
            die("Sem permissão para acessar/gravar dados no index.php!");
        }

        while(!feof($index_current)) {
            $line = fgets($index_current);
            // define('ENVIRONMENT', '###environment###');

            if (strpos($line, "define('ENVIRONMENT'")!==false) {
                $line="\tdefine('ENVIRONMENT', '{$env}');" . PHP_EOL;
            }
            fwrite($index_temp, $line);

        }       

        fclose($index_current);
        fclose($index_temp);  

        rename("index.tmp.php","index.php");

        redirect('install/config_db', 'refresh');

    }

    public function config_db() {

        $this->load->library('installer_lib');

        if (!$this->installer_lib->is_installed()) {

            $this->load->helper('form');

        } else {
            Template::set('installed', 'O WTChê Framework já esta instalado no ambiente "' . ENVIRONMENT . '".');
        }

        Template::set_view('install/config_db');
        Template::render();

    }

    public function do_config_db() {

        $this->load->library('installer_lib');
        $this->load->helper('form');

        if (!$this->installer_lib->test_db_connection()) {
            Template::set('error', 'Erro nas configurações de banco de dados!');
            Template::set_view('install/config_db');
            Template::render();
        } else {
            copy(APPPATH.'config/database.php', APPPATH.'config/'.ENVIRONMENT.'/database.php');
            write_db_config(array(ENVIRONMENT=>$this->input->post()));
            redirect('install/config_app', 'refresh');
        }


    }

    public function config_app() {

        $this->load->library('installer_lib');
        $this->load->helper('form');

        Template::set_view('install/config_app');
        Template::render();

    }

    public function do_config_app() {

        $this->load->library('installer_lib');
        $this->load->helper('form');

        copy(APPPATH.'config/config.php', APPPATH.'config/'.ENVIRONMENT.'/config.php');
        copy(APPPATH.'config/application.php', APPPATH.'config/'.ENVIRONMENT.'/application.php');

        foreach ($this->input->post() as $file => $config) {
            write_config(ENVIRONMENT.'/'.$file, $config);
        }
    
        redirect('install/info', 'refresh');

    }

    public function info()
    {
        $this->load->library('installer_lib');

        $data = array();

        // PHP Version Check
        $data['php_min_version']    = '5.3';
        $data['php_acceptable']     = $this->installer_lib->php_acceptable($data['php_min_version']);
        $data['php_version']        = $this->installer_lib->php_version;

        // Curl Enabled?
        $data['curl_enabled']       = $this->installer_lib->cURL_enabled();

        // Files/Folders writeable?
        $data['folders']            = $this->installer_lib->check_folders();
        $data['files']              = $this->installer_lib->check_files();

        Template::set($data);
    
        Template::set_view('install/info');
        Template::render();
    }

    //--------------------------------------------------------------------

    public function import()
    {
        $this->load->library('installer_lib');
        $this->load->helper('form');

        if (!$this->installer_lib->is_installed() && $this->input->post('importar'))
        {

            $config['upload_path'] = BASEPATH . '../../upload/';
            $config['allowed_types'] = '*';
            $config['file_name'] = 'import.sql';
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file'))
            {
                Template::set('import_error', $this->upload->display_errors());
            }
            else
            {
                redirect('install/do_install/import', 'refresh');
            }

        }
        else
        {
            Template::set('installed', 'O WTChê Framework já esta instalado no ambiente "' . ENVIRONMENT . '".');
        }
    
        Template::set_view('install/import');
        Template::render();
    }

    public function settings()
    {
        $this->load->library('installer_lib');
        $this->load->helper('form');

        if ($this->installer_lib->is_installed())
        {

            Template::set('installed', 'O WTChê Framework já esta instalado no ambiente "' . ENVIRONMENT . '".');

        }

    
        Template::set_view('install/settings');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Handles the basic installation of the migrations into the database
     * if available, and displays the current status.
     *
     * @return void
     */
    public function do_install($import = false)
    {
        $this->load->library('installer_lib');

        if ($this->installer_lib->is_installed())
        {
            die('O WTChê Framework já esta instalado no ambiente "' . ENVIRONMENT . '".');
        }

        if (!$import) {
            // Does the database table even exist?
            if ($this->installer_lib->db_settings_exist === FALSE)
            {
                show_error("Erro nas configurações de banco de dados!");
            }

            // Run our migrations
            $this->load->library('migrations/migrations');
        }
        

        if ($this->installer_lib->setup($import, $this->input->post())) {
            define('BF_DID_INSTALL', true);
        } else {
            define('BF_DID_INSTALL', false);
        }
        
        Template::set_view('install/do_install');
        Template::render();
    }

}