<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class api extends REST_Controller
{
    private $status = ['new' => "já vamos atende-los",'pending' => "na produção","delivered" => "tá na mesa"];
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function index_get()
    {
        $this->load->view('welcome_message');
    }

    function list_products_post(){

        $code = $this->input->post("identificador");
        $token = $this->input->post("imei");

        $this->list_products($code,$token);
     }
    function list_products_get($code,$token){
        $this->list_products($code,$token);
     }
    function list_products($code,$token){

        if(!$code || !$token)
        {
            $this->response(array('error' => "FALTA PARAMETRO"), 400);
            exit;
        }
        //Fechar todos pedidos em aberto


        $retorno = array();
        $this->db->select("establishments.*,identifiers.name as mesa");
        $this->db->where("identifiers.code",$code);
        $this->db->join("identifiers","identifiers.id_establishment = establishments.id");
        $establishments = $this->db->get("establishments")->result();
        $retorno['establishments'] = $establishments[0];
        //TODO VALIDAR SE TÁ ATIVO

        $this->db->select("products_categories.name as categoria,products.name as name,products.description as description,products.id,products.value");
        $this->db->join("products_categories","products_categories.id = products.id_product_category");
        $this->db->where("id_establishment",$retorno['establishments']->id);
        $this->db->where("products_categories.active",1);
        $this->db->where("products.active",1);
        $this->db->order_by("id_product_category");
        $retorno['list_products'] =  $this->db->get("products")->result();

        //Salva ordem no sistema
        $this->load->module("orders");
        $retorno['order'] = $this->orders->open_order($code,$token);
        $this->response($retorno, 200);
    }
    function list_order_itens($token){

        //$token = $this->input->post("imei");

        if(!$token)
        {
            $this->response(array('error' => "FALTA PARAMETRO"), 400);
            exit;
        }
        $this->load->module("orders");
        $ultimo = new stdClass();
        $ultimo = $this->orders->get_last($token);

        $this->db->select("order_items.status as status,products_categories.name as categoria, sum(1) as quantidade,products.description as description, sum(`order_items`.value) as value, products.id, products.`name` as name");

        $this->db->join("orders","orders.id = order_items.id_order");
        $this->db->join("products","products.id = order_items.id_product");
        $this->db->join("products_categories","products_categories.id = products.id_product_category");
        //$this->db->where("device_code",$token);
        //$this->db->where("orders.status","open");
        $this->db->where("order_items.id_order",$ultimo->id);
        $this->db->group_by("products.id,order_items.status");
        $this->db->order_by(" `order_items`.`created_on` desc, `orders`.`created_on` desc, products.id  desc");

        $retorno = new stdClass();
        $retorno->list_products = $this->db->get("order_items")->result();

        $this->response($retorno, 200);

    }
    function list_order_itens_post(){

        $token = $this->input->post("imei");

        $this->list_order_itens($token);

    }
    function list_order_itens_get($token){

        $this->list_order_itens($token);

    }
    /**
    TODO
    **/
    function add_order_post(){

        $token = $this->input->post("imei");
        $lista_pedido = json_decode($this->input->post("pedido"));

        $this->load->module("order_items");

        foreach ($lista_pedido as $pedido) {
            if(count($pedido->quantidade) > 0 ){
                for ($i=0; $i < $pedido->quantidade; $i++) {
                    $this->order_items->add($pedido->id,$token);
                }

            }

        }
        $this->response("Success", 200);

    }
    function pay_order_post(){
        $token = $this->input->post("imei");
        if(!$token)
        {
            $this->response(array('error' => "FALTA PARAMETRO"), 400);
            exit;
        }
        $this->load->module("orders");
        if($this->orders->close_order($token)){
            $this->response("dinossauros estão vivos, e vindo em nossa direção", 200);
        }
        else{
            $this->response(array('error' => "Erro fechar ordem"), 400);
            exit;
        }

    }
    function call_post(){

        $token = $this->input->post("imei");

        if(!$token)
        {
            $this->response(array('error' => "FALTA PARAMETRO"), 400);
            exit;
        }

        $this->load->module("calls");
        $this->response($this->calls->call($token), 200);

    }
    /*
    function listType_get()
    {


        $retorno = $this->db->get("TipoObjeto")->result();
        $this->response($retorno, 200);
    }

    function setDisabledProduct_get()
    {
        if(!$this->get('id'))
        {
            $this->response(array('error' => FALTA_PARAMETRO), 400);
            exit;
        }
        $this->db->set("StatusObjetoID",4);
        $this->db->where("ObjetoID",$this->get('id'));
        $this->db->update("Objeto");

        $this->response(array('sucess' => true), 200);

    }
    function setEnabledProduct_get()
    {
        if(!$this->get('id'))
        {
            $this->response(array('error' => FALTA_PARAMETRO), 400);
            exit;
        }
        $this->db->set("Ativo",1);
        $this->db->where("ObjetoID",$this->get('id'));
        $this->db->update("Objeto");

        $this->response(array('sucess' => true), 200);

    }

    function getVideoProductsList_get(){

        $retorno = array();
        $colecao_tipo_objeto = $this->db->get("TipoObjeto")->result();
        foreach ($colecao_tipo_objeto as $tipo_objeto) {

            $this->db->select("Objeto.*");
            $this->db->select("Pessoa.*");
            $this->db->select("StatusObjeto.Descricao as StatusObjeto");
            $this->db->select("TipoObjeto.Descricao as TipoObjeto");
            $this->db->where("Objeto.TipoObjetoID",$tipo_objeto->TipoObjetoID);
            $this->db->where("Objeto.StatusObjetoID",2);
            $this->db->where("Objeto.Ativo !=",'0');
            $this->db->order_by(" (Ordem  IS NULL), Ordem ,DataAprovacao asc ");
            $this->db->join('Pessoa', 'Pessoa.PessoaID = Objeto.PessoaID AND Pessoa.Ativo = 1');
            $this->db->join('StatusObjeto', 'Objeto.StatusObjetoID = StatusObjeto.StatusObjetoID');
            $this->db->join('TipoObjeto', 'Objeto.TipoObjetoID = TipoObjeto.TipoObjetoID');
            $colecao_objeto = $this->db->get("Objeto",$tipo_objeto->Quantidade)->result();

            // echo $this->db->last_query();
            // exit;
            if (count($colecao_objeto) > 0) {
                foreach ($colecao_objeto as $objeto) {
                    array_push($retorno,$objeto);
                }
            }

        }
        $this->response($retorno, 200);
    }

    function setUserTermsBC_get(){
        if(!$this->get('id'))
        {
            $this->response(array('error' => FALTA_PARAMETRO), 400);
            exit;
        }
        //var_dump($this->get('news'));
        if($this->get('news'))
        {
            $dados['TermoNews'] = true;
        }
        else{
             $dados['TermoNews'] = false;
        }

        $dados["TermoUsoTroca"] = true;
        $this->db->where("PessoaID",$this->get('id'));
        $this->db->update('Pessoa', $dados);
        $this->response(array('sucess' => true), 200);
    }

    function setUserTermsNews_get(){
        if(!$this->get('id'))
        {
            $this->response(array('error' => FALTA_PARAMETRO), 400);
            exit;
        }

        $dados['TermoNews'] = true;
        $this->db->where("PessoaID",$this->get('id'));
        $this->db->update('Pessoa', $dados);
        $this->response(array('sucess' => true), 200);
    }

    function setUserTermsMobile_get(){
        if(!$this->get('id'))
        {
            $this->response(array('error' => FALTA_PARAMETRO), 400);
        }

        if($this->get('news'))
        {
            $dados['TermoNews'] = true;
        }
        else{
             $dados['TermoNews'] = false;
        }
        $dados["TermoApp"] = true;
        $this->db->where("PessoaID",$this->get('id'));
        $this->db->update('Pessoa', $dados);

        if($this->db->affected_rows() > 0){
            $this->response(array('sucess' => true), 200);
        }
        else{
            $this->response(array('sucess' => true, "mensagem" => NAO_ATUALIZADO), 400);
        }


    }
    function getMyProductsList_get(){
        if(!$this->get('id'))
        {
            $this->response(array('error' => FALTA_PARAMETRO), 400);
            exit;
        }

        $this->db->order_by("StatusObjetoID")->where("PessoaID",$this->get('id'));

        $retorno = $this->db->order_by("StatusObjetoID,date(DataCadastro),TipoObjeto")->get("vwListaProduto")->result();
        $this->response($retorno, 200);
    }
    function getFBThumbFromProduct_get(){
        if(!$this->get('id'))
        {
            $this->response(array('error' => FALTA_PARAMETRO), 400);
            exit;
        }

        $this->db->where("PessoaID",$this->get('id'));
        $Pessoa = $this->db->get("Pessoa")->result();

        $imagem = file_get_contents($Pessoa[0]->Foto);
        header('Content-type: image/jpg');
        echo $imagem;
    }

    function sendOffer_post(){
        $this->form_validation->set_rules('PessoaID', 'PessoaID', 'required|numeric');
        //$this->form_validation->set_rules('Oferta', 'Oferta', 'required');
        $this->form_validation->set_rules('ObjetoID', 'ObjetoID', 'required|numeric');

        if($this->form_validation->run()){

            $this->db->where("PessoaID",$this->input->post('PessoaID'));
            $pessoa = $this->db->get("Pessoa")->result();

            $this->db->where("ObjetoID",$this->input->post('ObjetoID'));
            $objeto = $this->db->get("Objeto")->result();

            if(count($pessoa) == 0 && count($objeto) == 0){
                $this->response(array('error' => Pessoa_OBJETO_NAO_ENCONTRADO), 400);
            }
            elseif(count($pessoa) == 0){
                $this->response(array('error' => Pessoa_NAO_ENCONTRADO), 400);
            }
            elseif(count($objeto) == 0){
                $this->response(array('error' => OBJETO_NAO_ENCONTRADO), 400);
            }
            else{
                $dados = array(
                    'PessoaID' =>  $this->input->post('PessoaID'),
                    'ObjetoID' =>  $this->input->post('ObjetoID'),
                    'Descricao' =>  $this->input->post('Oferta'),
                    'DataCadastro' => date("Y-m-d H:i:s")
                );

                $this->db->insert('Oferta', $dados);


                $this->db->select("Pessoa.*");
                $this->db->where("ObjetoID",$this->input->post('ObjetoID'));

                $this->db->join('Objeto', 'Objeto.PessoaID = Pessoa.PessoaID');

                $pessoa = $this->db->get("Pessoa")->result();

                $this->db->where("PessoaID",$this->input->post('PessoaID'));
                $pessoaInteresse = $this->db->get("Pessoa")->result();

                $this->db->select("TipoObjeto.*");
                $this->db->where("ObjetoID",$this->input->post('ObjetoID'));
                $this->db->join('Objeto', 'Objeto.TipoObjetoID = TipoObjeto.TipoObjetoID');
                $tipo_objeto = $this->db->get("TipoObjeto")->result();

                $this->db->where("ObjetoID",$this->input->post('ObjetoID'));
                $objeto = $this->db->get("Objeto")->result();


                $this->load->library('email');

                $config['protocol']   = 'mail';
                $config['charset']    = 'utf8';
                $config['mailtype']   = 'html';

                $this->email->initialize($config);

                $this->email->from('webmaster@'.str_replace('www.','',$_SERVER['HTTP_HOST']), REMETENTEEMAILAPROVADO);
                //$this->email->reply_to($from, $from_name);
                $this->email->to($pessoa[0]->Email);
                //$this->email->bcc('joao@codzen.com.br');

                $dados = array();

                $dados["Objeto"] = '<a href="http://souumanovahistoria.com.br/preview.html?image='.base_url('upload/objeto/'.$objeto[0]->Imagem).'&index='.$objeto[0]->TipoObjetoID.'">'.$tipo_objeto[0]->Descricao.'</a>';

                $dados['Nome'] = $pessoa[0]->Nome;
                $dados['NomeInteresse'] = $pessoaInteresse[0]->Nome;
                $dados['EmailInteresse'] = $pessoaInteresse[0]->Email;
                $dados['Local'] = $pessoaInteresse[0]->Cidade . " - " .$pessoaInteresse[0]->Estado;
                $dados['Oferta'] = $this->input->post('Oferta');

                $html = $this->load->view("email/proposta-recebida.php",$dados,TRUE);

                $this->email->subject(ASSUNTOEMAILPROPOSTARECEBIDA);
                $this->email->message($html);

                if(!$this->email->send()){}

                $this->response(array('sucess' => true), 200);
            }
        }
        else{
            $this->response(array('error' => FALTA_PARAMETRO_INVALIDO), 400);
        }

    }

    function logFBUser_get(){
        $this->response(array('error' => FALTA_PARAMETRO), 400);
    }
    function logFBUser_post(){

        $facebookID = $this->input->post('FacebookID');
        if(!empty($facebookID))
        {
            $this->db->where("FacebookID",$facebookID);
            $pessoa = $this->db->get("Pessoa")->result();

            if(count($pessoa) > 0){
                $this->response($pessoa[0], 200);
            }
        }
        $this->form_validation->set_rules('FacebookID', 'FacebookID', 'required');
        $this->form_validation->set_rules('Nome', 'Nome', 'required');
        $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('Foto', 'Foto', 'required');

        if($this->form_validation->run()){

            $dados = array(
                'FacebookID' =>  $this->input->post('FacebookID'),
                'Nome' =>  $this->input->post('Nome'),
                'Email' =>  $this->input->post('Email'),
                'Foto' =>  $this->input->post('Foto'),
                'Estado' =>  $this->input->post('Estado'),
                'Cidade' =>  $this->input->post('Cidade'),
                'TermoApp' =>  0,
                'TermoNews' =>  0,
                'TermoUsoTroca' =>  0,
                'Ativo' =>  1,
                'DataCadastro' => date("Y-m-d H:i:s")
            );

            $this->db->insert('Pessoa', $dados);
            $dados['PessoaID'] = $this->db->insert_id();

            $this->response($dados, 200);
        }
        else{
            $this->response(array('error' => FALTA_PARAMETRO_INVALIDO), 400);
        }

    }
    function sendOffer_get(){
        $this->response(array('error' => FALTA_PARAMETRO), 400);
    }
    function goList_get(){

        $colecao_tipo_objeto = $this->db->get("TipoObjeto")->result();

        foreach ($colecao_tipo_objeto as $tipo_objeto) {
            $this->db->select("Objeto.*");
            $this->db->where("TipoObjetoID",$tipo_objeto->TipoObjetoID);
            $this->db->where("StatusObjetoID",2);
            $this->db->order_by(" (Ordem  IS NULL), Ordem ,DataAprovacao asc ");
            $this->db->join('Pessoa', 'Pessoa.PessoaID = Objeto.PessoaID AND Pessoa.Ativo = 1');
            $colecao_objeto = $this->db->get("Objeto",1)->result();

			if(count($colecao_objeto) > 0){
				$this->db->set("StatusObjetoID",4);
				$this->db->where("ObjetoID",$colecao_objeto[0]->ObjetoID);
				$this->db->update("Objeto");
			}

        }
        $this->avisarExibicao();

        $this->response(array('sucess' => true), 200);

    }

    function avisarExibicao(){

        $colecao_tipo_objeto = $this->db->get("TipoObjeto")->result();
        foreach ($colecao_tipo_objeto as $tipo_objeto) {

            $this->db->select("Objeto.*");
            $this->db->select("Pessoa.*");
            $this->db->select("StatusObjeto.Descricao as StatusObjeto");
            $this->db->select("TipoObjeto.Descricao as TipoObjeto");
            $this->db->where("Objeto.TipoObjetoID",$tipo_objeto->TipoObjetoID);
            $this->db->where("Objeto.StatusObjetoID",2);
            $this->db->where("Objeto.Ativo !=",'0');
            $this->db->order_by(" (Ordem  IS NULL), Ordem ,DataAprovacao asc ");
            $this->db->join('Pessoa', 'Pessoa.PessoaID = Objeto.PessoaID AND Pessoa.Ativo = 1');
            $this->db->join('StatusObjeto', 'Objeto.StatusObjetoID = StatusObjeto.StatusObjetoID');
            $this->db->join('TipoObjeto', 'Objeto.TipoObjetoID = TipoObjeto.TipoObjetoID');
            $colecao_objeto = $this->db->get("Objeto",$tipo_objeto->Quantidade)->result();

            if (count($colecao_objeto) > 0) {
                foreach ($colecao_objeto as $objeto) {
                    //array_push($retorno,$objeto);
                    if(!$objeto->Avisado){
                        $this->db->set("Avisado",'1');
                        $this->db->where("ObjetoID",$objeto->ObjetoID);
                        $this->db->update("Objeto");

                        $pessoa = $this->db->where("PessoaID",$objeto->PessoaID)->get("Pessoa")->result();

                        $this->load->library('email');

                        $config['protocol']   = 'mail';
                        $config['charset']    = 'utf8';
                        $config['mailtype']   = 'html';

                        $this->email->initialize($config);

                        $this->email->from('webmaster@'.str_replace('www.','',$_SERVER['HTTP_HOST']), REMETENTEEMAILAPROVADO);
                        //$this->email->reply_to($from, $from_name);
                        $this->email->to($pessoa[0]->Email);
                        //$this->email->bcc('joao@codzen.com.br');

                        $dados = array();


                        $this->db->select("TipoObjeto.*");
                        $this->db->where("ObjetoID",$objeto->ObjetoID);
                        $this->db->join('TipoObjeto', 'Objeto.TipoObjetoID = TipoObjeto.TipoObjetoID');
                        $tipo_objeto = $this->db->get("TipoObjeto")->result();

                        $dados["Objeto"] = '<a href="http://souumanovahistoria.com.br/preview.html?image='.base_url('upload/objeto/'.$objeto->Imagem).'&index='.$objeto->TipoObjetoID.'">'.$tipo_objeto[0]->Descricao.'</a>';

                        $dados["Nome"] = $pessoa[0]->Nome;


                        $html = $this->load->view("email/sendo-exibido.php",$dados,TRUE);

                        $this->email->subject(ASSUNTOEMAILEMEXIBICAO);
                        $this->email->message($html);

                        if(!$this->email->send()){}
                    }
                }
            }

        }
    }
    function sendNewProduct_post(){
        $this->form_validation->set_rules('PessoaID', 'PessoaID', 'required|numeric');
        $this->form_validation->set_rules('Descricao', 'Descricao', 'required');
        $this->form_validation->set_rules('TrocoPor', 'TrocoPor', 'required');
        $this->form_validation->set_rules('Imagem', 'Imagem', 'required');
        $this->form_validation->set_rules('TipoObjetoID', 'TipoObjetoID', 'required');

        if($this->form_validation->run()){

            $this->db->where("PessoaID",$this->input->post('PessoaID'));
            $pessoa = $this->db->get("Pessoa")->result();

            $this->db->where("TipoObjetoID",$this->input->post('TipoObjetoID'));
            $tipo_objeto = $this->db->get("TipoObjeto")->result();


            if(count($pessoa) == 0 && count($tipo_objeto) == 0){
                $this->response(array('error' => TIPO_OBJETO_Pessoa_NAO_ENCONTRADO), 400);
            }
            elseif(count($pessoa) == 0){
                $this->response(array('error' => Pessoa_NAO_ENCONTRADO), 400);
            }
            elseif(count($tipo_objeto) == 0){
                $this->response(array('error' => TIPO_OBJETO_NAO_ENCONTRADO), 400);
            }
            else{

                $rotacionar = false;
                if($this->input->post('TipoObjetoID') == 18){
                    $rotacionar = 90;
                }
                elseif($this->input->post('TipoObjetoID') == 11){
                    $rotacionar = 90;
                }
                elseif($this->input->post('TipoObjetoID') == 8){
                    $rotacionar = -90;
                }
                $nomeImagem = $this->saveImage($this->input->post('Imagem'),$rotacionar);

                if($nomeImagem){

                    $dados = array(
                        'PessoaID' =>  $this->input->post('PessoaID'),
                        'Descricao' =>  $this->input->post('Descricao'),
                        'TrocoPor' =>  $this->input->post('TrocoPor'),
                        'TipoObjetoID' =>  $this->input->post('TipoObjetoID'),
                        'Imagem' =>  $nomeImagem,
                        'StatusObjetoID' =>  1,
                        //'Base' => $this->input->post('Imagem'),
                        'Ativo' =>  1,
                        'DataCadastro' => date("Y-m-d H:i:s")
                    );

                    $this->db->insert('Objeto', $dados);

                    $this->response(array('sucess' => true), 200);

                }else{
                    $this->response(array('error' => FALTA_PARAMETRO_INVALIDO), 400);
                }
            }
        }
        else{
            $this->response(array('error' => FALTA_PARAMETRO_INVALIDO), 400);
        }

    }
    function saveImage($base,$rotacionar){

        if(!empty($base)){
            $nomeImagem = md5(date('dmYhis')).'.jpg';

            $base = str_replace("data:image/png;base64,", '', $base);

            $img = @imagecreatefromstring(base64_decode($base));
            if($img != false)
            {
                if($rotacionar){
                    imagealphablending($img, false);
                    imagesavealpha($img, true);

                    $imagem = imagerotate($img, $rotacionar, imageColorAllocateAlpha($img, 255, 255, 255));
                    // imagealphablending($imagem, false);
                    // imagesavealpha($imagem, true);
                }
                else{
                    $imagem = $img;
                }

                if(imagejpeg($imagem, "upload/objeto/".$nomeImagem)){
                    return $nomeImagem;
                }
                else{
                    return false;
                }

            }
            else{
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    function sendNewProduct_get(){
        $this->response(array('error' => FALTA_PARAMETRO), 400);
    }
    */
}