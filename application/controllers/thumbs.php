<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtagencia.com/#license
 * @link      http://www.wtagencia.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * WTchê Images
 *
 * A helper library to assist in on-the-fly resizing of images.
 * To use, make the src of your img tag reflect:
 *
 * http://mysite.com/images/abc.jpg?assets=images&size=xx
 *
 * Parameters:
 * The following parameters are available for use in the params
 * of the URL.
 *
 * assets	- The folder (relative to webroot) that the images can be found in.
 * 			  Optional. Defaults to '/assets'.
 * size	- If you want a square image returned, you can simply pass the size param
 * 			  and this will be used for both the height and width of the image.
 * height	- The height (in px) of the image. Not required if 'size' is passed.
 * width	- The width (in px) of the image. Not required if 'size' is passed.
 * crop	- If 'yes', will crop the image to the size or height/width.
 *
 * @package    WTchê
 * @subpackage Controllers
 * @category   Helpers
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */
class Thumbs extends Base_Controller {

	/**
	 * Simply redirects all calls to the index() method.
	 *
	 * @param string $file The name of the image to return.
	 */
	public function _remap($file=null)
	{
		$this->index($file);
	}//end _remap()

	//--------------------------------------------------------------------

	/**
	 * Performs the image processing based on the parameters provided in the GET request
	 *
	 * @param string $file The name of the image to return.
	 */
	public function index($file=null)
	{

		if (empty($file) || !is_string($file))
		{
			die('No file to process.');
		}

		$this->output->enable_profiler(false);

		// Get our params
		$path	= $this->input->get('path') ? $this->input->get('path') : '';
		$size	= $this->input->get('size');
		$height	= $this->input->get('height');
		$width	= $this->input->get('width');
		$ratio	= $this->input->get('ratio');
		$force	= $this->input->get('force');

		$ext = pathinfo($file, PATHINFO_EXTENSION);

		if (empty($ext))
		{
			die('Filename does not include a file extension.');
		}

		// Is it a square?
		if (!empty($size))
		{
			$height = (int)$size;
			$width	= (int)$size;
			$ratio 	= 'no';
		}
		
		$thumb_file = FCPATH . 'uploads/' . $path .'/'. $file;


		//Aqui vamos verificar se nao é outro tipo de arquivo e então retornar apenas um icone que represente a extensão
		if (!is_file($thumb_file) || !getimagesize($thumb_file)) {
			$thumb_file = FCPATH .'assets/images/extensions/'.$ext.'.png';
			if (!is_file($thumb_file)) {
				$thumb_file = FCPATH .'assets/images/extensions/_blank.png';
			}
			$ext = 'png';
		}

		$new_file = FCPATH .'assets/cache/'. str_replace('.'. $ext, '', $file) ."_{$width}x{$height}.". $ext;

		if (!is_file($new_file) || $force != 'yes')
		{
			$config = array(
				'image_library'		=> 'gd2',
				'source_image'		=> $thumb_file,
				'new_image'			=> $new_file,
				'create_thumb'		=> false,
				'maintain_ratio'	=> $ratio == 'no' ? false : true,
				'master_dim'		=> !empty($width) ? 'width' : 'height', 
				'width'				=> !empty($width) ? $width : $height,
				'height'			=> !empty($height) ? $height : $width,
			);

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();
		}

		$this->output
			->set_content_type($ext)
			->set_output(file_get_contents($new_file));
	}//end index()

	//--------------------------------------------------------------------

}//end class
