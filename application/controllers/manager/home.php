<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtagencia.com/#license
 * @link      http://www.wtagencia.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Content context controller
 *
 * The controller which displays the homepage of the Content context in WTchê site.
 *
 * @package    WTchê
 * @subpackage Controllers
 * @category   Controllers
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */
class Home extends Admin_Controller
{


	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		Template::set('toolbar_title', 'Content');

		$this->auth->restrict('Site.Content.View');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Displays the initial page of the Content context
	 *
	 * @return void
	 */
	public function index()
	{
		Template::set_view('admin/content/index');
		Template::render();
	}//end index()

	//--------------------------------------------------------------------


}//end class