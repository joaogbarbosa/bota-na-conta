package com.globo.hackinpoa.cardapioonline.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

/**
 * Created by usuario on 11/04/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RetornoCardapio {
    Estabelecimento establishments;
    ArrayList<Produto> list_products;

    public Estabelecimento getEstablishments() {
        return establishments;
    }

    public void setEstablishments(Estabelecimento establishments) {
        this.establishments = establishments;
    }

    public ArrayList<Produto> getList_products() {
        return list_products;
    }

    public void setList_products(ArrayList<Produto> list_products) {
        this.list_products = list_products;
    }
}
