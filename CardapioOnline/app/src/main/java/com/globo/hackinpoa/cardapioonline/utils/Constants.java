package com.globo.hackinpoa.cardapioonline.utils;

/**
 * Created by usuario on 11/04/2015.
 */
public class Constants {
    public static final String identificadorEstabelecimento = "identificadorEstabelecimento";
    public static final String identificadorMesa = "identificadorMesa";
    public static final String nomeEstabelecimento = "nomeEstabelecimento";

    public static final int MENSAGEM_CONSULTA_ESTABELECIMENTO = 1;

    public static final int MENSAGEM_ENVIAR_PEDIDO = 2;


    public static final int MENSAGEM_CONSULTA_PEDIDO = 3;

    public static final int MENSAGEM_EFETUAR_PAGAMENTO = 4;
}
