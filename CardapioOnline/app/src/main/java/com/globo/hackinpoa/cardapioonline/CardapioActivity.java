package com.globo.hackinpoa.cardapioonline;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.globo.hackinpoa.cardapioonline.Models.Produto;
import com.globo.hackinpoa.cardapioonline.Models.RetornoCardapio;
import com.globo.hackinpoa.cardapioonline.adapters.AdapterProduto;
import com.globo.hackinpoa.cardapioonline.utils.Constants;
import com.globo.hackinpoa.cardapioonline.utils.PojoMapper;
import com.globo.hackinpoa.cardapioonline.utils.SendServerInfo;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;


public class CardapioActivity extends GeneralActivity {

    RetornoCardapio retorno = null;
    TextView textViewMeusPedidos;
    AdapterProduto adProdutos;
    TextView textViewValor;
    TextView textViewNomeEstabelecimento;
    ListView listView;
    boolean cancelado = true;
    double total = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardapio);
        setTitle("Cardápio");

        getSupportActionBar().setBackgroundDrawable(getDrawable(R.drawable.ac_red));
        listView = (ListView) findViewById(R.id.listViewCardapio);
        textViewValor = (TextView) findViewById(R.id.textViewValor);
        textViewMeusPedidos = (TextView) findViewById(R.id.textViewMeusPedidos);
        textViewMeusPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CardapioActivity.this, PedidosActivity.class);
                if (retorno != null && retorno.getEstablishments() != null && retorno.getEstablishments().getName() != null) {
                    intent.putExtra(Constants.nomeEstabelecimento, retorno.getEstablishments().getName());
                    intent.putExtra(Constants.identificadorEstabelecimento, retorno.getEstablishments().getId()+"");
                    startActivity(intent);
                }
            }
        });
        textViewNomeEstabelecimento = (TextView) findViewById(R.id.textViewNomeEstabelecimento);
        textViewNomeEstabelecimento.setVisibility(View.INVISIBLE);
        buttonChamarGarcon = (Button) findViewById(R.id.buttonChamarGarcon);
        buttonChamarGarcon.setOnClickListener(onclickChamar);
        ((Button) findViewById(R.id.sino)).setOnClickListener(onclickChamar);
        Button buttonOKPedido = (Button) findViewById(R.id.buttonOKPedido);
        buttonOKPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (total > 0) {
                    openDialogEnviarPedido();
                }
            }
        });



        if (retorno == null) {
            adProdutos = new AdapterProduto(this, R.layout.item_produto);
            listView.setAdapter(adProdutos);
            //nao buscou as informacoeses precisa ir buscar
            String identificador = getIntent().getExtras().getString(Constants.identificadorEstabelecimento);
            ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("identificador", identificador));
            new SendServerInfo(this, "list_products/" + identificador, params).send(this, Constants.MENSAGEM_CONSULTA_ESTABELECIMENTO);
        } else {
            adProdutos = new AdapterProduto(this, R.layout.item_produto, retorno.getList_products());
            listView.setAdapter(adProdutos);
        }

    }

    public void openDialogEnviarPedido() {
        cancelado = true;
        LayoutInflater inflater = getLayoutInflater();
        final View layout = inflater.inflate(R.layout.diloag_enviar, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        final AlertDialog dialog = builder.create();
        Button bt = (Button) layout.findViewById(R.id.buttonCancelar);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
        final int tempoCancelar = 5000;
        final int tickTime = 1000;
        final DonutProgress donutProgress = (DonutProgress) layout.findViewById(R.id.donut_progress);
        donutProgress.setInnerBackgroundColor(getResources().getColor(R.color.cor_rosa_claro_fundo));
        donutProgress.setFinishedStrokeColor(getResources().getColor(R.color.cor_verde_escuro));
        donutProgress.setTextColor(getResources().getColor(R.color.cor_verde_escuro));

        donutProgress.setSuffixText("");
        new CountDownTimer(tempoCancelar, tickTime) {

            public void onTick(long millisUntilFinished) {
                if (dialog.isShowing()) {
                    long x = (tempoCancelar + tickTime - millisUntilFinished);
                    long percent = x * 100 / tempoCancelar;
                    donutProgress.setProgress((int) percent);
                }
            }

            public void onFinish() {
                if (dialog.isShowing()) {
                    donutProgress.setProgress(100);
                    try {
                        String listaPedidos = PojoMapper.toJson(adProdutos.getItems(), false);
                        ;
                        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("pedido", listaPedidos));
                        Log.e("teste", listaPedidos);
                        params.add(new BasicNameValuePair("id_estabelecimento", retorno.getEstablishments().getId()+""));
                        Log.e("teste",  retorno.getEstablishments().getId()+"");
                        new SendServerInfo(CardapioActivity.this, "add_order/", params).send(CardapioActivity.this, Constants.MENSAGEM_ENVIAR_PEDIDO);

                        limparPedido();
                    } catch (Exception ve) {
                        Log.e("teste", "DEU RUIM" + ve.getMessage(), ve);
                    }
                    dialog.cancel();
                }
            }
        }.start();
    }

    @Override
    public void receiveFromServer(String content, Integer code) {
        Log.e("teste", content);
        if (code.intValue() == Constants.MENSAGEM_CONSULTA_ESTABELECIMENTO) {
            try {
                retorno = (RetornoCardapio) PojoMapper.fromJson(content, RetornoCardapio.class);
                textViewNomeEstabelecimento.setText(retorno.getEstablishments().getName());
                textViewNomeEstabelecimento.setVisibility(View.VISIBLE);
                adProdutos = new AdapterProduto(this, R.layout.item_produto, retorno.getList_products());
                listView.setAdapter(adProdutos);
                atualizaPreco();
            } catch (Throwable e) {
                //serivor esta retornando os erro do php quando, não consegue criar uma comanda pois não existe estabelecimento
                Toast.makeText(CardapioActivity.this,"Estabelecimento inexistente.",Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (code.intValue() == Constants.MENSAGEM_ENVIAR_PEDIDO) {

        }
    }

    public void limparPedido() {
        total = 0;
        for (Produto p : retorno.getList_products()) {
            p.setQuantidade(0);
        }
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        textViewValor.setText(currencyFormatter.format(total));
        adProdutos = new AdapterProduto(this, R.layout.item_produto, retorno.getList_products());
        listView.setAdapter(adProdutos);
        findViewById(R.id.painelPeido).setVisibility(View.GONE);
    }

    public void atualizaPreco() {
        total = 0;
        for (Produto p : adProdutos.getItems()) {
            total += (p.getValue() * p.getQuantidade());
        }
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        textViewValor.setText(currencyFormatter.format(total));
       View painelPeido =  findViewById(R.id.painelPeido);
        if(total > 0){
            painelPeido.setVisibility(View.VISIBLE);
        }else{
            painelPeido.setVisibility(View.GONE);
        }
    }

}
