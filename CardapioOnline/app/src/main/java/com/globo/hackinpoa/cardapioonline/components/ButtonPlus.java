package com.globo.hackinpoa.cardapioonline.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.globo.hackinpoa.cardapioonline.helper.CustomFontHelper;

/**
 * Created by usuario on 11/04/2015.
 */
public class ButtonPlus extends Button {

    public ButtonPlus(Context context) {
        super(context);
    }

    public ButtonPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public ButtonPlus(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }
}