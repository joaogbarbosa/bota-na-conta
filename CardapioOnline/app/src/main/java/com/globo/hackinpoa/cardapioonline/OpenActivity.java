package com.globo.hackinpoa.cardapioonline;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.globo.hackinpoa.cardapioonline.utils.Constants;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


public class OpenActivity extends ActionBarActivity {
    String identificador;
    String mesa;
    Button btOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setBackgroundDrawable(getDrawable(R.drawable.ac_red));
        setContentView(R.layout.activity_open);
        btOK = (Button) findViewById(R.id.buttonOk);
        btOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goCardapio();
            }
        });

        Button btQRCode = (Button) findViewById(R.id.buttonQRCode);
        btQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(OpenActivity.this);
                //integrator.addExtra("SCAN_WIDTH", 640);
                //integrator.addExtra("SCAN_HEIGHT", 480);
                integrator.addExtra("SCAN_MODE", "QR_CODE_MODE");
                //customize the prompt message before scanning
                integrator.addExtra("PROMPT_MESSAGE", "Descobrindo estabelecimentos...");
                integrator.initiateScan(IntentIntegrator.QR_CODE_TYPES);
            }
        });

    }

    public void goCardapio() {
        Intent intent = new Intent(OpenActivity.this, CardapioActivity.class);
        String identificadorManual = ((EditText) findViewById(R.id.editTextCodigo)).getText().toString();
        if (identificadorManual != null && !identificadorManual.trim().equals("")) {
            identificador = identificadorManual;
        }
        intent.putExtra(Constants.identificadorEstabelecimento, identificador);
        intent.putExtra(Constants.identificadorMesa, mesa);
        startActivity(intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        ((EditText) findViewById(R.id.editTextCodigo)).setText("");
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (result != null) {
            String contents = result.getContents();
            if (contents != null) {
                identificador = result.getContents().trim();
                goCardapio();
            } else {
                showCamposManuais();
            }
        }
    }

    public void showCamposManuais() {
        identificador = null;
        btOK.setVisibility(View.VISIBLE);
        findViewById(R.id.editTextCodigo).setVisibility(View.VISIBLE);
    }
}
