package com.globo.hackinpoa.cardapioonline.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

/**
 * Created by usuario on 11/04/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Produto {
    private Integer id;
    private String name;
    private String categoria;
    private ArrayList<Composicao> composicao;
    private String status;
    private double value = 0;
    private int quantidade = 0;
    public Produto() {

    }
    public Produto(String name, ArrayList<Composicao> composicao, double value) {
        this.name = name;
        this.composicao = composicao;
        this.value = value;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public ArrayList<Composicao> getComposicao() {
        return composicao;
    }

    public void setComposicao(ArrayList<Composicao> composicao) {
        this.composicao = composicao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
