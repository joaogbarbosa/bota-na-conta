package com.globo.hackinpoa.cardapioonline;

import com.globo.hackinpoa.cardapioonline.Models.Produto;

/**
 * Created by usuario on 11/04/2015.
 */
public interface IReceiverProdutoAdd {

    public void add(Produto p);
}
