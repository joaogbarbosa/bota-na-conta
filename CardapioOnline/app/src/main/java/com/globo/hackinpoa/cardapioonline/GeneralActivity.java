package com.globo.hackinpoa.cardapioonline;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.globo.hackinpoa.cardapioonline.adapters.AdapterProduto;
import com.globo.hackinpoa.cardapioonline.utils.SendServerInfo;
import com.globo.hackinpoa.cardapioonline.utils.Util;

/**
 * Created by usuario on 12/04/2015.
 */
public abstract class GeneralActivity extends ActionBarActivity implements IReceiverServer {

    Button buttonChamarGarcon;
    View.OnClickListener onclickChamar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new SendServerInfo(GeneralActivity.this, "call", null).send();

            int random = Util.randInt(1, 3);
            switch (random) {
                case 1:
                    buttonChamarGarcon.setText(getString(R.string.call_1));
                    break;
                case 2:

                    buttonChamarGarcon.setText(getString(R.string.call_2));
                    break;
                case 3:
                    buttonChamarGarcon.setText(getString(R.string.call_3));
                    break;
            }
        }
    };


}
