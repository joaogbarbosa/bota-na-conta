package com.globo.hackinpoa.cardapioonline;

/**
 * Created by usuario on 11/04/2015.
 */
public interface IReceiverServer {
    public void receiveFromServer(String content, Integer code);
}
