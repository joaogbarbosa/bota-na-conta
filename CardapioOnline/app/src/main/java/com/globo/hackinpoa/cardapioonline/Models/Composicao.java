package com.globo.hackinpoa.cardapioonline.Models;

/**
 * Created by usuario on 11/04/2015.
 */
public class Composicao {
    private Integer id;

    private String descricao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
