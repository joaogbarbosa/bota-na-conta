package com.globo.hackinpoa.cardapioonline.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.globo.hackinpoa.cardapioonline.IReceiverServer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by usuario on 11/04/2015.
 */
public class SendServerInfo {

    String url;
    List<NameValuePair> listParams;
    Context context;

    public SendServerInfo(Context context, String url, List<NameValuePair> listParams) {
        this.url = "http://104.236.11.133/api/" + url;
        this.listParams = listParams;
        this.context = context;
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<NameValuePair> getListParams() {
        return listParams;
    }

    public void setListParams(List<NameValuePair> listParams) {
        this.listParams = listParams;
    }

    public void send(IReceiverServer receiver, Integer code) {
        new DownloadFilesTask(code, receiver).execute();
    }

    public void send() {
        send(null, null);
    }

    private class DownloadFilesTask extends AsyncTask<Void, Void, String> {
        Integer code;
        IReceiverServer receiver;

        public DownloadFilesTask(Integer code, IReceiverServer receiver) {
            this.code = code;
            this.receiver = receiver;
        }

        protected String doInBackground(Void... urls) {
            String retorno = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(SendServerInfo.this.url);

            try {
                if (listParams == null) {
                    listParams = new ArrayList<NameValuePair>();
                }
                //TODO remover por em outro lugar
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                listParams.add(new BasicNameValuePair("imei", telephonyManager.getDeviceId()));
                httppost.setEntity(new UrlEncodedFormEntity(listParams));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    InputStream instream = entity.getContent();

                    retorno = convertStreamToString(instream);
                    Log.i("Read from server", retorno);
                }


            } catch (ClientProtocolException e) {
                Log.i("Read from server", e.getMessage(), e);
            } catch (IOException e) {
                Log.i("Read from server", e.getMessage(), e);
            }
            return retorno;
        }


        protected void onPostExecute(String result) {
            if (result != null && receiver != null) {
                receiver.receiveFromServer(result, code);
            }
        }
    }
}
