package com.globo.hackinpoa.cardapioonline;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.globo.hackinpoa.cardapioonline.Models.Produto;
import com.globo.hackinpoa.cardapioonline.Models.RetornoCardapio;
import com.globo.hackinpoa.cardapioonline.adapters.AdapterProduto;
import com.globo.hackinpoa.cardapioonline.utils.Constants;
import com.globo.hackinpoa.cardapioonline.utils.PojoMapper;
import com.globo.hackinpoa.cardapioonline.utils.SendServerInfo;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;


//TODO classe clonada, deve ser refeita passando para uma clase pai
public class PedidosActivity extends GeneralActivity {


    RetornoCardapio retorno = null;
    TextView textViewCardapios;
    TextView textViewAguardando;
    TextView textViewNomeEstabelecimento;
    TextView textViewValor;
    Button buttonOKConta;
    AdapterProduto adProdutos;
    ListView listView;
    double total = 0;

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);
        setTitle("Pedido");
        getSupportActionBar().setBackgroundDrawable(getDrawable(R.drawable.ac_red));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.listViewCardapio);
        textViewValor = (TextView) findViewById(R.id.textViewValor);
        textViewCardapios = (TextView) findViewById(R.id.textViewCardapios);
        textViewCardapios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        textViewNomeEstabelecimento = (TextView) findViewById(R.id.textViewNomeEstabelecimento);
        findViewById(R.id.painelInferior).setVisibility(View.VISIBLE);
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString(Constants.nomeEstabelecimento) != null) {
            textViewNomeEstabelecimento.setText(getIntent().getExtras().getString(Constants.nomeEstabelecimento));
        } else {
            textViewNomeEstabelecimento.setVisibility(View.GONE);
        }

        buttonOKConta = (Button) findViewById(R.id.buttonOKConta);
        textViewAguardando = (TextView) findViewById(R.id.textViewAguardando);
        buttonOKConta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pagaar();
            }
        });

        buttonChamarGarcon = (Button) findViewById(R.id.buttonChamarGarcon);
        buttonChamarGarcon.setOnClickListener(onclickChamar);
        ((Button) findViewById(R.id.sino)).setOnClickListener(onclickChamar);

        adProdutos = new AdapterProduto(this, R.layout.item_produto_pedido);
        listView.setAdapter(adProdutos);
        //buscando do servidor as informações da ordem dele...id_estabelecimento
        ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
        String idEstabelecimento = getIntent().getExtras().getString(Constants.identificadorEstabelecimento);
        listParams.add(new BasicNameValuePair("id_estabelecimento",idEstabelecimento));
        new SendServerInfo(this, "list_order_itens/", listParams).send(this, Constants.MENSAGEM_CONSULTA_PEDIDO);

    }

    public void pagaar() {
        buttonOKConta.setVisibility(View.GONE);
        findViewById(R.id.painelInferior).setVisibility(View.GONE);
        textViewAguardando.setVisibility(View.VISIBLE);
        new SendServerInfo(PedidosActivity.this, "pay_order/", null).send(PedidosActivity.this, Constants.MENSAGEM_EFETUAR_PAGAMENTO);
    }

    public void openDialogConfirmaSair() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void receiveFromServer(String content, Integer code) {
        Log.e("teste", content);
        if (code.intValue() == Constants.MENSAGEM_CONSULTA_PEDIDO) {
            try {
                retorno = (RetornoCardapio) PojoMapper.fromJson(content, RetornoCardapio.class);
                adProdutos = new AdapterProduto(this, R.layout.item_produto_pedido, retorno.getList_products());
                listView.setAdapter(adProdutos);
                atualizaPreco();
            } catch (IOException e) {
                Log.e("teste", "deu ruim " + e.getMessage(), e);
            }
        } else if (code.intValue() == Constants.MENSAGEM_EFETUAR_PAGAMENTO) {
            textViewAguardando.setVisibility(View.GONE);
            findViewById(R.id.textViewPago).setVisibility(View.VISIBLE);
        }
    }

    public void limparPedido() {
        total = 0;
        for (Produto p : retorno.getList_products()) {
            p.setQuantidade(0);
        }
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        textViewValor.setText(currencyFormatter.format(total));
        adProdutos = new AdapterProduto(this, R.layout.item_produto_pedido, retorno.getList_products());
        listView.setAdapter(adProdutos);
    }

    public void atualizaPreco() {
        total = 0;
        for (Produto p : adProdutos.getItems()) {
            total += (p.getValue());
        }
        if (total > 0) {
            buttonOKConta.setVisibility(View.VISIBLE);
        }
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        textViewValor.setText(currencyFormatter.format(total));
    }
}
