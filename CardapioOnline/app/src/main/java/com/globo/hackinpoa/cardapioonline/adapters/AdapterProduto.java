package com.globo.hackinpoa.cardapioonline.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.globo.hackinpoa.cardapioonline.CardapioActivity;
import com.globo.hackinpoa.cardapioonline.Models.Produto;
import com.globo.hackinpoa.cardapioonline.R;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by usuario on 11/04/2015.
 */
public class AdapterProduto extends ArrayAdapter<Produto> {
    int resource;
    List<Produto> items;


    public AdapterProduto(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.resource = textViewResourceId;
    }

    public AdapterProduto(Context context, int resource, List<Produto> items) {
        super(context, resource, items);
        this.resource = resource;
        this.items = items;
    }

    public List<Produto> getItems() {
        return items;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(resource, null);
        }

        final Produto p = getItem(position);

        if (p != null) {
            TextView produtoDesc = (TextView) v.findViewById(R.id.textViewProduto);
            final TextView editTextQuantidade = (TextView) v.findViewById(R.id.editTextQuantidade);
            TextView valor = (TextView) v.findViewById(R.id.textViewValor);
            Button btAdd = (Button) v.findViewById(R.id.buttonAdd);
            editTextQuantidade.setText(String.valueOf(p.getQuantidade()));
            if (btAdd != null) {
                btAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        p.setQuantidade(p.getQuantidade() + 1);
                        editTextQuantidade.setText(String.valueOf(p.getQuantidade()));
                        //TODO melhorar o call back (este esta provisorio)
                        if (getContext() instanceof CardapioActivity) {
                            ((CardapioActivity) getContext()).atualizaPreco();
                        }
                    }
                });
            }

            TextView textViewStatus = (TextView) v.findViewById(R.id.textViewStatus);
            TextView bullet = (TextView) v.findViewById(R.id.bullet);
            if (textViewStatus != null) {
                if (p.getStatus() != null) {
                    bullet.setVisibility(View.VISIBLE);
                    textViewStatus.setVisibility(View.VISIBLE);
                    String descricaoFinal = "";
                    //necessario pois backend ta lascado...
                    if(p.getStatus().equals("new")){
                        descricaoFinal = "Já vamos atendê-lo";
                        bullet.setTextColor(getContext().getResources().getColor(R.color.cor_vermelho_claro));
                    }else if(p.getStatus().equals("pending")){
                        descricaoFinal = "Na produção";
                        bullet.setTextColor(getContext().getResources().getColor(R.color.cor_laranja));
                    }else if(p.getStatus().equals("delivered")){
                        descricaoFinal = "Tá na mesa";
                        bullet.setTextColor(getContext().getResources().getColor(R.color.cor_verde_escuro));
                    }
                    textViewStatus.setText(descricaoFinal);
                } else {
                    textViewStatus.setVisibility(View.GONE);
                    bullet.setVisibility(View.GONE);
                }
            }

            Button btMenos = (Button) v.findViewById(R.id.buttonMenos);
            if (btMenos != null) {
                btMenos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (p.getQuantidade() > 0) {
                            p.setQuantidade(p.getQuantidade() - 1);
                            editTextQuantidade.setText(String.valueOf(p.getQuantidade()));
                            //TODO melhorar o call back (este esta provisorio)
                            if (getContext() instanceof CardapioActivity) {
                                ((CardapioActivity) getContext()).atualizaPreco();
                            }
                        }
                    }
                });
            }


            if (produtoDesc != null) {
                produtoDesc.setText(p.getName());
            }
            if (valor != null) {

                valor.setText(getValorPt(p.getValue()));
            }
        }

        return v;

    }

    public String getValorPt(double valor) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        return currencyFormatter.format(valor);
    }
}
