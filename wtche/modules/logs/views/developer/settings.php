<?php if ($log_threshold == 0) : ?>
	<div class="alert alert-warning fade in">
		<a class="close" data-dismiss="alert">&times;</a>
		<?php echo lang('log_not_enabled'); ?>
	</div>
	<br>
<?php endif; ?>

<div class="alert alert-info fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<?php echo lang('log_big_file_note'); ?>
</div>
<br>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><?php echo $toolbar_title ?></h6></div>
    <div class="panel-body">

		<?php echo form_open(site_url(SITE_AREA .'/developer/logs/enable'), 'class="form-horizontal"'); ?>

			<div class="form-group">
				<label for="log_threshold" class="col-sm-2 control-label text-right"><?php echo lang('log_the_following'); ?></label>
				<div class="col-sm-10">
					<select class="select2" name="log_threshold" id="log_threshold">
						<option value="0" <?php echo ($log_threshold == 0) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_0'); ?></option>
						<option value="1" <?php echo ($log_threshold == 1) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_1'); ?></option>
						<option value="2" <?php echo ($log_threshold == 2) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_2'); ?></option>
						<option value="3" <?php echo ($log_threshold == 3) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_3'); ?></option>
						<option value="4" <?php echo ($log_threshold == 4) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_4'); ?></option>
					</select>

					<p class="help-block"><?php echo lang('log_what_note'); ?></p>
				</div>
			</div>

			<div class="form-actions text-right">
				<input type="submit" name="save" class="btn btn-success" value="<?php echo lang('log_save_button'); ?>" />
			</div>

		<?php echo form_close(); ?>
	</div>
</div>
