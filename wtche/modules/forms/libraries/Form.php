<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe responsável pela criação dos Forms Administrativos
 */
class Form {

	/**
	 * $this->wtche =& get_instance(); //on constructor
	 *
	 * @var CI Object
	 * @access public
	 */
	public $wtche;

	/**
	 * Guarda a referencia ao Model
	 *
	 * @var & WT_Model()
	 **/
	public $model;

	/**
	 * Guarda os fields adicionados ao Form Atual.
	 *
	 * @var array()
	 **/
	private $fields = array();
	
	/**
	 * Construtor
	 * @param &WT_Model $model Recebe a referência à um Model
	 */
	public function __construct($model)
	{

		$this->wtche =& get_instance();
		$this->wtche->load->library('forms/Field');

		$this->model = $model;

		/**
		 * Adiciona automaticamente um campo Hidden fazendo referência ao ID (key) do Model.
		 */
		$this->addField('hidden')
			->setFieldName($this->model->get_key())
			->hideFrom('edit') //Esconde do Edit pois lá já existe a referência
			->hideFrom('create'); //Esconde do create pois não existe valor nesse caso

	}

	
	/**
	 * Adiciona um Field ao Formulário
	 * @param String $field_type Tipo do campo a ser adicionado (Deverá coincidir com as classes disponíveis
	 *                           na pasta Fields).
	 */
	public function addField($field_type) {

		$field_type = strtolower($field_type);

		$field = $this->wtche->field->create($field_type);

		array_push($this->fields, $field);

		return $field;

	}

	/**
	 * Remove um Field ao Formulário
	 * @param String $field_name Nome do campo a ser removido
	 */
	public function removeField($field_name) {

		foreach ($this->fields as $key => $field) {
            if ($field->fieldName() == $field_name) {
                unset($this->fields[$key]);
                return true;
            }
        }

		return false;

	}

	/**
	 * Retorna os Fields adicionados ao Form
	 * @param  String $action Especifica qual tipo de ação ("list" ou "form")
	 * @return array() Retorna um array com os fields filtrados		
	 */
	public function getFields($action = 'list')
	{

		return array_filter($this->fields, function($field) use($action) {
			return !$field->isHideFrom($action);
		});

	}

	/**
	 * Retorna o Field do Form de acordo com o Nome
	 * @param  String $field_name Especifica qual field retornar
	 * @return Field		
	 */
	public function getField($field_name)
	{

		return current(array_filter($this->fields, function($field) use($field_name) {
			return $field->fieldName() == $field_name;
		}));

	}


}

/* End of file Form.php */
/* Location: ./application/libraries/Form/Form.php */