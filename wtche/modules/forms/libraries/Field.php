<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe responsável pela configuração dos Fields
 * Os métodos e atributos contidos aqui são disponíveis a todos os Fields
 */
class Field {

	/**
	 * $this->wtche =& get_instance(); //on constructor
	 *
	 * @var CI Object
	 * @access public
	 */
	public $wtche;

	/**
	 * Guarda o nome do Field (Deve coincidir com o nome do banco)
	 * @var String
	 */
	protected $field_name = null;
	public function setFieldName($field_name) {
		$this->field_name = $field_name;
		$this->setLabel(humanize($field_name));
		return $this;
	}
	public function fieldName() {
		return $this->field_name;
	}

	/**
	 * Guarda o valor do campo
	 * Caso o campo seja um Relation esse valor será um array(KEY,VALUE)
	 **/
	protected $value = null;
	public function setValue($value) {
		$this->value = $value;
		return $this;
	}
	public function value() {
		return $this->value;
	}

	/**
	 * Tamanho máximo de caracteres de um campo
	 **/
	protected $max_length = 0; //0 = Sem limite
	public function setMaxLength($max_length) {
		$this->max_length = $max_length;
		return $this;
	}
	public function maxLength() {
		return $this->max_length;
	}

	/**
	 * Caso o campo não deva ser adicionado em Banco deve-se setar esse valor para TRUE
	 **/
	protected $static = false;
	public function setStatic() {
		if ($this->isInlineEdit()) {
			throw new Exception("Campo InlineEdit não pode ser Static!", 1);
		}
		$this->static = TRUE;
		return $this;
	}
	public function isStatic() {
		return ($this->static === true);
	}

	/**
	 * Guarda as views de onde o campo será escondido
	 **/
	protected $hide_from = array();
	public function hideFrom($from) {
		array_push($this->hide_from, $from);
		return $this;
	}
	public function isHideFrom($from) {
		return in_array($from, $this->hide_from);
	}

	/**
	 * Label utilizada para impressão nas telas
	 **/
	protected $label = '';
	public function setLabel($label) {
		$this->label = $label;
		return $this;
	}
	public function label() {
		return $this->label;
	}

	/**
	 * Tipo do campo
	 **/
	protected $field_type;
	public function fieldType() {
		return $this->field_type;
	}
	public function isHidden() {

		return ($this->field_type == 'hidden');
	}

	/**
	 * Habilita Edição na Listagem
	 **/
	protected $inline_edit = false;
	public function setInlineEdit($inline_edit = true) {
		if ($this->isStatic()) {
			throw new Exception("Campo Static não pode ser InlineEdit!", 1);
		}
		$this->inline_edit = $inline_edit;
		return $this;
	}
	public function isInlineEdit() {

		return $this->inline_edit;
	}

	/**
	 * Possível Ordenar na Listagem?
	 **/
	protected $orderable = true;
	public function orderable() {
		return $this->orderable;
	}

	/**
	 * Validation Rules
	 **/
	protected $validation_rules = '';
	public function setValidationRules($validation_rules) {
		$this->validation_rules = $validation_rules;
		return $this;
	}
	public function validationRules() {

		return $this->validation_rules;
	}

	/**
	 * Máscara
	 **/
	protected $mask = false;
	public function setMask($mask) {
		$this->mask = $mask;
		return $this;
	}
	public function mask() {

		return $this->mask;
	}
	/**
	 * Configuração Máscara (Em um futuro distante deverá ser criada uma classe PHP para isso)
	 **/
	protected $mask_config = "{}";
	public function setMaskConfig($mask_config) {
		$this->mask_config = $mask_config;
		return $this;
	}
	public function maskConfig() {

		return $this->mask_config;
	}

	/**
	 * Construtor
	 **/
	public function __construct()
	{

		$this->wtche =& get_instance();

	}

	/**
	 * Carrega a classe necessária para criação do Field
	 * @param  String $field_type Tipo do Field a ser carregado
	 * @return Field Retorna um objeto Field
	 */	
	public function create($field_type) {

		//Caso algum Field necessite de tratamento especial no load deve ser adicionado ao switch
		switch ($field_type) {
			
			//Os Fields que podem ser carregados de forma padrão são carregados aqui
			default:

					
					$this->wtche->load->library('forms/Fields/' . ucfirst($field_type));
					
					//Verifica se a Classe existe
					if (class_exists(ucfirst($field_type))) {
						$field = new $field_type;
						break;
					}

					throw new Exception("Classe " . ucfirst($field_type) . " inexistente no arquivo!", 1);

				break;

		}

		$field->field_type = $field_type;

		return $field;

	}

	/**
	 * Função que renderiza o Field dentro do HTML
	 * @param  String $action A view com a estrutura do Field será carregada de acordo com essa variável
	 * @return HTML Retorna o HTML contendo a estrutura do Field
	 */
	public function render($action) {

		return $this->wtche->load->view('forms/fields/' . $this->field_type . '/' . $action, array('field'=>$this), true);

	}

}

/* End of file Field.php */