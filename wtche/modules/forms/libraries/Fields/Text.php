<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe para o Field Text
 */
class Text extends Field {

	/**
	 * Definie a utilização do CKEditor para este Field
	 **/
	protected $rich = FALSE;
	public function setRich() {
		$this->rich = TRUE;
		return $this;
	}
	public function isRich() {
		return $this->rich;
	}

	///////////////////////////////////////////////////////////////////////////
	// Métodos que não devem poder ser chamados dentro de um Field Text //
	///////////////////////////////////////////////////////////////////////////
	
	public function setInlineEdit($inline_edit = true) {
		throw new Exception("Campo Text não pode ser definido como InlineEdit!", 1);
	}

	public function setMask($mask_config) {
		throw new Exception("Campo Text não pode ser definido com Máscara!", 1);
	}

	public function setMaskConfig($mask_config) {
		throw new Exception("Campo Text não pode ser definido com Máscara!", 1);
	}

}

/* End of file Text.php */