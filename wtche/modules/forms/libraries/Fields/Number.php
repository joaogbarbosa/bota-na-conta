<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Campo tipo Number
 */
class Number extends Field {

	public function __construct()
	{
		parent::__construct();
		
		//Seta a Máscara padrão para somente Números
		$this->setMask('9#');
		$this->setMaskConfig('{maxlength: false}');

	}

}

/* End of file Number.php */