<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Select extends Field {

	private $arr_items = array();

	/**
	 * Adiciona um array de itens para ser usado no select.
	 * @param array $arr_itens O array a ser. 
	 */
	public function setItemsArray($arr_items){
		if ($this->items()) {
			throw new Exception("Este campo já possui itens!", 1);
		}
		if(!is_array($arr_items)):
			throw new Exception("Esperado array e recebido ".gettype($arr_items)." no arquivo ".__FILE__." na linha ".__LINE__.".", 1);
		endif;
		$this->arr_items = $arr_items;

		return $this;
	}

	/**
	 * Retorna os itens do select.
	 * @return array
	 */
	public function items() {
		return $this->arr_items;
	}

	/**
	 * Utilizado para relações entre tabelas (1 para 1)
	 * @var Mixed (false || array())
	 */
	protected $relation = false;
	public function setRelation($key, $table, $field, $where = null) {
		if ($this->items()) {
			throw new Exception("Este campo já esta com Itens definidos!", 1);
		}
		if ($this->relationMultiple()) {
			throw new Exception("Este campo já esta com relação múltipla a outra tabela!", 1);
		}
		$this->relation['key']   = $key;
		$this->relation['table'] = $table;
		$this->relation['field'] = $field;

		//Seta os itens de acordo com a tabela relacionada
		if ($where) {
			$this->wtche->db->where($where);
		}
		$result = $this->wtche->db
                    ->select($key)
                    ->select($field)
                    ->from($this->wtche->db->dbprefix($table))
                    ->get()->result_object();

        $items = array();
        foreach ($result as $value) {
        	$items[$value->{$key}] = $value->{$field};
        }

        $this->setItemsArray($items);

		return $this;
	}
	public function relation() {
		return $this->relation;
	}

	/**
	 * Utilizado para relações entre tabelas (N para M)
	 * @var Mixed (false || array())
	 */
	protected $relation_multiple = false;
	public function setRelationMultiple($model, $key, $table, $field) {
		if ($this->items()) {
			throw new Exception("Este campo já esta com Itens definidos!", 1);
		}
		if ($this->relation()) {
			throw new Exception("Este campo já esta com relação a outra tabela!", 1);
		}

		$this->orderable = FALSE;

		/////////////////////////////////
		//Criação da tabela auxiliar //
		/////////////////////////////////
		$this->wtche->load->dbforge();

		$table_aux = $model->get_table() . '_' . $table;

		$field_child = $model->get_table() . '_' . $model->get_key();
		$field_parent = $table . '_' . $key;

		if ($field_child == $field_parent) {
			$field_parent .= '_parent'; //Apenas para garantir que não sao dois campos como mesmo nome (Auto-relacionamento)
		}

		$fields = array(
          	$field_child => array(
				'type' => 'INT',
				'constraint' => '11',
			),
			$field_parent => array(
				'type' => 'INT',
				'constraint' => '11',
			)
        );
		$this->wtche->dbforge->add_field($fields);
		$this->wtche->dbforge->create_table($table_aux, TRUE);
		/////////////////////////////////////////
		//Fim: Criação da tabela auxiliar    //
		/////////////////////////////////////////

		$this->relation_multiple['table_aux']    = $table_aux;
		$this->relation_multiple['field_child']  = $field_child;
		$this->relation_multiple['field_parent'] = $field_parent;
		$this->relation_multiple['key']          = $key;
		$this->relation_multiple['table']        = $table;
		$this->relation_multiple['field']        = $field;

		//Seta os itens de acordo com a tabela relacionada
		$result = $this->wtche->db
                    ->select($key)
                    ->select($field)
                    ->from($this->wtche->db->dbprefix($table))
                    ->get()->result_object();

        $items = array();
        foreach ($result as $value) {
        	$items[$value->{$key}] = $value->{$field};
        }

        $this->setItemsArray($items);

		return $this;
	}
	public function relationMultiple() {
		return $this->relation_multiple;
	}

	/**
	 * Para o Select os values são tratados diferente dos demais Fields
	 * @param [type] $value [description]
	 */
	public function setValue($value, $row_id) {

		if ($this->items()) {

			if ($this->relationMultiple()) {

				$relation_multiple = $this->relationMultiple();

		        $result_aux = $this->wtche->db
		                    ->select($relation_multiple['field_parent'])
		                    ->from($this->wtche->db->dbprefix($relation_multiple['table_aux']))
		                    ->where($relation_multiple['field_child'],$row_id)
		                    ->get()->result_object();

		        $value = array();

		        foreach ($result_aux as $row_aux) {
		            
		            $result = $this->wtche->db
		                    ->select($relation_multiple['field'])
		                    ->from($this->wtche->db->dbprefix($relation_multiple['table']))
		                    ->where($relation_multiple['key'],$row_aux->{$relation_multiple['field_parent']})
		                    ->get();

		            $row = current($result->result_object());

		            @$value[$row_aux->{$relation_multiple['field_parent']}] = $row->{$relation_multiple['field']};

		        }

		        $this->value = $value;

			} else {

				$items = $this->items();
				if (isset($items[$value])) {
					$value_text = $items[$value];
				} else {
					$value_text = '<span class=text-danger>[' . $value . ']</span>';
				}
				$this->value = array(
					$value => $value_text
				);

			}
			
			return $this;
		}

		throw new Exception("Field Select sem Elementos Setados!", 1);
	}

	/**
	 * Salva os dados do Select Multiplo no banco
	 * @return bool
	 */
	public function save($row_id, $post_data) {

		//Se for um Select Multiplo
        if ($this->relationMultiple()) {

            $relation_multiple = $this->relationMultiple();

            //Deleta todas as referencias na tabela auxiliar 
            $this->wtche->db
                ->where($relation_multiple['field_child'], $row_id)
                ->delete($relation_multiple['table_aux']);

            if ($post_data) {
                foreach ($post_data as $value) {
                    $relation_data[] = array(
                        $relation_multiple['field_child'] => $row_id,
                        $relation_multiple['field_parent'] => $value
                    );
                }
                if (!$this->wtche->db->insert_batch($relation_multiple['table_aux'], $relation_data)) {
                	return FALSE;
                }
            }

        }

        return TRUE;

	}

}

/* End of file Select.php */
/* Location: ./application/libraries/Form/Fields/Select.php */