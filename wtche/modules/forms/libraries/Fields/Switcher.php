<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe para Switcher Field
 * Campo utilizado para "Ativo/Inativo"
 */
class Switcher extends Field {

	/**
	 * Label utilizada para impressão nas telas quando o status for "OFF"
	 **/
	protected $label_off = '<i class="icon-close"></i>';
	public function setLabelOff($label_off) {
		$this->label_off = $label_off;
		return $this;
	}
	public function labelOff() {
		return $this->label_off;
	}

	/**
	 * Label utilizada para impressão nas telas quando o status for "ON"
	 **/
	protected $label_on = '<i class="icon-checkmark"></i>';
	public function setLabelOn($label_on) {
		$this->label_on = $label_on;
		return $this;
	}
	public function labelOn() {
		return $this->label_on;
	}

	/**
	 * Valor utilizada quando o status for "OFF"
	 **/
	protected $value_off = 0;
	public function setValueOff($value_off) {
		$this->value_off = $value_off;
		return $this;
	}
	public function valueOff() {
		return $this->value_off;
	}

	/**
	 * Valor utilizada quando o status for "ON"
	 **/
	protected $value_on = 1;
	public function setValueOn($value_on) {
		$this->value_on = $value_on;
		return $this;
	}
	public function valueOn() {
		return $this->value_on;
	}

	/**
	 * Valor utilizada quando o status for "ON"
	 **/
	protected $default_on = false;
	public function setDefaultOn() {
		$this->default_on = true;
		return $this;
	}
	public function defaultOn() {
		return $this->default_on;
	}	

	public function __construct()
	{
		parent::__construct();
	}

	///////////////////////////////////////////////////////////////////////////
	// Métodos que não devem poder ser chamados dentro de um Field Switcher //
	///////////////////////////////////////////////////////////////////////////

	public function setMask($mask_config) {
		throw new Exception("Campo Switcher não pode ser definido com Máscara!", 1);
	}

	public function setMaskConfig($mask_config) {
		throw new Exception("Campo Switcher não pode ser definido com Máscara!", 1);
	}

}

/* End of file Switcher.php */