<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File_Upload extends Field {

	public function __construct()
	{
		$this->orderable = FALSE;
		parent::__construct();
	}

	/**
	 * Definie o limite de arquivos
	 **/
	protected $max_files = 1;
	public function setMaxFiles($max_files) {
		$this->max_files = $max_files;
		return $this;
	}
	public function maxFiles() {
		return $this->max_files;
	}

	/**
	 * Definie o limite de tamanho dos arquivos em K
	 **/
	protected $max_size = '500'; //padrão 500K
	public function setMaxSize($max_size) {
		$this->max_size = $max_size;
		return $this;
	}
	public function maxSize() {
		return $this->max_size;
	}

	/**
	 * Definie os formatos de arquivos aceitos (Separados por | (pipe))
	 **/
	protected $allowed_types = '*'; //* = Todos
	public function setAllowedTypes($allowed_types) {
		$this->allowed_types = $allowed_types;
		return $this;
	}
	public function allowedTypes() {
		return $this->allowed_types;
	}

	/**
	 * Para setar o field_name de um upload precisamos do model correspondente também
	 * pois vamos guardar um nome de tabela auxiliar
	 */
	protected $table_name;
	protected $fk;
	public $model;
	public function setFieldName($model, $field_name) {
		
		/////////////////////////////////
		//Criação da tabela auxiliar //
		/////////////////////////////////
		$this->wtche->load->dbforge();

		$table_aux = $model->get_table() . '_' . strtolower($field_name);

		$this->fk = $model->get_table() . '_' . $model->get_key();

		$fields = array(
          	$model->get_key() => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			$this->fk => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'file_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'file_name_original' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'file_order' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			),
			'is_valid' => array(
				'type' => 'SMALLINT',
				'default' => 0
			)
        );
		$this->wtche->dbforge->add_field($fields);
		$this->wtche->dbforge->add_key($model->get_key(), true);
		$this->wtche->dbforge->create_table($table_aux, TRUE);
		/////////////////////////////////////////
		//Fim: Criação da tabela auxiliar    //
		///////////////////////////////////////// 
		
		$this->table_name = $table_aux;

		$this->model = $model;

		return parent::setFieldName($field_name);
	}
	public function get_table() {
		return $this->table_name;
	}
	public function fk() {
		return $this->fk;
	}

	/**
	 * Para o Upload os values são tratados diferente dos demais Fields
	 * @param [type] $value [description]
	 */
	public function setValue($value, $row_id) {

		$result = $this->wtche->db
                ->from($this->get_table())
                ->where($this->fk,$row_id)
                ->order_by('file_order')
                ->get()->result_object();

        $this->value = $result;

        return $this;

	}

	/**
	 * Salva os dados do Upload no banco
	 * @return bool
	 */
	public function save($row_id, $post_data, $just_insert = false) {

		if (!$just_insert) {
			//Deleta todas as referencias na tabela dos files 
	        $this->wtche->db
	            ->where($this->fk(), $row_id)
	            ->delete($this->get_table());
		}

        if ($post_data) {

        	$order = 1; //inicia a ordem em 1

            foreach ($post_data as $file_name => $file) {
                $file_data = array(
                    $this->fk() => $row_id,
                    'file_name' => $file_name,
                    'file_name_original' => $file['orig_name'],
                    'file_order' => $order++,
                    'is_valid' => 1
                );
                if ($this->wtche->db->insert($this->get_table(), $file_data)) {
                	//Move o arquivo para a posição correta
                	$path = 'uploads/' . $this->get_table();
                	@rename($path . '/_temp/' . $file_name, $path . '/' . $file_name);
                } else {
	            	return FALSE;
	            }
            }
        }

        return TRUE;

	}

	///////////////////////////////////////////////////////////////////////////
	// Métodos que não devem poder ser chamados dentro de um File Upload //
	///////////////////////////////////////////////////////////////////////////

	public function setMask($mask_config) {
		throw new Exception("Campo Switcher não pode ser definido com Máscara!", 1);
	}

	public function setMaskConfig($mask_config) {
		throw new Exception("Campo Switcher não pode ser definido com Máscara!", 1);
	}

}

/* End of file File_Upload.php */
/* Location: ./application/libraries/Form/Fields/File_Upload.php */