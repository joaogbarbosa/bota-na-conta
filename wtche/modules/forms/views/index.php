

<script type="text/javascript">
    
    $(function(){

        oTable = $('.datatable table').on( 'processing.dt', function ( e, settings, processing ) {
            if (processing) {
                $('.datatable-scroll').append(
                    $('<div>')
                        .addClass('overlay')
                        .css('position','absolute')
                        .append(
                            $('<div>')
                                .addClass('opacity')
                        )
                        .append(
                            $('<i>')
                                .addClass('icon-spinner2 spin')
                        )
                );
                $('.overlay').fadeIn(100);
            } else {
                $('.overlay').fadeOut(150, function() {
                    $(this).remove();
                });
            }
        }).dataTable({
            serverSide: true,
            ajax: {
                url: "<?php echo site_url(SITE_AREA . '/' . $context_name . '/' . $module_name . '/get_list' . (($query_string)?('?'.$query_string):'')) ?>",
                type: "POST"
            },
            columns: [

                <?php if ($can_delete): ?>
                    { data: "<?php echo $key ?>", 
                        visible: <?php echo json_encode($can_delete) ?>,
                        orderable: false, 
                        searchable: false,
                        className: "column-check text-center",
                        render: function ( data, type, full, meta ) {
                            return $('<label>')
                                .addClass('checkbox-inline checkbox-danger')
                                .append($('<input>')
                                    .addClass('styled')
                                    .attr('type','checkbox')
                                    .attr('name','checked[]')
                                    .val(data)
                            ).get()[0].outerHTML;
                        }
                    },
                <?php endif ?>

                <?php foreach ($fields as $field): ?>
                    <?php if(!$field->isHidden()): ?>
                        { 
                            data: "<?php echo $field->fieldName() ?>",
                            orderable: <?php echo json_encode($field->orderable()) ?>,
                            render: function ( data, type, full, meta ) {
                                var view = <?php echo json_encode($field->render('list')) ?>;
                                if ($.isPlainObject(data) || $.isArray(data)) {
                                    return view.replace(/{{data}}/g, JSON.stringify(data)).replace(/{{key}}/g, full.<?php echo $key ?>);
                                } else{
                                    return view.replace(/{{data}}/g, data).replace(/{{key}}/g, full.<?php echo $key ?>);
                                }
                            }
                        },
                    <?php endif ?>
                <?php endforeach ?>

                <?php if ($can_edit): ?>
                    { data: "<?php echo $key ?>",
                        orderable: false, 
                        searchable: false,
                        className: "text-center",
                        render: function ( data, type, full, meta ) {
                            return $('<a>')
                                .attr('href','<?php echo site_url(SITE_AREA . "/" . $context_name . "/" . $module_name . "/edit") ?>/' + data + '<?php echo (($query_string)?("?".$query_string):"") ?>')
                                .addClass('btn btn-primary btn-icon')
                                .append($('<i>')
                                    .addClass('icon-pencil')
                                ).get()[0].outerHTML;
                        }
                    }
                <?php endif ?>

            ],
            pagingType: "simple_numbers",
            order: [],
            stateSave: true,
            dom: '<"datatable-header"Tfl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: "<span><?php echo lang('datatables_search') ?>:</span> _INPUT_",
                lengthMenu: "<span><?php echo lang('datatables_lengthMenu') ?>:</span> _MENU_",
                paginate: { "previous": "←", "next": "→" },
                info: "<?php echo lang('datatables_info') ?>",
                emptyTable: "<span class='text-danger'><?php echo sprintf(lang('datatables_emptyTable'), $module_title) ?></span>",
                zeroRecords: "<?php echo lang('datatables_zeroRecords') ?>",
                infoEmpty: "<?php echo lang('datatables_infoEmpty') ?>",
                infoFiltered: "<?php echo lang('datatables_infoFiltered') ?>",
                processing: "<?php echo lang('datatables_processing') ?>"
            },
            oTableTools: {
                sSwfPath: base_url + "themes/wtche/media/swf/copy_csv_xls_pdf.swf",
                aButtons: [
                    // {
                    //     sExtends:    "collection",
                    //     sButtonText: "Salvar <span class='caret'></span>",
                    //     sButtonClass: "btn btn-default",
                    //     aButtons:    [ "csv", "xls", "pdf" ]
                    // }
                ]
            },
            initComplete: function( settings, json ) {
                <?php if ($can_delete): ?>
                    $('.datatable-footer').prepend(
                        $('<a>')
                            .addClass('btn btn-danger pull-left')
                            .attr('data-toggle','modal')
                            .attr('role','button')
                            .attr('href','#delete_modal')
                            .css('margin', 10)
                            .text("<?php echo lang('module_action_delete_batch'); ?>")
                    );
                <?php endif ?>
            },
            drawCallback: function( settings ) {
                $('.check-all').attr('checked', false);
                _ui();
            },
            
        });

        //===== Datatable select =====//

        $(".dataTables_length select").select2({
            minimumResultsForSearch: "-1"
        });

        //===== Datatable filter =====//

        $('.dataTables_filter input[type=search]').attr('placeholder','<?php echo lang("datatables_search_placeholder") ?>');

    });

</script>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><?php echo $toolbar_title ?></h6></div>
    <?php echo form_open($this->uri->uri_string()); ?>
    <div class="datatable">
        <table class="table table-bordered" width="100%">
            <thead>
                <tr>
                    <?php if ($can_delete): ?>
                        <th width="40" class="text-center">
                            <label class="checkbox-inline checkbox-danger">
                                <input type="checkbox" class="check-all styled">
                            </label>
                        </th>
                    <?php endif ?>
                    <?php foreach ($fields as $field): ?>
                        <?php if(!$field->isHidden()): ?>
                            <th><?php echo $field->label() ?></th>
                        <?php endif ?>
                    <?php endforeach ?>
                    <?php if ($can_edit): ?>
                        <th width="40" class="text-center">
                            <?php echo lang('module_edit') ?>
                        </th>
                    <?php endif ?>
                </tr>
            </thead>
        </table>
    </div>
    <!-- Delete Confirm modal -->
    <div id="delete_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="icon-notification"></i> <?php echo lang('module_action_delete_batch_title') ?></h4>
                </div>

                <div class="modal-body with-padding">
                    <p><?php echo lang("module_delete_confirm") ?></p>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal"><?php echo lang('module_cancel') ?></button>
                    <button type="submit" name="delete" id="delete-me" class="btn btn-danger"><?php echo lang('module_delete_record') ?></button>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Confirm modal -->
    <?php echo form_close(); ?>
</div>