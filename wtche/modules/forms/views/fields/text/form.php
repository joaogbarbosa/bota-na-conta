<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">

	<?php echo form_label($field->label(), $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<textarea data-validation-engine="validate[<?php echo $field->validationRules() ?>]" rows="10" class="form-control" name="<?php echo $field->fieldName() ?>"><?php echo set_value($field->fieldName(), $field->value()); ?></textarea>
	</div>

</div>

<script type="text/javascript">
	$(function(){

		var field = $('textarea[name="<?php echo $field->fieldName() ?>"]');

		<?php if ($field->isRich()): ?>
			field.wysihtml5({
			    stylesheets: "<?php echo base_url('assets/_admin/css/wysiwyg-color.css') ?>"
			});
		<?php endif ?>

		<?php if ($field->maxLength()): ?>
			field.inputlimiter({
				limit: <?php echo $field->maxLength(); ?>,
				boxAttach: false
			});
		<?php endif ?>

	});
</script>