<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">

	<?php echo form_label($field->label(), $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<a href="<?php echo site_url(SITE_AREA .'/'. $context_name .'/'. $field->module() . '?template=form&filter=' . $field->value()) ?>" class="btn btn-default btn-icon lightbox fancybox.iframe"><i class="icon-list"></i></a>
	</div>

</div>