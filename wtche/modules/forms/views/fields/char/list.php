<?php if ($field->isInlineEdit() && $can_edit): ?>

	<script type="text/javascript">

		$(function(){

			var field = $('input[name="<?php echo $field->fieldName() ?>"][data-key="{{key}}"]');

			field.on('change', function( event, ui ) {
				if (!field.validationEngine('validate')) { //Sim! É ao contrário mesmo...
					$.ajax({
						type: "POST",
						url: '<?php echo site_url($field->wtche->uri->uri_string."/save_ajax") ?>/{{key}}',
						data: {
							<?php echo $field->fieldName() ?>: field.val()
						},
						success: function(data){
							_field_update_status(field, 'Campo <?php echo $field->label() ?> atualizado', 'success');
						},
						error: function(data){
							$.each(data.responseJSON, function(index, message) {
								_field_update_status(field, message, 'error');
							});
						},
						dataType: "json"
					});
				}
			});
			
			<?php if ($field->mask()): ?>
				field.mask('<?php echo $field->mask() ?>', <?php echo $field->maskConfig() ?>);
			<?php endif ?>

			<?php if ($field->maxLength()): ?>
				field.inputlimiter({
					limit: <?php echo $field->maxLength(); ?>,
					boxAttach: false
				});
			<?php endif ?>

		});

	</script>

	<div>
		<input data-validation-engine="validate[<?php echo $field->validationRules() ?>]" name="<?php echo $field->fieldName() ?>" data-key="{{key}}" class="form-control spinner" type="text" value="{{data}}" />
	</div>
	
<?php else: ?>
	{{data}}
<?php endif ?>