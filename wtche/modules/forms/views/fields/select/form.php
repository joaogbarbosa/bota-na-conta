<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">
	<?php if ($field->relationMultiple()): ?>
		<input type="hidden" name="<?php echo $field->fieldName() ?>[]" value="0" />
	<?php endif ?>
	<?php echo form_label($field->label(), $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<select <?php echo iif($field->relationMultiple(), 'multiple="multiple"') ?> data-validation-engine="validate[<?php echo $field->validationRules() ?>]" name="<?php echo $field->fieldName() ?><?php echo iif($field->relationMultiple(), '[]') ?>" class="select2">
            <?php foreach ($field->items() as $key => $value): ?>
                <option value="<?php echo $key ?>" <?php echo set_select($field->fieldName(),$key,($field->value() && array_key_exists($key, $field->value())));?>><?php echo $value ?></option>
            <?php endforeach ?>
        </select>
	</div>
</div>