<?php if(($field->isInlineEdit() && $can_edit) || $field->relationMultiple()):?>

	<script type="text/javascript">

		$(function(){

			var field = $('select[name="<?php echo $field->fieldName() ?>"][data-key="{{key}}"]');

			var obj_data = [];

			if ('{{data}}') {
				obj_data = $.parseJSON('{{data}}');
			}

			$.each(obj_data, function(i, data) {
				field.find('option[value="' + i + '"]').attr('selected', true);
			});

			<?php if($field->isInlineEdit()): ?>
				field.on('change', function( event, ui ) {
					if (!field.validationEngine('validate')) { //Sim! É ao contrário mesmo...
						$.ajax({
							type: "POST",
							url: '<?php echo site_url($field->wtche->uri->uri_string."/save_ajax") ?>/{{key}}',
							data: {
								<?php echo $field->fieldName() ?>: field.val()
							},
							success: function(data){
								_field_update_status(field, 'Campo <?php echo $field->label() ?> atualizado', 'success');
							},
							error: function(data){
								$.each(data.responseJSON, function(index, message) {
									_field_update_status(field, message, 'error');
								});
							},
							dataType: "json"
						});
					}
				});
			<?php endif ?>

		});

	</script>

	<div>
		<select <?php echo iif($field->relationMultiple(), 'multiple="multiple"') ?> <?php echo iif($field->relationMultiple() && !$field->isInlineEdit(), 'disabled') ?> data-validation-engine="validate[<?php echo $field->validationRules() ?>]" name="<?php echo $field->fieldName() ?>" data-key="{{key}}" class="select2">
			<option></option>
			<?php foreach ($field->items() as $key => $value): ?>
				<option value="<?php echo $key ?>"><?php echo $value ?></option>
			<?php endforeach ?>
		</select>
	</div>

<?php else: ?>
	<script type="text/javascript">
		var field = $('span[data-name="<?php echo $field->fieldName() ?>"][data-key="{{key}}"]');

		var obj_data = $.parseJSON('{{data}}');

		$.each(obj_data, function(i, data) {
			field.html(data);
		});
	</script>
	<span data-name="<?php echo $field->fieldName() ?>" data-key="{{key}}">
		---
	</span>
<?php endif; ?>
