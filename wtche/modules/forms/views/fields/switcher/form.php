
<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">

	<?php echo form_label($field->label(), $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<?php
			if (!$field->value()) {
				if ($field->defaultOn()) {
					$field->setValue($field->valueOn());
				}
			}
		?>
		<input data-validation-engine="validate[<?php echo $field->validationRules() ?>]" type="checkbox" class="form-control" name="<?php echo $field->fieldName() ?>" value="<?php echo $field->valueOn() ?>" <?php echo set_checkbox($field->fieldName(), $field->valueOn(), ($field->valueOn() == $field->value())) ?> data-on-text='<?php echo $field->labelOn() ?>' data-off-text='<?php echo $field->labelOff() ?>'>
	</div>

</div>

<script type="text/javascript">

		$(function(){

			var field = $('input[name="<?php echo $field->fieldName() ?>"]');

			field.bootstrapSwitch({
				state: field.attr('checked')
			});

		});

	</script>