<?php if ($field->isInlineEdit() && $can_edit): ?>

	<script type="text/javascript">

		$(function(){

			var field = $('input[name="<?php echo $field->fieldName() ?>"][data-key="{{key}}"]');

			field.bootstrapSwitch({
				onSwitchChange: function(event, state){
					$.ajax({
						type: "POST",
						url: '<?php echo site_url($field->wtche->uri->uri_string."/save_ajax") ?>/{{key}}',
						data: {
							<?php echo $field->fieldName() ?>: Number(state)
						},
						success: function(data){
							_field_update_status(field, 'Campo <?php echo $field->label() ?> atualizado', 'success');
						},
						error: function(data){
							$.each(data.responseJSON, function(index, message) {
								_field_update_status(field, message, 'error');
							});
						},
						dataType: "json"
					});
				},
				state: '{{data}}' == '<?php echo $field->valueOn() ?>',
				size: 'small'
			});

		});

	</script>

	<input type="checkbox" data-key="{{key}}" name="<?php echo $field->fieldName() ?>" data-on-text='<?php echo $field->labelOn() ?>' data-off-text='<?php echo $field->labelOff() ?>'>
	
<?php else: ?>

	<script type="text/javascript">

	var field = $('span[data-name="<?php echo $field->fieldName() ?>"][data-key="{{key}}"]');

	if ('{{data}}' == '<?php echo $field->valueOn() ?>') {
		field.addClass('label-success').text('<?php echo $field->labelOn() ?>');
	} else {
		field.addClass('label-danger').text('<?php echo $field->labelOff() ?>');
	}

	</script>
	
	<span data-key="{{key}}" data-name="<?php echo $field->fieldName() ?>" class="label"></span>
	
<?php endif ?>