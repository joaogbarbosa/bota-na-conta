<div class="form-group <?php echo form_error($field->fieldName()) ? 'has-error' : ''; ?>">

	<?php echo form_label($field->label(), $field->fieldName(), array('class' => 'col-sm-2 control-label text-right') ); ?>
	<div class='col-sm-10'>
		<div class="file_upload">
			<ul class="files">
				
			</ul>
			<input type="file" name="<?php echo $field->fieldName() ?>" multiple>
		</div>
	</div>

</div>

<script type="text/javascript">

	(function() { 

		var previewSize = 80;
		var field = 'input[name="<?php echo $field->fieldName() ?>"]';

		$(function(){

			var obj_data = [];

			<?php if ($field->value()): ?>
				obj_data = $.parseJSON('<?php echo json_encode($field->value()) ?>');
			<?php endif ?>

			$.each(obj_data, function(i, data) {
				
				getFileBox(data, function(filebox) {
					$(field).parents('.file_upload').find('.files').append(filebox);
				});

			});

			$(field).data('numberOfFiles', $(field).closest('.file_upload').find('ul.files li').size());

			$(field).fileupload({
				url: '<?php echo site_url(SITE_AREA . "/" . $context_name . "/" . $module_name) ?>/do_upload',
				sequentialUploads: true,
				dropZone: field,
				acceptFileTypes: /(\.|\/)(<?php echo $field->allowedTypes() ?>)$/i,
				maxFileSize: <?php echo $field->maxSize()*1000 ?>,
				maxNumberOfFiles: <?php echo $field->maxFiles() ?>,
				getNumberOfFiles: function() {
					console.log('getNumberOfFiles: ' + $(field).data('numberOfFiles'));
					return $(field).data('numberOfFiles');
				},
				//Configs dos Previews
				previewMaxWidth: previewSize,
		        previewMaxHeight: previewSize,
		        previewCrop: true,
			    //Form Data
		        formData: {
		        	<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>',
		        	key: '<?php echo (isset(${$module_name}))?${$module_name}->{$key}:"" ?>',
		        	field_name: '<?php echo $field->fieldName() ?>'
		        }
		    }).on('fileuploadadd', function (e, data) {

		    	data.context = renderFileBox(null);
		    	$(field).parents('.file_upload').find('.files').append(data.context);

		    }).on('fileuploadprocessalways', function (e, data) {

		        var index = data.index,
		            file = data.files[index];

		        if (file.preview) {
		            data.context.find('.canvas')
		                .prepend($(file.preview).css('border-radius', 2));
		        } else {
					data.context.find('.canvas')
						.prepend($('<img>')
							.attr('src', '<?php echo site_url("thumbs") ?>/' + file.name + '?path=<?php echo $field->get_table() ?>&size='+previewSize)
							.css('border-radius', 2)
						);
		        }

		        if (!data.files[0].error) {
		        	$(field).data('numberOfFiles', ($(field).data('numberOfFiles')||0) + 1);
		        } else if(data.files[0].error) {
		        	setFileError(data, data.files[0].error);
		        }

		    }).on('fileuploadprogress', function (e, data) {

		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        data.context.find('.progress .progress-bar')
		        	.width(progress+'%')
		        	.attr({
		        		'aria-valuenow': progress
		        	});

		    }).on('fileuploaddone', function (e, data) {

		    	data.context.find('.progress').remove();
		    	data.context.find('.canvas').fadeTo('400', 1);
		    	getFileBox(data.result, function(filebox) {
					data.context.replaceWith(filebox);
				});
		    	

		    }).on('fileuploadfail', function (e, data) {
		    	
		    	$(field).data('numberOfFiles', ($(field).data('numberOfFiles')||0) - 1);
		    	setFileError(data, data.jqXHR.responseJSON.error);

		    });

		    $('.file_upload .files').sortable({
				update: function(event, ui) {
					
					setFilesOrder($(this));
					
				}
			});

			function setFileError(data, error_message) {
				data.context
					.attr('data-original-title',error_message).tooltip()
					.append($('<i>')
						.addClass('icon-close')
						.css({
			    			color: '#E48561',
			    			position: 'absolute',
			    			right: 3,
			    			top: 3,	
			    			'font-size': 10,
			    			cursor: 'pointer'
			    		})
			    		.click(function(){
			    			data.context.tooltip("destroy");
			    			data.context.remove();
			    		})
			    	)
					.find('.progress').remove();
			    _field_update_status(null, error_message, 'error');
			}

			function getFileBox(file, callback) {

				if ($.isPlainObject(file)) {
					callback(renderFileBox(file));
				} else if (file != null) {
					$.ajax({
						type: "GET",
						url: '<?php echo site_url($field->wtche->uri->uri_string."/get_file/".$field->fieldName()) ?>/' + file,
						success: function(data){
							callback(renderFileBox(data));
						},
						error: function(data){
							//?
						},
						dataType: "json"
					});
				}

			}

			function renderFileBox(file) {

				if (file == null) {
					file = {};
				}

				//Cria o box principal
				var box = $('<li>')
						.attr('data-key', file.<?php echo $field->model->get_key() ?>)
						.addClass('context')
						.css({
							width: previewSize,
							height: previewSize
						})
						.append($('<div>')
							.addClass('canvas')
							.css({
								width: previewSize,
								height: previewSize
							})
						);

				//Se tiver arquivo já ele cria o box completo
			    if (file.file_name) {

			    	if (file.<?php echo $field->model->get_key() ?>) {
			    		box.find('.canvas').wrap(
			    			$('<a>')
		    					.attr('href','<?php echo site_url("thumbs") ?>/' + file.file_name + '?path=<?php echo $field->get_table() ?>')
		    					.addClass('lightbox')
			    				.fancybox()
						).append($('<img>')
			        		.attr('src','<?php echo site_url("thumbs") ?>/' + file.file_name + '?path=<?php echo $field->get_table() ?>&size=' + previewSize)
			        		.css({
								width: previewSize,
								height: previewSize,
								'border-radius': 2
							})
			        	);
			    	} else {
			    		box.find('.canvas').wrap(
			    			$('<a>')
		    					.attr('href','<?php echo site_url("thumbs") ?>/' + file.file_name + '?path=<?php echo $field->get_table() ?>/_temp')
		    					.addClass('lightbox')
			    				.fancybox({
									padding: 1
								})
						).append($('<img>')
			        		.attr('src','<?php echo site_url("thumbs") ?>/' + file.file_name + '?path=<?php echo $field->get_table() ?>/_temp&size=' + previewSize)
			        		.css({
								width: previewSize,
								height: previewSize,
								'border-radius': 2
							})
			        	);
			    	}

			    	
			    	box.append($('<i>')
						.addClass('icon-close remove-file')
						.click(function(){
							bootbox.dialog({
							  message: "Tem certeza que deseja deletar este arquivo?",
							  title: "Atenção",
							  buttons: {
							    main: {
							      label: "Cancelar",
							      className: "btn-default"
							    },
							    danger: {
							      label: "Deletar",
							      className: "btn-danger",
							      callback: function() {
							      	$.ajax({
										type: "POST",
										url: '<?php echo site_url(SITE_AREA . '/' . $context_name.'/'.$module_name."/delete_upload") ?>',
										data: {
											'file_name': file.file_name,
											'field_name': '<?php echo $field->fieldName() ?>'
										},
										success: function(data){
											box.remove();
											$(field).data('numberOfFiles', ($(field).data('numberOfFiles')||0) - 1);
											_field_update_status(null, 'Arquivo deletado', 'success');
										},
										error: function(data){
											_field_update_status(null, 'Erro ao deletar o arquivo', 'error');
										},
										dataType: "json"
									});
							      }
							    }
							  }
							});
						})
			    	);
			    	
			    	box.append($('<input>')
			    		.attr('type','hidden')
			    		.attr('name','<?php echo $field->fieldName() ?>[' + file.file_name + '][orig_name]')
			    		.attr('value',file.orig_name || file.file_name_original)
			    	);

			    //Senão ele cria uma barra de progresso pois está fazendo upload
			    } else {
			    	box.find('.canvas').css('opacity','0.5');
					box.append($('<div>')
						.addClass('progress progress-micro')
						.append($('<div>')
							.addClass('progress-bar progress-bar-info')
							.attr({
								'role': 'progressbar',
								'aria-valuenow': '0',
								'aria-valuemin': '0',
								'aria-valuemax': '100'
							})
						)
					);
			    }
							
			    return box;
			}

		});

	})();

</script>