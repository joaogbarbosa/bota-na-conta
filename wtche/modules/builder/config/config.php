<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(
	'description'	=> 'Ajuda na hora de criar novos módulos',
	'author'		=> 'WTche Team',
	'name'			=> 'Module Builder'
);