<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtagencia.com/#license
 * @link      http://www.wtagencia.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Module Builder Controller
 *
 * @package    WTchê
 * @category   Controllers
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */
class Developer extends Admin_Controller
{


	/**
	 * Sets up the permissions and loads the language file
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Site.Developer.View');
		$this->auth->restrict('WTche.Module.Builder');

		$this->lang->load('builder');

		//Template::set_block('sub_nav', 'database/developer/_sub_nav');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Display the list og migrations available at core, application and module level
	 *
	 * @access public
	 *
	 * @return void
	 */
	public function index()
	{
		
		die('BUILDER');

	}//end index()

	//--------------------------------------------------------------------
}//end class