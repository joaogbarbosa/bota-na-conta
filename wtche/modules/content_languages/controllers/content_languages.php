<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content_languages extends Base_Controller
{

	public function get($chave)
	{
		
		$this->load->model('content_languages/content_languages_model');

		return $this->content_languages_model->get($chave);
		
	}

	public function get_all()
	{
		
		$this->load->model('content_languages/content_languages_model');

		return $this->content_languages_model->find_all();
		
	}

}