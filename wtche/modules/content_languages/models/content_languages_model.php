<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtagencia.com/#license
 * @link      http://www.wtagencia.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Content_language Module Model
 *
 * @package    WTchê
 * @subpackage Content_language
 * @category   Models
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */
class Content_languages_model extends WT_Model
{


	/**
	 * Name of the table
	 *
	 * @access protected
	 *
	 * @var string
	 */
	protected $table_name	= 'content_languages';

	/**
	 * Busca o SEO
	 *
	 * @access public
	 *
	 * @param string $uri Chave do SEO Cadastrado.
	 *
	 * @return array
	 */
	public function get($key)
	{
		if (empty($key)) return FALSE;

		return $this->find_by('key', $key);

	}//end find_all_by()

}//end Settings_model
