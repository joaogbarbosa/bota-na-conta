<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_Default_Content_Languages extends Migration
{

	/**
	 * The name of the database table
	 *
	 * @var String
	 */
	private $table_name; //Pupulado no Construct

	//--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->table_name = get_module_name(__FILE__);

	}

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$this->db->insert($this->table_name, array('key' => 'pt', 'name' => 'Português'));
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		$this->db->truncate($this->table_name); 
	}

	//--------------------------------------------------------------------

}