<p class="intro"><?php echo lang('em_template_note'); ?></p>

<?php echo form_open(SITE_AREA .'/settings/emailer/template'); ?>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('em_header'); ?></h6></div>
    <div class="panel-body">
    	<textarea name="header" rows="15" style="width: 99%"><?php echo htmlspecialchars_decode($this->load->view('email/_header', null, true)) ;?></textarea>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('em_footer'); ?></h6></div>
    <div class="panel-body">
    	<textarea name="footer" rows="15" style="width: 99%"><?php echo htmlspecialchars_decode($this->load->view('email/_footer', null, true)) ;?></textarea>
    </div>
</div>

<div class="form-actions text-right">
	<?php echo anchor(SITE_AREA .'/settings/emailer', lang('bf_action_cancel'), 'class="btn btn-warning"'); ?>
	<input type="submit" name="save" id="submit" class="btn btn-success" value="<?php e(lang('em_save_template')); ?>" /> 
</div>

<?php echo form_close(); ?>
