<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(
	'description'	=> 'Allows users to exist in WTchê.',
	'author'		=> 'WTchê Team',
	'weights'		=> array(
		'settings'	=> 1
	)
);