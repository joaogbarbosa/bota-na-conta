<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class settings extends Admin_Controller_WT
{

	/**
	 * Sets up the require permissions and loads required classes
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct(__CLASS__, get_module_name(__FILE__));
	}//end __construct()

	/**
	 * Displays a form with seo setings
	 *
	 * @access public
	 *
	 * @return void
	 */
	public function defaults()
	{

		Template::set_block('sub_nav', 'seo/settings/_sub_nav');

		$this->auth->restrict('Seo.Settings.Manage');
		$this->lang->load('settings/settings');

		Template::set('toolbar_title', lang('seo_configuration'));

		$this->load->helper('config_file');

		if (isset($_POST['save']))
		{
			if ($this->save_defaults())
			{
				Template::set_message(lang('settings_saved_success'), 'success');
				redirect(SITE_AREA .'/settings/seo/defaults');
			}
			else
			{
				Template::set_message(lang('settings_error_success'), 'error');
			}
		}

		// Read our current settings
		$settings = $this->settings_lib->find_all();
		Template::set('settings', $settings);

		Template::set_view('seo/settings/index');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Performs the form validation and saves the settings to the database
	 *
	 * @access private
	 *
	 * @return bool
	 */
	private function save_defaults()
	{
		$this->wt_validation->set_rules('seo.default_title', 'lang:seo_default_title', 'trim');
		$this->wt_validation->set_rules('seo.default_description', 'lang:seo_default_description', 'trim');
		$this->wt_validation->set_rules('seo.default_keywords', 'lang:seo_default_keywords', 'trim');

		if ($this->wt_validation->run() === FALSE)
		{
			return FALSE;
		}


		$data = array(
			array('name' => 'seo.default_title', 'value' => $this->input->post('seo_default_title')),
			array('name' => 'seo.default_description', 'value' => $this->input->post('seo_default_description')),
			array('name' => 'seo.default_keywords', 'value' => $this->input->post('seo_default_keywords'))
		);

		//destroy the saved update message in case they changed update preferences.
		if ($this->cache->get('update_message'))
		{
			$this->cache->delete('update_message');
		}

		// Log the activity
		log_activity($this->current_user->id, lang('bf_act_settings_saved').': ' . $this->input->ip_address(), 'seo');

		// save the settings to the DB
		$updated = $this->settings_model->update_batch($data, 'name');

		return $updated;

	}//end save_settings()

}