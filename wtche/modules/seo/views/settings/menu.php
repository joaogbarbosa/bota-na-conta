<ul>
	<?php if ($this->auth->restrict('Seo.Settings.Manage')): ?>
		<li><a href="<?php echo site_url(SITE_AREA .'/settings/seo/defaults') ?>">Configurações Padrões</a></li>
	<?php endif ?>
	<li><a href="<?php echo site_url(SITE_AREA .'/settings/seo') ?>">Configurações por Página</a></li>
</ul>
