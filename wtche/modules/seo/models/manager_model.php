<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manager_model extends WT_Model {

	protected $table_name; //Pupulado no Construct
	protected $key			= "id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";

	protected $log_user 	= FALSE;

	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";

	/*
		Customize the operations of the model without recreating the insert, update,
		etc methods by adding the method names to act as callbacks here.
	 */
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	/*
		For performance reasons, you may require your model to NOT return the
		id of the last inserted row as it is a bit of a slow method. This is
		primarily helpful when running big loops over data.
	 */
	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array("id");

	protected $skip_validation 			= FALSE;

	protected $module_name;

	//--------------------------------------------------------------------

	public function __construct()
	{

		parent::__construct();

		$this->module_name = get_module_name(__FILE__);

		$this->table_name = $this->module_name;

		$this->lang->load($this->module_name,'',FALSE,TRUE,'',$this->module_name);
		
		$this->wtche->form->addField('char')
			->setFieldName('uri')
			->setLabel('URL')
			->setValidationRules('required|trim');

		$this->wtche->form->addField('char')
			->setFieldName('title')
			->setLabel('Título')
			->setValidationRules('required');

		$this->wtche->form->addField('text')
			->setFieldName('description')
			->setLabel('Descrição')
			->setValidationRules('');

		$this->wtche->form->addField('char')
			->setFieldName('keywords')
			->setLabel('Palavras Chave')
			->setValidationRules('trim');

	}

}
