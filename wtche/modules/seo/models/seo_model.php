<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtagencia.com/#license
 * @link      http://www.wtagencia.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Settings Module Model
 *
 * Provides methods to retrieve and update settings in the database
 *
 * @package    WTchê
 * @subpackage Modules_Settings
 * @category   Models
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */
class Seo_model extends WT_Model
{


	/**
	 * Name of the table
	 *
	 * @access protected
	 *
	 * @var string
	 */
	protected $table_name	= 'seo';

	/**
	 * Busca o SEO
	 *
	 * @access public
	 *
	 * @param string $uri Chave do SEO Cadastrado.
	 *
	 * @return array
	 */
	public function get($uri)
	{
		if (empty($uri)) return FALSE;

		return $this->find_by('uri', $uri);

	}//end find_all_by()

}//end Settings_model
