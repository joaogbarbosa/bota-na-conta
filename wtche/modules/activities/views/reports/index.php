<br>
<ul class="info-blocks">
	
	<?php if (has_permission('Activities.Own.View')): ?>
		<li class="bg-primary">
			<div class="top-info">
				<a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_own') ?>"><?php echo lang('activity_own'); ?></a>
				<small><?php echo lang('activity_own_description'); ?></small>
			</div>
			<a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_own') ?>"><i class="icon-user"></i></a>
		</li>
	<?php endif ?>

	<?php if (has_permission('Activities.User.View')): ?>
		<li class="bg-primary">
			<div class="top-info">
				<a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_user') ?>"><?php echo lang('activity_users'); ?></a>
				<small><?php echo lang('activity_users_description'); ?></small>
			</div>
			<a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_user') ?>"><i class="icon-users"></i></a>
		</li>
	<?php endif ?>

	<?php if (has_permission('Activities.Module.View')): ?>
		<li class="bg-primary">
			<div class="top-info">
				<a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_module') ?>"><?php echo lang('activity_modules'); ?></a>
				<small><?php echo lang('activity_module_description'); ?></small>
			</div>
			<a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_module') ?>"><i class="icon-stack"></i></a>
		</li>
	<?php endif ?>

	<?php if (has_permission('Activities.Date.View')): ?>
		<li class="bg-primary">
			<div class="top-info">
				<a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_date') ?>"><?php echo lang('activity_date'); ?></a>
				<small><?php echo lang('activity_date_description'); ?></small>
			</div>
			<a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_date') ?>"><i class="icon-calendar2"></i></a>
		</li>
	<?php endif ?>

</ul>

<br/>

<div class="row">
	<div class="column col-md-6">

		<!-- Active Modules -->
		<div class="panel panel-default">
            <div class="panel-heading"><h6 class="panel-title"><i class="icon-stack"></i> <?php echo lang('activity_top_modules'); ?></h6></div>
            <div class="table-responsive">
            	<?php if (isset($top_modules) && is_array($top_modules) && count($top_modules)) : ?>
            		<table class="table">
						<thead>
							<tr>
								<th><?php echo lang('activity_module'); ?></th>
								<th class="text-right"><?php echo lang('activity_logged'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($top_modules as $top_module) : ?>
							<tr>
								<td>
									<?php echo ucwords($top_module->module); ?>
								</td>
								<td class="text-right">
									<span class="label label-info"><?php echo $top_module->activity_count; ?></span>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
        		<?php else : ?>
					<?php echo lang('activity_no_top_modules'); ?>
				<?php endif; ?>
            </div>
        </div>

	</div>

	<div class="column col-md-6">

		<!-- Active Users -->
		<div class="panel panel-default">
            <div class="panel-heading"><h6 class="panel-title"><i class="icon-users"></i> <?php echo lang('activity_top_users'); ?></h6></div>
            <div class="table-responsive">
            	<?php if (isset($top_users) && is_array($top_users) && count($top_users)) : ?>
            		<table class="table table-striped">
						<thead>
							<tr>
								<th><?php echo lang('activity_user'); ?></th>
								<th class="text-right"><?php echo lang('activity_logged'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($top_users as $top_user) : ?>
							<tr>
								<td><strong><?php e($top_user->username == '' ? 'Not found':$top_user->username); ?></strong></td>
								<td class="text-right">
									<span class="label label-info"><?php echo $top_user->activity_count; ?></span>
								</td>
							</tr>
					<?php endforeach; ?>
						</tbody>
					</table>
        		<?php else : ?>
					<?php echo lang('activity_no_top_users'); ?>
				<?php endif; ?>
            </div>
        </div>

	</div>
</div>


<div class="row">

	<div class="column col-md-12">

		<div class="panel panel-default">
	        <div class="panel-heading"><h6 class="panel-title"><i class="icon-remove"></i> <?php echo lang('activity_cleanup'); ?></h6></div>
	        <div class="table-responsive">
	        	<?php $empty_table = true; ?>

				<table class="table">
					<tbody>
						<?php if (has_permission('Activities.Own.Delete')): ?>
							<tr>
								<td>
									<?php echo form_open(SITE_AREA .'/reports/activities/delete', array('id' => 'activity_own_form')); ?>
									<input type="hidden" name="action" value="activity_own" />
									<div class="form-inline">
										<label for="activity_own_select">
											<?php echo lang('activity_delete_own_note'); ?>
											<select name="which" id="activity_own_select" class="select2">
												<option value="<?php echo $current_user->id; ?>"><?php e($current_user->username); ?></option>
											</select>
										</label>
									</div>
									<?php echo form_close(); ?>
								</td>

								<td class="text-right">
									<button type="button" class="btn btn-danger" id="delete-activity_own"><?php echo lang('activity_own_delete'); ?></button>
								</td>
							</tr>
							<?php $empty_table = false; ?>
						<?php endif; ?>

						<?php if (has_permission('Activities.User.Delete')): ?>
						<tr>
							<td>
								<?php echo form_open(SITE_AREA .'/reports/activities/delete', array('id' => 'activity_user_form')); ?>
								<input type="hidden" name="action" value="activity_user" />
								<div class="form-inline">
									<label for="activity_user_select">
										<?php echo lang('activity_delete_user_note'); ?>
										<select name="which" id="activity_user_select" class="select2">
											<option value="all"><?php echo lang('activity_all_users'); ?></option>
										<?php foreach ($users as $au) : ?>
											<option value="<?php echo $au->id; ?>"><?php e($au->username); ?></option>
										<?php endforeach; ?>
										</select>
									</label>
								</div>
								<?php echo form_close(); ?>
							</td>

							<td class="text-right">
								<button type="button" class="btn btn-danger" id="delete-activity_user"><?php echo lang('activity_user_delete'); ?></button>
							</td>
						</tr>
						<?php $empty_table = false; ?>
						<?php endif; ?>

						<?php if (has_permission('Activities.Module.Delete')): ?>
						<tr>
							<td>
								<?php echo form_open(SITE_AREA .'/reports/activities/delete', array('id' => 'activity_module_form')); ?>
								<input type="hidden" name="action" value="activity_module" />

								<div class="form-inline">
									<label for="activity_module_select">
										<?php echo lang('activity_delete_module_note'); ?>
										<select name="which" id="activity_module_select" class="select2">
											<option value="all"><?php echo lang('activity_all_modules'); ?></option>
											<option value="core"><?php echo lang('activity_core'); ?></option>
										<?php foreach ($modules as $mod) : ?>
											<option value="<?php echo $mod; ?>"><?php echo $mod; ?></option>
										<?php endforeach; ?>
										</select>
									</label>
								</div>
								<?php echo form_close(); ?>
							</td>
							<td class="text-right">
								<button type="button" class="btn btn-danger" id="delete-activity_module"><?php echo lang('activity_module_delete'); ?></button>
							</td>
						</tr>
						<?php $empty_table = false; ?>
						<?php endif; ?>

						<?php if (has_permission('Activities.Date.Delete')): ?>
						<tr>
							<td>
								<?php echo form_open(SITE_AREA .'/reports/activities/delete', array('id' => 'activity_date_form')); ?>
								<input type="hidden" name="action" value="activity_date" />

								<div class="form-inline">
									<label for="activity_date_select">
										<?php echo lang('activity_delete_date_note'); ?>
										<select name="which" id="activity_date_select" class="select2">
											<option value="all"><?php echo lang('activity_all_dates'); ?></option>
										<?php foreach ($activities as $activity) : ?>
											<option value="<?php echo $activity->activity_id; ?>"><?php echo $activity->created_on; ?></option>
										<?php endforeach; ?>
										</select>
									</label>
								</div>
								<?php echo form_close(); ?>
							</td>
							<td class="text-right">
								<button type="button" class="btn btn-danger" id="delete-activity_date"><?php echo lang('activity_date_delete'); ?></button>
							</td>
						</tr>
						<?php $empty_table = false; ?>
						<?php endif; ?>

						<?php if ($empty_table) :?>
						<tr>
							<td colspan="2"><?php echo lang('activity_none_found'); ?></td>
						</tr>
						<?php endif; ?>
					</tbody>
				</table>
	        </div>
	    </div>

	</div>

</div>
