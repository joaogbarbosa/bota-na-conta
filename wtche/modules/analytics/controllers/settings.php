<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class settings extends Admin_Controller
{

	/**
	 * Sets up the require permissions and loads required classes
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		// restrict access - View and Manage
		$this->auth->restrict('Analytics.Settings.View');
		$this->auth->restrict('Analytics.Settings.Manage');

		$this->lang->load('analytics');
		$this->lang->load('settings/settings');

		Template::set('toolbar_title', lang('analytics_configuration'));

		$this->load->helper('config_file');

	}//end __construct()

	/**
	 * Displays a form with analytics setings
	 *
	 * @access public
	 *
	 * @return void
	 */
	public function index()
	{

		if (isset($_POST['save']))
		{
			if ($this->save_settings())
			{
				Template::set_message(lang('settings_saved_success'), 'success');
				redirect(SITE_AREA .'/settings/analytics');
			}
			else
			{
				Template::set_message(lang('settings_error_success'), 'error');
			}
		}

		// Read our current settings
		$settings = $this->settings_lib->find_all();
		Template::set('settings', $settings);

		Template::set_block('sub_nav', 'analytics/settings/_sub_nav');

		Template::set_view('analytics/settings/index');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Performs the form validation and saves the settings to the database
	 *
	 * @access private
	 *
	 * @return bool
	 */
	private function save_settings()
	{
		$this->wt_validation->set_rules('analytics.code', 'lang:analytics_code', 'trim');

		if ($this->wt_validation->run() === FALSE)
		{
			return FALSE;
		}


		$data = array(
			array('name' => 'analytics.code', 'value' => $this->input->post('analytics_code', FALSE))
		);

		//destroy the saved update message in case they changed update preferences.
		if ($this->cache->get('update_message'))
		{
			$this->cache->delete('update_message');
		}

		// Log the activity
		log_activity($this->current_user->id, lang('bf_act_settings_saved').': ' . $this->input->ip_address(), 'analytics');

		// save the settings to the DB
		$updated = $this->settings_model->update_batch($data, 'name');

		return $updated;

	}//end save_settings()

}