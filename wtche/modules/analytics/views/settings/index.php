<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal form-bordered"'); ?>
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><?php echo $toolbar_title ?></h6></div>
        <div class="panel-body">

        	<div class="form-group">
				<label class="col-sm-2 control-label text-right" for="force_pass_reset"><?php echo lang('analytics_code') ?></label>
				<div class="col-sm-10">
					<textarea name="analytics_code" class="form-control"><?php echo set_value('analytics_code', isset($settings['analytics.code']) ? $settings['analytics.code'] : '') ?></textarea>
					<p class="help-block"><?php echo lang('analytics_code_help') ?></p>
				</div>
			</div>

        </div>
    </div>

    <div class="form-actions text-right">
		<input type="submit" name="save" class="btn btn-success" value="<?php echo lang('bf_action_save') . ' ' . lang('bf_context_settings'); ?>" />
	</div>

<?php echo form_close(); ?>