<div class="bg-info with-padding block-inner"><?php echo lang('mig_intro'); ?></div>
<div class="admin-box">

	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#app-tab" data-toggle="tab"><?php echo lang('mig_tab_app'); ?></a>
		</li>
		<li>
			<a href="#mod-tab" data-toggle="tab"><?php echo lang('mig_tab_mod'); ?></a>
		</li>
		<li>
			<a href="#core-tab" data-toggle="tab"><?php echo lang('mig_tab_core'); ?></a>
		</li>
	</ul>
	<br>

	<div class="tab-content" style="border: 0">
		<!-- Application Migrations -->
		<div class="tab-pane active" id="app-tab">

			<div class="panel panel-default">
			    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('mig_app_migrations'); ?></h6></div>
			    <div class="panel-body">

					<div class="alert alert-info fade in">
						<a class="close" data-dismiss="alert">&times;</a>
						<?php echo sprintf(lang('mig_installed_version'), $installed_version); ?> /
						<?php echo sprintf(lang('mig_latest_version'), $latest_version); ?>
					</div>
					<br>
					<?php echo form_open($this->uri->uri_string(), 'class="constrained"'); ?>
						<input type="hidden" name="core_only" value="0" />

						<?php if (count($app_migrations)) : ?>
						<p>
							<?php echo lang('mig_choose_migration'); ?>
							<select name="migration">
							<?php foreach ($app_migrations as $migration) :?>
								<option value="<?php echo (int)substr($migration, 0, 3) ?>" <?php echo ((int)substr($migration, 0, 3) == $this->uri->segment(5)) ? 'selected="selected"' : '' ?>><?php echo $migration ?></option>
							<?php endforeach; ?>
							</select>
						</p>

						<div class="form-actions">
							<input type="submit" name="migrate" value="<?php echo lang('mig_migrate_button'); ?>" /> or <?php echo anchor(SITE_AREA .'/developer/migrations', lang('bf_action_cancel')); ?>
						</div>
						<?php else: ?>
							<div class="alert alert-warning fade in">
								<a class="close" data-dismiss="alert">&times;</a>
								<?php echo lang('mig_no_migrations') ?>
							</div>
						<?php endif; ?>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>

		<!-- Module Migrations -->
		<div id="mod-tab" class="tab-pane">
			<div class="panel panel-default">
			    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('mig_mod_migrations'); ?></h6></div>
			    <div class="table-responsive">

					<?php if (isset($mod_migrations) && is_array($mod_migrations)) :?>
						<table class="table table-striped">
							<thead>
								<tr>
									<th><?php echo lang('mig_tbl_module'); ?></th>
									<th><?php echo lang('mig_tbl_installed_ver'); ?></th>
									<th><?php echo lang('mig_tbl_latest_ver'); ?></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($mod_migrations as $module => $migrations) : ?>
								<tr>
									<td><?php echo ucfirst($module) ?></td>
									<td><?php echo $migrations['installed_version'] ?></td>
									<td><?php echo $migrations['latest_version'] ?></td>
									<td>
										<?php echo form_open(site_url(SITE_AREA .'/developer/migrations/migrate_module/'. $module), 'class="form-horizontal"'); ?>
											<input type="hidden" name="is_module" value="1" />
											<div class="row">
												<div class="col-md-8">
													<select name="version" class="select2 pull-left">
														<option value=""><?php echo lang('mig_choose_migration'); ?></option>
														<option value="uninstall"><?php echo lang('mig_uninstall'); ?></option>
														<?php foreach ($migrations as $migration) : ?>
															<?php if(is_array($migration)): ?>
																<?php foreach ($migration as $filename) :?>
																	<option <?php echo $filename === reset($migration)?'selected':'' ?>><?php echo $filename; ?></option>
																<?php endforeach; ?>
															<?php endif;?>
														<?php endforeach; ?>
													</select>
												</div>
												<div class="col-md-2 text-right">
													<input type="submit" name="migrate" class="btn btn-warning" value="<?php e(lang('mig_migrate_module')); ?>" />
												</div>
											</div>
										<?php echo form_close(); ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

					<?php else : ?>
						<br/>
						<div class="alert alert-info fade in ">
							<a class="close" data-dismiss="alert">&times;</a>
							<?php echo lang('mig_no_migrations') ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<!-- WTche Migrations -->
		<div id="core-tab" class="tab-pane">

			<div class="panel panel-default">
			    <div class="panel-heading"><h6 class="panel-title"><?php echo lang('mig_core_migrations'); ?></h6></div>
			    <div class="panel-body">

					<div class="alert alert-info fade in ">
						<a class="close" data-dismiss="alert">&times;</a>
						<?php echo sprintf(lang('mig_installed_version'), $core_installed_version); ?> /
						<?php echo sprintf(lang('mig_latest_version'), $core_latest_version); ?>
					</div>
					<br>

					<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
						<input type="hidden" name="core_only" value="1" />

						<?php if (count($core_migrations)) : ?>
							<div class="form-group">
								<label class="col-sm-2 control-label text-right" for="migration"><?php echo lang('mig_choose_migration'); ?></label>
								<div class="col-sm-10">
									<select name="migration" id="migration" class="select2">
									<?php foreach ($core_migrations as $migration) :?>
										<option value="<?php echo (int)substr($migration, 0, 3) ?>" <?php echo ((int)substr($migration, 0, 3) == $this->uri->segment(5)) ? 'selected="selected"' : '' ?>><?php echo $migration ?></option>
									<?php endforeach; ?>
									</select>
								</div>
							</div>

							<div class="form-actions text-right">
							<?php echo anchor(SITE_AREA .'/developer/migrations', lang('bf_action_cancel'), 'class="btn btn-primary"'); ?>
								<input type="submit" name="migrate" class="btn btn-warning" value="<?php echo lang('mig_migrate_button'); ?>" />
							</div>
						<?php else: ?>
							<div class="alert alert-info" style="margin: 10px;">
								<p><?php echo lang('mig_no_migrations'); ?></p>
							</div>
						<?php endif; ?>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
