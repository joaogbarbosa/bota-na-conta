<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_v0001 extends Migration {

	/*

	Instalação Inicial do WTchê

	*/
	
	public function up() 
	{
		
		if ( ! $this->db->table_exists('activities')) {
            $this->dbforge->add_field(
            	array(
					'activity_id' => array(
						'type' => 'BIGINT',
						'constraint' => 20,
						'auto_increment' => true,
			            'null' => false
					),
					'user_id' => array(
						'type' => 'BIGINT',
						'constraint' => 20,
			            'null' => false,
			            'default' => 0
					),
					'activity' => array(
						'type' => 'LONGTEXT'
					),
					'module' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
					),
					'created_on' => array(
						'type' => 'DATETIME',
			            'null' => false
					),
					'deleted' => array(
						'type' => 'tinyint',
						'constraint' => 12,
			            'null' => false,
			            'default' => 0
					),
				)
            );
            $this->dbforge->add_key('activity_id', true);
            $this->dbforge->create_table('activities');
        }

		if ( ! $this->db->table_exists('email_queue')) {
            $this->dbforge->add_field(
            	array(
					'id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'auto_increment' => true,
			            'null' => false
					),
					'to_email' => array(
						'type' => 'VARCHAR',
						'constraint' => 128,
			            'null' => false
					),
					'reply_to' => array(
						'type' => 'VARCHAR',
						'constraint' => 128,
			            'null' => false
					),
					'subject' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false
					),
					'message' => array(
						'type' => 'TEXT',
			            'null' => false,
					),
					'alt_message' => array(
						'type' => 'TEXT',
						'null' => true,
					),
					'max_attempts' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false,
			            'default' => 3
					),
					'attempts' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false,
			            'default' => 0
					),
					'success' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'date_published' => array(
						'type' => 'DATETIME',
			            'null' => true
					),
					'last_attempt' => array(
						'type' => 'DATETIME',
			            'null' => true
					),
					'date_sent' => array(
						'type' => 'DATETIME',
			            'null' => true
					),
				)
            );
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('email_queue');
        }

		if ( ! $this->db->table_exists('login_attempts')) {
            $this->dbforge->add_field(
            	array(
					'id' => array(
						'type' => 'BIGINT',
						'constraint' => 20,
						'auto_increment' => true,
			            'null' => false
					),
					'ip_address' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false
					),
					'login' => array(
						'type' => 'VARCHAR',
						'constraint' => 50,
			            'null' => false
					),
					/* This will probably cause an error outside MySQL and may not
			         * be cross-database compatible for reasons other than
			         * CURRENT_TIMESTAMP
			         */
					'time TIMESTAMP DEFAULT CURRENT_TIMESTAMP'

				)
            );
            $this->dbforge->add_key('id', true);
            $this->dbforge->create_table('login_attempts');
        }

		if ( ! $this->db->table_exists('permissions')) {
            $this->dbforge->add_field(
            	array(
					'permission_id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'auto_increment' => true,
			            'null' => false
					),
					'name' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false
					),
					'description' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false
					),
					'status' => array(
						'type' => 'ENUM',
						'constraint' => "'active','inactive','deleted'",
						'default' => 'active',
						'null' => false,
					)

				)
            );
            $this->dbforge->add_key('permission_id', true);
            $this->dbforge->create_table('permissions');
            $this->db->insert_batch('permissions', array(
				array('name' => 'Site.Content.View','description' => 'Permite aos usuários visualizar o contexto "Conteúdo"','status' => 'active'),
				array('name' => 'Site.Reports.View','description' => 'Allow users to view the Reports Context','status' => 'active'),
				array('name' => 'Site.Settings.View','description' => 'Allow users to view the Settings Context','status' => 'active'),
				array('name' => 'Site.Developer.View','description' => 'Allow users to view the Developer Context','status' => 'active'),
				array('name' => 'WTche.Roles.Manage','description' => 'Allow users to manage the user Roles','status' => 'active'),
				array('name' => 'WTche.Users.Manage','description' => 'Allow users to manage the site Users','status' => 'active'),
				array('name' => 'WTche.Users.View','description' => 'Allow users access to the User Settings','status' => 'active'),
				array('name' => 'WTche.Users.Add','description' => 'Allow users to add new Users','status' => 'active'),
				array('name' => 'WTche.Database.Manage','description' => 'Allow users to manage the Database settings','status' => 'active'),
				array('name' => 'WTche.Emailer.Manage','description' => 'Allow users to manage the Emailer settings','status' => 'active'),
				array('name' => 'WTche.Logs.View','description' => 'Allow users access to the Log details','status' => 'active'),
				array('name' => 'WTche.Logs.Manage','description' => 'Allow users to manage the Log files','status' => 'active'),
				array('name' => 'WTche.Emailer.View','description' => 'Allow users access to the Emailer settings','status' => 'active'),
				array('name' => 'Site.Signin.Offline','description' => 'Allow users to login to the site when the site is offline','status' => 'active'),
				array('name' => 'WTche.Permissions.View','description' => 'Allow access to view the Permissions menu unders Settings Context','status' => 'active'),
				array('name' => 'WTche.Permissions.Manage','description' => 'Allow access to manage the Permissions in the system','status' => 'active'),
				array('name' => 'WTche.Roles.Delete','description' => 'Allow users to delete user Roles','status' => 'active'),
				array('name' => 'WTche.Modules.Add','description' => 'Allow creation of modules with the builder.','status' => 'active'),
				array('name' => 'WTche.Modules.Delete','description' => 'Allow deletion of modules.','status' => 'active'),
				array('name' => 'Permissions.Administrador.Manage','description' => 'To manage the access control permissions for the Administrator role.','status' => 'active'),
				array('name' => 'Permissions.Usuário.Manage','description' => 'To manage the access control permissions for the Developer role.','status' => 'inactive'),
				array('name' => 'Activities.Own.View','description' => 'To view the users own activity logs','status' => 'active'),
				array('name' => 'Activities.Own.Delete','description' => 'To delete the users own activity logs','status' => 'active'),
				array('name' => 'Activities.User.View','description' => 'To view the user activity logs','status' => 'active'),
				array('name' => 'Activities.User.Delete','description' => 'To delete the user activity logs, except own','status' => 'active'),
				array('name' => 'Activities.Module.View','description' => 'To view the module activity logs','status' => 'active'),
				array('name' => 'Activities.Module.Delete','description' => 'To delete the module activity logs','status' => 'active'),
				array('name' => 'Activities.Date.View','description' => 'To view the users own activity logs','status' => 'active'),
				array('name' => 'Activities.Date.Delete','description' => 'To delete the dated activity logs','status' => 'active'),
				array('name' => 'WTche.Settings.View','description' => 'To view the site settings page.','status' => 'active'),
				array('name' => 'WTche.Settings.Manage','description' => 'To manage the site settings.','status' => 'active'),
				array('name' => 'WTche.Activities.View','description' => 'To view the Activities menu.','status' => 'active'),
				array('name' => 'WTche.Database.View','description' => 'To view the Database menu.','status' => 'active'),
				array('name' => 'WTche.Migrations.View','description' => 'To view the Migrations menu.','status' => 'active'),
				array('name' => 'WTche.Builder.View','description' => 'To view the Modulebuilder menu.','status' => 'active'),
				array('name' => 'WTche.Roles.View','description' => 'To view the Roles menu.','status' => 'active'),
				array('name' => 'WTche.Sysinfo.View','description' => 'To view the System Information page.','status' => 'active'),
				array('name' => 'WTche.Translate.Manage','description' => 'To manage the Language Translation.','status' => 'active'),
				array('name' => 'WTche.Translate.View','description' => 'To view the Language Translate menu.','status' => 'active'),
				array('name' => 'WTche.Profiler.View','description' => 'To view the Console Profiler Bar.','status' => 'active'),
				array('name' => 'WTche.Roles.Add','description' => 'To add New Roles','status' => 'active'),
				array('name' => 'WTche.Module.Builder','description' => 'To create new Modules','status' => 'active'),
			));
        }

        if ( ! $this->db->table_exists('role_permissions')) {
            $this->dbforge->add_field(
            	array(
					'role_id' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false
					),
					'permission_id' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false
					),
				)
            );
            $this->dbforge->add_key('role_id', true);
            $this->dbforge->add_key('permission_id', true);
            $this->dbforge->create_table('role_permissions');
            $this->db->insert_batch('role_permissions', array(
            	array('role_id' => '1','permission_id' => '1'),
            	array('role_id' => '1','permission_id' => '2'),
				array('role_id' => '1','permission_id' => '3'),
				array('role_id' => '1','permission_id' => '4'),
				array('role_id' => '1','permission_id' => '5'),
				array('role_id' => '1','permission_id' => '6'),
				array('role_id' => '1','permission_id' => '7'),
				array('role_id' => '1','permission_id' => '8'),
				array('role_id' => '1','permission_id' => '9'),
				array('role_id' => '1','permission_id' => '10'),
				array('role_id' => '1','permission_id' => '11'),
				array('role_id' => '1','permission_id' => '12'),
				array('role_id' => '1','permission_id' => '13'),
				array('role_id' => '1','permission_id' => '14'),
				array('role_id' => '1','permission_id' => '15'),
				array('role_id' => '1','permission_id' => '16'),
				array('role_id' => '1','permission_id' => '17'),
				array('role_id' => '1','permission_id' => '18'),
				array('role_id' => '1','permission_id' => '19'),
				array('role_id' => '1','permission_id' => '20'),
				array('role_id' => '1','permission_id' => '21'),
				array('role_id' => '1','permission_id' => '22'),
				array('role_id' => '1','permission_id' => '23'),
				array('role_id' => '1','permission_id' => '24'),
				array('role_id' => '1','permission_id' => '25'),
				array('role_id' => '1','permission_id' => '26'),
				array('role_id' => '1','permission_id' => '27'),
				array('role_id' => '1','permission_id' => '28'),
				array('role_id' => '1','permission_id' => '29'),
				array('role_id' => '1','permission_id' => '30'),
				array('role_id' => '1','permission_id' => '31'),
				array('role_id' => '1','permission_id' => '32'),
				array('role_id' => '1','permission_id' => '33'),
				array('role_id' => '1','permission_id' => '34'),
				array('role_id' => '1','permission_id' => '35'),
				array('role_id' => '1','permission_id' => '36'),
				array('role_id' => '1','permission_id' => '37'),
				array('role_id' => '1','permission_id' => '38'),
				array('role_id' => '1','permission_id' => '39'),
				array('role_id' => '1','permission_id' => '40'),
				array('role_id' => '1','permission_id' => '41'),
				array('role_id' => '1','permission_id' => '42')
			));
        }

        if ( ! $this->db->table_exists('roles')) {
            $this->dbforge->add_field(
            	array(
					'role_id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'auto_increment' => true,
			            'null' => false
					),
					'role_name' => array(
						'type' => 'VARCHAR',
						'constraint' => 60,
			            'null' => false
					),
					'description' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => true
					),
					'default' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'can_delete' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 1
					),
					'login_destination' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
			            'default' => '/'
					),
					'deleted' => array(
						'type' => 'INT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'default_context' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
			            'default' => 'content'
					)
				)
            );
            $this->dbforge->add_key('role_id', true);
            $this->dbforge->create_table('roles');
            $this->db->insert_batch('roles', array(
            	array('role_name' => 'Administrador', 'description' => 'Has full control over every aspect of the site.', 'default' => '0', 'can_delete' => '0', 'login_destination' => '/manager', 'deleted' => '0', 'default_context' => 'content'),
				array('role_name' => 'Usuário', 'description' => 'This is the default user with access to login.', 'default' => '1', 'can_delete' => '1', 'login_destination' => '/manager', 'deleted' => '0', 'default_context' => 'content'),
			));
        }

        /* Não necessário pois é feito ná classe de migrations
        // Vamos deixar aqui apenas para ficar visível que é criada a tabela
		if ( ! $this->db->table_exists('schema_version')) {
            $this->dbforge->add_field(
            	array(
					'type' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false
					),
					'version' => array(
						'type' => 'INT',
						'constraint' => 4,
			            'null' => false,
			            'default' => 0
					)
				)
            );
            $this->dbforge->add_key('type', true);
            $this->dbforge->create_table('schema_version');
            $this->db->insert_batch('schema_version', array(
            	array('type' => 'core', 'version' => '0')
            ));
        }
        */

		if ( ! $this->db->table_exists('sessions')) {
            $this->dbforge->add_field(
            	array(
					'session_id' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false,
			            'default' => 0
					),
					'ip_address' => array(
						'type' => 'VARCHAR',
						'constraint' => 45,
			            'null' => false,
			            'default' => 0
					),
					'user_agent' => array(
						'type' => 'VARCHAR',
						'constraint' => 120,
			            'null' => false
					),
					'last_activity' => array(
						'type' => 'INT',
						'constraint' => 10,
						'unsigned' => true,
			            'null' => false,
			            'default' => 0
					),
					'user_data' => array(
						'type' => 'TEXT',
			            'null' => false
					)
				)
            );
            $this->dbforge->add_key('session_id', true);
            $this->dbforge->create_table('sessions');
        }
		
		if ( ! $this->db->table_exists('settings')) {
            $this->dbforge->add_field(
            	array(
					'name' => array(
						'type' => 'VARCHAR',
						'constraint' => 30,
			            'null' => false
					),
					'module' => array(
						'type' => 'VARCHAR',
						'constraint' => 50,
			            'null' => false
					),
					'value' => array(
						'type' => 'TEXT',
			            'null' => false
					)
				)
            );
            $this->dbforge->add_key('name', true);
            $this->dbforge->create_table('settings');
            $this->db->insert_batch('settings', array(
            	array('name' => 'auth.allow_name_change', 			'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.allow_register', 				'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.allow_remember', 				'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.do_login_redirect', 			'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.login_type', 					'module' => 'core', 	'value' => 'username'),
				array('name' => 'auth.name_change_frequency', 		'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.name_change_limit', 			'module' => 'core', 	'value' => '1'),
				array('name' => 'auth.password_force_mixed_case', 	'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.password_force_numbers', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.password_force_symbols', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.password_min_length', 		'module' => 'core', 	'value' => '8'),
				array('name' => 'auth.password_show_labels', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.remember_length', 			'module' => 'core', 	'value' => '1209600'),
				array('name' => 'auth.user_activation_method', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.use_extended_profile', 		'module' => 'core', 	'value' => '0'),
				array('name' => 'auth.use_usernames', 				'module' => 'core', 	'value' => '1'),
				array('name' => 'mailpath', 						'module' => 'email', 	'value' => '/usr/sbin/sendmail'),
				array('name' => 'mailtype', 						'module' => 'email', 	'value' => 'html'),
				array('name' => 'password_iterations', 				'module' => 'users', 	'value' => '8'),
				array('name' => 'protocol', 						'module' => 'email', 	'value' => 'smtp'),
				array('name' => 'sender_email', 					'module' => 'email', 	'value' => 'programadores@botanaconta.com.br'),
				array('name' => 'site.languages', 					'module' => 'core', 	'value' => 'a:1:{i:0;s:2:"pt";}'),
				array('name' => 'site.show_front_profiler', 		'module' => 'core', 	'value' => '1'),
				array('name' => 'site.show_profiler', 				'module' => 'core', 	'value' => '1'),
				array('name' => 'site.status', 						'module' => 'core', 	'value' => '1'),
				array('name' => 'site.system_email', 				'module' => 'core', 	'value' => 'programadores@botanaconta.com.br'),
				array('name' => 'site.title', 						'module' => 'core', 	'value' => 'Bota na Conta'),
				array('name' => 'smtp_host', 						'module' => 'email', 	'value' => 'smtp.botanaconta.com.br'),
				array('name' => 'smtp_pass', 						'module' => 'email', 	'value' => ''),
				array('name' => 'smtp_port', 						'module' => 'email', 	'value' => '587'),
				array('name' => 'smtp_timeout', 					'module' => 'email', 	'value' => '30'),
				array('name' => 'smtp_user', 						'module' => 'email', 	'value' => 'enviosmtp@botanaconta.com.br'),
				array('name' => 'site.list_limit', 					'module' => 'core', 	'value' => '25'),
            ));
        }

		if ( ! $this->db->table_exists('user_cookies')) {
            $this->dbforge->add_field(
            	array(
					'user_id' => array(
						'type' => 'BIGINT',
						'constraint' => 20,
			            'null' => false
					),
					'token' => array(
						'type' => 'VARCHAR',
						'constraint' => 128,
			            'null' => false
					),
					'created_on' => array(
						'type' => 'DATETIME',
			            'null' => false
					)
				)
            );
            $this->dbforge->add_key('token', true);
            $this->dbforge->create_table('user_cookies');

        }

		if ( ! $this->db->table_exists('user_meta')) {
            $this->dbforge->add_field(
            	array(
					'meta_id' => array(
						'type' => 'INT',
						'constraint' => 20,
						'unsigned' => true,
			            'null' => false,
			            'auto_increment' => true,
					),
					'user_id' => array(
						'type' => 'INT',
						'constraint' => 20,
						'unsigned' => true,
			            'null' => false,
			            'default' => 0,
					),
					'meta_key' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
			            'default' => ''
					),
					'meta_value' => array(
						'type' => 'TEXT'
					)
				)
            );
            $this->dbforge->add_key('meta_id', true);
            $this->dbforge->create_table('user_meta');

        }

        
		if ( ! $this->db->table_exists('users')) {
            $this->dbforge->add_field(
            	array(
					'id' => array(
						'type' => 'BIGINT',
						'constraint' => 20,
						'unsigned' => true,
			            'null' => false,
			            'auto_increment' => true,
					),
					'role_id' => array(
						'type' => 'INT',
						'constraint' => 11,
			            'null' => false,
			            'default' => 2,
					),
					'email' => array(
						'type' => 'VARCHAR',
						'constraint' => 120,
			            'null' => false
					),
					'username' => array(
						'type' => 'VARCHAR',
						'constraint' => 30,
			            'null' => false,
			            'default' => ''
					),
					'password_hash' => array(
						'type' => 'CHAR',
						'constraint' => 60,
			            'null' => false
					),
					'reset_hash' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => true
					),
					'last_login' => array(
						'type' => 'DATETIME',
			            'null' => false,
			            'default' => '0000-00-00 00:00:00'
					),
					'last_ip' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false,
			            'default' => '0'
					),
					'created_on' => array(
						'type' => 'DATETIME',
			            'null' => false,
			            'default' => '0000-00-00 00:00:00'
					),
					'deleted' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'reset_by' => array(
						'type' => 'INT',
						'constraint' => 10,
			            'null' => true
					),
					'banned' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'ban_message' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => true
					),
					'display_name' => array(
						'type' => 'VARCHAR',
						'constraint' => 255,
			            'null' => false,
			            'default' => ''
					),
					'display_name_changed' => array(
						'type' => 'DATE',
			            'null' => true
					),
					'timezone' => array(
						'type' => 'CHAR',
						'constraint' => 4,
			            'null' => false,
			            'default' => 'UM3'
					),
					'language' => array(
						'type' => 'VARCHAR',
						'constraint' => 20,
			            'null' => false,
			            'default' => 'pt'
					),
					'active' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					),
					'activate_hash' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
			            'null' => false,
			            'default' => ''
					),
					'password_iterations' => array(
						'type' => 'INT',
						'constraint' => 4,
			            'null' => false
					),
					'force_password_reset' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
			            'null' => false,
			            'default' => 0
					)
				)
            );
            $this->dbforge->add_key('id', true);
            $this->dbforge->add_key('email');
            $this->dbforge->create_table('users');

        }

	}
	
	public function down() 
	{
		
		//Boom! =D

	}

}