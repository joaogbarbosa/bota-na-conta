<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtagencia.com/#license
 * @link      http://www.wtagencia.com
 * @since     Version 1.0
 * @filesource
 */

/**
 * Lang Helpers
 *
 * Provides additional functions for working with langs.
 *
 * @package    WTchê
 * @subpackage Helpers
 * @category   Helpers
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */

if ( ! function_exists('site_lang'))
{

	/**
	 * Retorna o key do lang atual
	 *
	 * @return string
	 */
	function site_lang()
	{
		return WT_Language::lang_key();
	}//end site_lang()
}

if ( ! function_exists('lang_url'))
{

	/**
	 * Retorna a URL de acordo com o lang atual
	 *
	 * @return string
	 */
	function lang_url($path = null)
	{
		return WT_Language::lang_url($path);
	}//end lang_url()
}