<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

if ( ! function_exists('get_module_name')) {
	function get_module_name($file) {
		$matches = array();
		$file = str_replace('\\',  '/', $file);
		preg_match('/^.*\/modules\/([^\/]+)\/.*$/', $file, $matches);
		return end($matches);
	}
}


