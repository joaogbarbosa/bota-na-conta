<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtagencia.com/#license
 * @link      http://www.wtagencia.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * UI Helper
 *
 * @package    WTchê
 * @subpackage Modules_Ui
 * @category   Helpers
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */

if (!function_exists('render_search_box'))
{
	/**
	 * Displays a search box
	 *
	 * @access public
	 *
	 * @return void
	 */
	function render_search_box()
	{
		$ci =& get_instance();
		$search = $ci->lang->line('bf_action_search');
		$search_lower = strtolower($search);

		$form =<<<END
<a href="#" class="list-search">{$search}...</a>

<div id="search-form" style="display: none">
	<input type="search" class="list-search" value="" placeholder="{$search_lower}..."  />
</div>
END;

		echo $form;

	}//end render_search_box()
}

//--------------------------------------------------------------------
