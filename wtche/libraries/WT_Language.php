<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * WTchê
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   WTchê
 * @author    WTchê Dev Team
 * @copyright Copyright (c) 2011 - 2013, WTchê Dev Team
 * @license   http://www.wtagencia.com/#license
 * @link      http://www.wtagencia.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Language
 *
 * Metodos para auxílio nos sites multi-idiomas
 *
 * @package    WTchê
 * @subpackage Libraries
 * @category   Libraries
 * @author     WTchê Dev Team
 * @link       http://www.wtagencia.com.br/#guides
 *
 */
class WT_Language
{

    /**
     * Retorna a LANG atual do Site
     *
     */
    public static function lang_key() {

        $wtche =& get_instance();

        $wtche->load->module('content_languages');
        $lang = $wtche->content_languages->get($wtche->uri->segment(1));
        if ($lang) {
            $key = $lang->key;
        }
        if (!$lang && $wtche->uri->segment(1)) {
            show_404();
        } elseif (!$wtche->uri->segment(1)) {
            $key = $wtche->config->item('language');
        }

        return $key;

    }

    /**
     * Retorna a URL com o lang já setado
     *
     */
    public static function lang_url($path = null) {
        return site_url(self::lang_key() . '/' . $path);
    }

}//end class

/* End of file : ./libraries/WT_Language.php */